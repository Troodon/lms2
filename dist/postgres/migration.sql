-- feed items
alter table lms2.feed_item alter column created_at type timestamp(0) with time zone;
alter table lms2.feed_item alter column updated_at type timestamp(0) with time zone;

-- certificate
alter table lms2.certificate alter column created_at type timestamp(0) with time zone;
alter table lms2.certificate alter column updated_at type timestamp(0) with time zone;
alter table lms2.certificate alter column expires_at type timestamp(0) with time zone;

-- domain
alter table lms2.domain alter column created_at type timestamp(0) with time zone;
alter table lms2.domain alter column updated_at type timestamp(0) with time zone;

-- feed
alter table lms2.feed alter column created_at type timestamp(0) with time zone;
alter table lms2.feed alter column updated_at type timestamp(0) with time zone;

-- page
alter table lms.space alter column created_at type timestamp(0) with time zone;
alter table lms.space alter column updated_at type timestamp(0) with time zone;

-- payment
alter table lms2.payment alter column created_at type timestamp(0) with time zone;
alter table lms2.payment alter column updated_at type timestamp(0) with time zone;
alter table lms2.payment alter column paid_to type timestamp(0) with time zone;

-- role
alter table lms2.role alter column created_at type timestamp(0) with time zone;
alter table lms2.role alter column updated_at type timestamp(0) with time zone;

-- schedule
alter table lms2.schedule alter column created_at type timestamp(0) with time zone;
alter table lms2.schedule alter column updated_at type timestamp(0) with time zone;
alter table lms2.schedule alter column date type timestamp(0) with time zone;

-- site
alter table lms2.site alter column created_at type timestamp(0) with time zone;
alter table lms2.site alter column updated_at type timestamp(0) with time zone;
alter table lms2.site alter column active_to type timestamp(0) with time zone;

-- token
alter table lms2.token alter column created_at type timestamp(0) with time zone;
alter table lms2.token alter column expires_at type timestamp(0) with time zone;

-- user
alter table lms2.user alter column created_at type timestamp(0) with time zone;
alter table lms2.user alter column updated_at type timestamp(0) with time zone;
