<?php

use easmith\selectel\storage\SelectelStorage;

error_reporting(E_ALL);

include __DIR__ . "/../vendor/autoload.php";

function get_site_id($domain)
{
    $domain = trim(mb_strtolower($domain));
    $config = require_once __DIR__ . '/../app/config/config.php';

    if ($siteId = apcu_fetch("st:{$domain}")) {
        return +$siteId;
    }

    $connection = new PDO(
        "mysql:host={$config->database->host};dbname={$config->database->dbname}",
        $config->database->username,
        $config->database->password,
        [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]
    );

    $sth = $connection->prepare("SELECT * FROM domain d WHERE d.domain = :domain ");
    $sth->execute(['domain' => $domain]);
    $Domain = $sth->fetch();

    return +$Domain['site'];
}

try {
    $selectelStorage = new SelectelStorage("87384", "vvkcPyRW");
    $siteId = get_site_id(mb_strtolower($_SERVER['HTTP_HOST']));
    $path = "static/{$_GET['_url']}";
    $path = str_replace('//', '/', $path);
    $path = str_replace('static/static', 'static/', $path);
    $path = str_replace('//', '/', $path);

    if ($siteId) {
        $file = $selectelStorage
            ->getContainer("site{$siteId}")
            ->getFile($path, []);

        // var_dump([$path, $_SERVER['HTTP_HOST']]);exit();
        header('Content-type:' . $file['header']['content-type']);
        header('Content-length: ' . $file['header']['content-length']);

        if (isset($file['header']['last_modified'])) {
            header('Last-Modified: ' . $file['header']['last_modified']);
        }

//        ~rt($file['header']['content-type'], $file);
        die($file['content']);
    }
    var_dump([$path, $siteId, $_SERVER['HTTP_HOST']]);exit();
} catch (Exception $e) {
    echo $e->getMessage();
}
