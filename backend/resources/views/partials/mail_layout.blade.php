<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=400" name="viewport"/>
    <title>Напоминаем об оплате</title>
    <style type="text/css">
        .ExternalClass {
            width: 100%;
        }

        img {
            border: 0 none;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a img {
            border: 0 none;
        }

        #outlook a {
            padding: 0;
        }

        #allrecords {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
            -webkit-font-smoothing: antialiased;
            line-height: 1.45;
        }

        #allrecords td {
            margin: 0;
            padding: 0;
        }

        #allrecords ul {
            -webkit-padding-start: 30px;
        }

        @media only screen and (max-width: 600px) {
            .r {
                width: 100% !important;
                min-width: 400px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .t-emailBlock {
                display: block !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                width: 100% !important;
            }

            .t-emailBlockPadding {
                padding-top: 15px !important;
            }

            .t-emailBlockPadding30 {
                padding-top: 30px !important;
            }

            .t-emailAlignLeft {
                text-align: left !important;
                margin-left: 0 !important;
            }

            .t-emailAlignCenter {
                text-align: center !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
        }</style>
</head>
<body cellpadding="0" cellspacing="0"
      style="padding: 0; margin: 0; border: 0; width:100%; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; background-color: #efefef;">
<!--allrecords-->
<table cellpadding="0" cellspacing="0" id="allrecords"
       style="width:100%; border-collapse:collapse; border-spacing:0; padding:0; margin:0; border:0;">
    <tr>
        <td style="background-color: #efefef; "><!--record_mail-->
            <table cellpadding="0" cellspacing="0" style="width:100%; border-collapse:collapse; border-spacing:0; margin:0; border:0;">
                <tr>
                    <td style="padding-left:15px; padding-right:15px; ">
                        <table align="center" class="r" id="recin102104971"
                               style="margin: 0 auto;border-spacing: 0;width:600px;">
                            <tr>
                                <td style="padding-top:0px;padding-bottom:0px;padding-left:0;padding-right:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                                        <tr>
                                            <td height="30px" style="height:30px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><!--/record-->


        @yield('content')


        <!--record_mail-->
            <table cellpadding="0" cellspacing="0" style="width:100%; border-collapse:collapse; border-spacing:0; margin:0; border:0;">
                <tr>
                    <td style="padding-left:15px; padding-right:15px; ">
                        <table align="center" class="r" id="recin102104976"
                               style="margin: 0 auto;border-spacing: 0;width:600px;">
                            <tr>
                                <td style="padding-top:0px;padding-bottom:0px;padding-left:0;padding-right:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                                        <tr>
                                            <td height="30px" style="height:30px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><!--/record--><!--record_mail-->
            <table cellpadding="0" cellspacing="0" style="width:100%; border-collapse:collapse; border-spacing:0; margin:0; border:0;">
                <tr>
                    <td style="padding-left:15px; padding-right:15px; ">
                        <table align="center" class="r"
                               id="recin102104977"
                               style="margin: 0 auto;background-color:#ffffff;border-spacing: 0;width:600px;">
                            <tr>
                                <td style="padding-top:45px;padding-bottom:45px;padding-left:30px;padding-right:30px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;"
                                                       width="100%">
                                                    <tr>
                                                            <td><a href="https://lms.space">LMS.space</a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><!--/record--><!--record_mail-->
            <table cellpadding="0" cellspacing="0" style="width:100%; border-collapse:collapse; border-spacing:0; margin:0; border:0;">
                <tr>
                    <td style="padding-left:15px; padding-right:15px; ">
                        <table align="center" class="r" id="recin102104980"
                               style="margin: 0 auto;border-spacing: 0;width:600px;">
                            <tr>
                                <td style="padding-top:0px;padding-bottom:0px;padding-left:0;padding-right:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                                        <tr>
                                            <td height="30px" style="height:30px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><!--/record--> </td>
    </tr>
</table><!--/allrecords--></body>
</html>
