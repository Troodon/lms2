<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>LMS</title>

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#18639d">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#18639d">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <meta name="description" content="LMS"/>
    <link rel="canonical" href="https://lms.space/"/>
    <meta name="author" content="lms.space">

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <link rel="stylesheet" href="/static/css/material-dashboard.css" type="text/css" media="screen"/>
</head>
<body>
<div id="app"></div>
<script src="http://localhost:8080/app.js"></script>
</body>
</html>
