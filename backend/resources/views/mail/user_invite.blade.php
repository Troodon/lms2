@extends('partials.mail_layout')

@section('content')
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto;">
        <!-- 1 Column Text + Button : BEGIN -->
        <tr>
            <td style="background-color: #ffffff;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"
                       id="mail_content_body">
                    <tr>
                        <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                            <h1 style="margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #333333; font-weight: normal;">
                                Добро пожаловать в команду организации &laquo;{{ $site['name'] }}&raquo;</h1>
                            @if ($password)
                            <p style="margin: 0 0 10px;">Для получения доступа в панель управления перейдите
                                <a href="{{ $config['url'] }}">по этой ссылке</a>
                            </p>
                            <p style="margin: 0 0 10px;">
                                <b>Логин</b>: {{ $user['email'] }}
                            </p>
                            <p style="margin: 0 0 10px;">
                                <b>Пароль</b>: {{ $password }}
                            </p>
                            @else
                                <p style="margin: 0 0 10px;">Для получения доступа авторизуйтесь в системе
                                    <a href="{{ $config['url'] }}">по этой ссылке</a>
                                </p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0 20px 20px;">
                            <!-- Button : BEGIN -->
                            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0"
                                   style="margin: auto;">
                                <tr>
                                    <td class="button-td button-td-primary"
                                        style="border-radius: 4px; background: #222222;">
                                        @if ($password)
                                        <a class="button-a button-a-primary" href="{{ $config['url'] }}"
                                           style="background: #222222; border: 1px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">Перейти
                                            к управлению</a>
                                        @else
                                            <a class="button-a button-a-primary"
                                               href="{{ $config['url'] }}signup?user={{ $user['id'] }}&hash={{ $user_hash  }}"
                                               style="background: #222222; border: 1px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">Перейти
                                                к регистрации</a>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                            <!-- Button : END -->
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <!-- 1 Column Text + Button : END -->

        <!-- Clear Spacer : BEGIN -->
        <tr>
            <td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px;">
                &nbsp;
            </td>
        </tr>
        <!-- Clear Spacer : END -->
    </table>
@endsection
