<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use controllers\IndexController;
use controllers\WebhookController;

Auth::routes();

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'IndexController@index');
    Route::post('/yandex-webhook', 'WebhookController@yandex');
    Route::get('/billing', 'IndexController@index');
    Route::get('/tariffs', 'IndexController@index');
    Route::get('/signup', 'IndexController@index');
    Route::get('/login', 'IndexController@index');
    Route::get('/terms', 'IndexController@index');
    Route::get('/billing/payment/{paymentHash}', 'IndexController@payment');
    Route::get('/profile', 'IndexController@index');
    Route::get('/{site}', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/welcome', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/design', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/ftp', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/team', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/config', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/settings', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/users', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/users/{user}', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/courses', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/courses/{course}', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/courses/{course}/units', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/courses/{course}/units/{item}', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/files', 'IndexController@index')->where('site', '[0-9]+');
    Route::get('/{site}/files/{any}', 'IndexController@index')->where('site', '[0-9]+');
});

//
//Route::get('/home', 'HomeController@index')->name('home');
