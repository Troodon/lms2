<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['api', 'return-json'], 'namespace' => '\App\Http\Controllers\Api'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/userinfo', 'AuthController@info');
        Route::post('/order', 'AuthController@order');
        Route::post('/{site}/team/delete', 'SitesController@teamDelete')->where('site', '[0-9]+');
        Route::post('/{site}', 'SitesController@update')->where('site', '[0-9]+');
        Route::get('/payment/{paymentId}', 'AuthController@payment')->where('paymentId', '[0-9a-z]+');
        Route::post('/sites/create', 'SitesController@create');
        Route::post('/{site}/pages/new', 'PagesController@create')->where('site', '[0-9]+');
        Route::get('/{site}/pages/{page}', 'PagesController@show')->where('page', '[0-9]+')->where('site', '[0-9]+');
        Route::post('/{site}/pages/{page}', 'PagesController@update')->where('page', '[0-9]+')->where('site', '[0-9]+');
        Route::get('/{site}/pages', 'PagesController@index')->where('site', '[0-9]+');
        Route::post('/{site}/courses/new', 'CoursesController@create')->where('site', '[0-9]+');
        Route::get('/{site}/courses/{course}/units', 'CoursesController@items')->where('site', '[0-9]+');
        Route::post('/{site}/courses/{course}/units/new', 'CoursesController@createItem')->where('site', '[0-9]+');
        Route::get('/{site}/courses/{course}/units/{course_unit}', 'CoursesController@showItem')->where('course_unit', '[0-9]+')->where('site', '[0-9]+');
        Route::post('/{site}/courses/{course}/units/{course_unit}', 'CoursesController@updateItem')->where('course_unit', '[0-9]+')->where('site', '[0-9]+');
        Route::get('/{site}/courses/{course}', 'CoursesController@show')->where('site', '[0-9]+');
        Route::post('/{site}/courses/{course}', 'CoursesController@update')->where('site', '[0-9]+');
        Route::get('/{site}/courses', 'CoursesController@index')->where('site', '[0-9]+');
        Route::post('/{site}/team/invite', 'SitesController@teamInvite')->where('site', '[0-9]+');
        Route::get('/{site}/team', 'SitesController@team')->where('site', '[0-9]+');
        Route::get('/{site}/ftp', 'SitesController@ftp')->where('site', '[0-9]+');
        Route::post('/{site}/ftp', 'SitesController@ftpSave')->where('site', '[0-9]+');
        Route::get('/{site}/config', 'SitesController@config')->where('site', '[0-9]+');
        Route::post('/{site}/config', 'SitesController@configSave')->where('site', '[0-9]+');
        Route::post('/{site}/purge_cdn', 'SitesController@purgeCDN')->where('site', '[0-9]+');
        Route::get('/{site}', 'SitesController@show');
        Route::get('/{site}/storage/read', 'StorageController@show')->where('site', '[0-9]+');
        Route::post('/{site}/storage/update', 'StorageController@update')->where('site', '[0-9]+');
        Route::post('/{site}/storage/upload', 'StorageController@upload')->where('site', '[0-9]+');
        Route::post('/{site}/storage/delete', 'StorageController@delete')->where('site', '[0-9]+');
        Route::get('/{site}/storage', 'StorageController@index')->where('site', '[0-9]+');
    });
    Route::post('/auth', 'AuthController@login');
    Route::post('/register', 'AuthController@register');
});


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
