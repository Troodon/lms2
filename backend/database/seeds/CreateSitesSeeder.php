<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class CreateSitesSeeder
 */
class CreateSitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'domain'    => 'sreda.local',
                'name'      => 'Среда Обучения',
                'tariff'    => 0,
                'active_to' => '2050-10-01'
            ],
        ];

        foreach ($user as $key => $value) {
            DB::table('site')->insert([
                'id'         => 1,
                'domain'     => $value['domain'],
                'name'       => $value['name'],
                'tariff'     => $value['tariff'],
                'active_to'  => $value['active_to'],
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ]);
        }
    }
}
