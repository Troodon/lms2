<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'id'          => 1,
                'email'       => 'root@localhost',
                'password'    => 'qwerty',
                'phone'       => '',
                'firstname'   => '',
                'middle_name' => '',
                'surname'     => '',
                'meta'     => '',
                'status'      => 'ok'
            ],
        ];

        foreach ($user as $key => $value) {
            DB::table('user')->insert([
                'id'          => $value['id'],
                'status'      => $value['status'],
                'email'       => $value['email'],
                'phone'       => $value['phone'],
                'firstname'   => $value['firstname'],
                'middle_name' => $value['middle_name'],
                'surname'     => $value['surname'],
                'meta'     => $value['meta'],
                'password'    => Hash::make($value['password']),
                'created_at'  => DB::raw('NOW()'),
                'updated_at'  => DB::raw('NOW()'),
            ]);
        }
    }
}
