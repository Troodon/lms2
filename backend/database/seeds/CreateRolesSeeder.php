<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'user_id' => 1,
                'site_id' => 1,
                'status'  => 'ok',
                'role'    => 'root',
            ],
        ];

        foreach ($roles as $key => $role) {
            DB::table('role')->insert([
                'site_id'    => $role['site_id'],
                'user_id'    => $role['user_id'],
                'meta'       => serialize([]),
                'role'       => $role['role'],
                'status'     => $role['status'],
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ]);
        }
    }
}
