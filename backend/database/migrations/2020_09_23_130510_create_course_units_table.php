<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCourseUnitsTable
 */
class CreateCourseUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_unit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('site_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->string('status', 50)->default('draft');
            $table->string('type', 50)->default('none');
            $table->string('name');
            $table->text('description');
            $table->longText('content');
            $table->longText('meta')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_unit');
    }
}
