/* TEAM */
    Your title: Red Thread Design Studio
    Site: www.redthread.studio, talk@redthread.studio
    Location: Moscow, Russia

    Web designer: Valeriy Shapovalov
	Twitter: @rotebaron
	From:Moscow, Russia

/* SITE */
    Last update: 2020/03/29
    Standards: HTML5, CSS3
    Components: jQuery, TweenMax, ScrollMagic, Smooth scrollbar
    Software: NodeJS, MongoDB, NPM
    Language: English
    Doctype:HTML5