<?php

namespace App\Jobs;

use App\Models\Course;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class SearchRebuildFeed
 * @package App\Jobs
 */
class SearchRebuildCourse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $feedId;

    /**
     * Create a new job instance.
     *
     * @param $feedId
     */
    public function __construct($feedId)
    {
        $this->feedId = +$feedId;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        /** @var Course $Feed */
        $Feed = Course::where([['id', '=', $this->feedId]])->first();

        if (!$Feed) {
            throw new Exception('Ошибка доступа');
        }

        $Feed->createSearchIndex(true);
        $Feed->reindexItemsSearch();
    }
}
