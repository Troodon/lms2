<?php

namespace App\Library;


use App\CFG;
use Exception;

/**
 * Class Utils
 */
class Utils
{
    /**
     * @param $siteId
     * @return bool|mixed
     */
    public static function reloadSite($siteId)
    {
        try {
            $ch = curl_init();
            $hostUrl = !!CFG::get('platform:port') ? CFG::get('platform:host') . ":" . CFG::get('platform:port') : CFG::get('platform:host');

            curl_setopt($ch, CURLOPT_URL, "http://{$hostUrl}/reload?site_id={$siteId}");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, '{"site_id": ' . $siteId . '}');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);
            curl_close($ch);

            if ($server_output && $response = json_decode($server_output)) {
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    /**
     * @param $number
     * @return string|string[]|null
     */
    public static function formatPhoneNumber($number)
    {
        $num = self::formatPhone($number);
        $len = strlen($num);

        if ($len == 7) $num = preg_replace('/([0-9]{2})([0-9]{2})([0-9]{3})/', '$1 $2 $3', $num);
        elseif ($len == 8) $num = preg_replace('/([0-9]{3})([0-9]{2})([0-9]{3})/', '$1 - $2 $3', $num);
        elseif ($len == 9) $num = preg_replace('/([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{2})/', '$1 - $2 $3 $4', $num);
        elseif ($len == 10) $num = preg_replace('/([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{3})/', '$1 - $2 $3 $4', $num);
        elseif ($len == 11) $num = preg_replace('/([0-9])([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})/', '+$1 ($2) $3 $4-$5', $num);

        return $num;
    }

    /**
     * Форматирует телефонный номер
     *
     * @param $number
     * @return string
     */
    public static function formatPhone($number)
    {
        $phone = trim($number);
        $phone = preg_replace('#\D#', '', $phone);
        $phone = preg_replace('#^89#', '79', $phone);
        $phone = preg_replace('#^849#', '749', $phone);

        return $phone;
    }

    /**
     * @param $num
     * @param bool $withRub
     * @return string
     */
    function text_number($num, $withRub = true)
    {
        $nul = 'ноль';
        $ten = [
            ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
            ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ];
        $a20 = ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];
        $tens = [2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
        $hundred = ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];
        $unit = [ // Units
                  ['копейка', 'копейки', 'копеек', 1],
                  ['рубль', 'рубля', 'рублей', 0],
                  ['тысяча', 'тысячи', 'тысяч', 1],
                  ['миллион', 'миллиона', 'миллионов', 0],
                  ['миллиард', 'милиарда', 'миллиардов', 0],
        ];
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) $out[] = Utils::morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        } else $out[] = $nul;

        if (!!$withRub) {
            $out[] = Utils::morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub

            if ($kop > 0) {
                $out[] = $kop . ' ' . Utils::morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
            }
        }

        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * @param $n
     * @param $f1
     * @param $f2
     * @param $f5
     * @return mixed
     */
    public function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;

        return $f5;
    }
}
