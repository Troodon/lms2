<?php

namespace App\Library;

use App\CFG;
use easmith\selectel\storage\integera;
use easmith\selectel\storage\SelectelContainer;
use Exception;
use Jenssegers\Date\Date;

/**
 * Class TabulaStorage
 */
class TabulaStorage
{
    /**
     * @var TSelectelStorage
     */
    protected $selectelStorage;

    /**
     * @var int
     */
    protected $siteId;

    protected $containerName;

    /**
     * @var SelectelContainer
     */
    protected $container;

    /**
     * TabulaStorage constructor.
     * @param $container
     * @param bool $init
     */
    public function __construct($container = null, $init = false)
    {
        if (!$container || CFG::get('dev')) $container = CFG::get('selectel:storage:default');

        $this->selectelStorage = new TSelectelStorage(CFG::get('selectel:storage:user'), CFG::get('selectel:storage:key'));
        $this->containerName = is_numeric($container) ? CFG::get('selectel:storage:prefix') . $container : $container;

        if (!!$init) {
            $this->container = $this->selectelStorage->createContainer($this->containerName);
        } else {
            $this->container = $this->selectelStorage->getContainer($this->containerName);
        }
    }

    /**
     * @param $filename
     * @param bool $beautify
     * @return string
     */
    public static function filterFilename($filename, $beautify = true): string
    {
        // sanitize filename
        $filename = preg_replace(
            '~
        [<>:"/\\|?*]|            # file system reserved https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
        [\x00-\x1F]|             # control characters http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247%28v=vs.85%29.aspx
        [\x7F\xA0\xAD]|          # non-printing characters DEL, NO-BREAK SPACE, SOFT HYPHEN
        [#\[\]@!$&\'()+,;=]|     # URI reserved https://tools.ietf.org/html/rfc3986#section-2.2
        [{}^\~`]                 # URL unsafe characters https://www.ietf.org/rfc/rfc1738.txt
        ~x',
            '-', $filename);
        // avoids ".", ".." or ".hiddenFiles"
        $filename = ltrim($filename, '.-');
        // optional beautification
        if ($beautify) $filename = self::beautifyFilename($filename);
        // maximize filename length to 255 bytes http://serverfault.com/a/9548/44086
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $filename = preg_replace("#.{$ext}$#", '', $filename);
        return mb_strcut($filename, 0, 255 - ($ext ? strlen($ext) + 1 : 0), mb_detect_encoding($filename)) . ($ext ? '.' . $ext : '');
    }

    /**
     * @param $filename
     * @return string
     */
    public static function beautifyFilename($filename): string
    {
        // reduce consecutive characters
        $filename = preg_replace(array(
            // "file   name.zip" becomes "file-name.zip"
            '/ +/',
            // "file___name.zip" becomes "file-name.zip"
            '/_+/',
            // "file---name.zip" becomes "file-name.zip"
            '/-+/'
        ), '-', $filename);
        $filename = preg_replace(array(
            // "file--.--.-.--name.zip" becomes "file.name.zip"
            '/-*\.-*/',
            // "file...name..zip" becomes "file.name.zip"
            '/\.{2,}/'
        ), '.', $filename);
        // lowercase for windows/unix interoperability http://support.microsoft.com/kb/100625
        $filename = mb_strtolower($filename, mb_detect_encoding($filename));
        // ".file-name.-" becomes "file-name"
        return trim($filename, '.-');
    }

    /**
     * @param $path
     * @return array
     */
    public function getPathInfo($path)
    {
        return $this->container->getFileInfo($path);
    }

    /**
     * @param $path
     * @param $content
     * @return array
     */
    public function writePath($path, $content)
    {
        return $this->container->putFileContents($content, $path);
    }

    /**
     * @param $path
     * @return integera
     */
    public function deletePath($path)
    {
        return $this->container->delete($path);
    }

    /**
     * @param string $path
     * @return array
     * @throws Exception
     */
    public function listPath($path = '/')
    {
        if (!$this->container) throw new Exception('Ошибка доступа файловного хранилища');
        $path = preg_replace('#^/(.+)#', '$1', $path);
        $path = (substr($path, -1) == '/' || $path == '') ? $path : "$path/";

        $result = [];
        $lastMarker = null;
        $response = $this->container->listFiles(
            10000,
            $lastMarker,
            $path,
            null,
            '/',
            'json');
        $response = json_decode($response, true);

        foreach ($response as $item) {
            if (isset($item['content_type']) && $item['content_type'] == 'application/directory') {
                continue;
            }

            if (isset($item['name'])) {
                $item['path'] = $item['name'];
                $item['name'] = basename($item['name']);
                $item['content_type'] = preg_replace('#(.+)(;.+)#', '$1', $item['content_type']);
                $item['last_modified'] = (new Date($item['last_modified']))->toIso8601String();
            } else if (isset($item['subdir'])) {
                $item['path'] = $item['subdir'];
                $item['name'] = basename($item['subdir']);
            }

            $result[] = $item;
        }

        return $result;
    }

    /**
     * @return SelectelContainer
     */
    public function Container()
    {
        return $this->container;
    }

    /**
     * @param string $path
     * @return array
     * @throws Exception
     */
    public function explore($path = '/')
    {
        if (!$this->container) throw new Exception('Ошибка доступа файловного хранилища');

//        $path = "static/{$path}";
//        $path = str_replace('//', '/', $path);
//        $path = str_replace('static/static/', 'static/', $path);
//        $path = str_replace('//', '/', $path);
        $result = [
            'directories' => [],
            'files'       => []
        ];
        $lastMarker = null;

        for ($i = 1; $i <= 10; $i++) {
            $response = $this->container->listFiles(10000, $lastMarker, null, $path);

            if (!$response || $response == ['']) {
                break;
            }

            foreach ($response as $iteratePath) {
                $iterateName = str_replace($path, '', $iteratePath);
                if (
                    !preg_match('#\w+\.\w+#', $iterateName)
                    && ($fileInfo = $this->container->getFileInfo($iteratePath))
                    && $fileInfo['content_type'] == 'application/directory') {
                    $result['directories'][] = [
                        'name' => $iterateName,
                        'path' => $iteratePath
                    ];
                } else {
                    $result['files'][] = [
                        'name' => $iterateName,
                        'path' => $iteratePath,
                    ];
                }

                $lastMarker = $iteratePath;
            }

        }

        return $result;
    }

    /**
     * @param $path
     * @return mixed
     * @throws Exception
     */
    public function getContent($path)
    {
        if (!$this->container) throw new Exception('Ошибка доступа файловного хранилища');

//        $path = "static/{$path}";
//        $path = str_replace('//', '/', $path);
//        $path = str_replace('static/static/', 'static/', $path);
//        $path = str_replace('//', '/', $path);

        $res = $this->container->getFile($path);

        return $res['content'];
    }

}
