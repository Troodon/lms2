<?php

namespace App\Library;

use App\CFG;
use App\Http\Controllers\Response\Sites\SiteResponse;
use App\Http\Controllers\Response\Users\UserResponse;
use App\Models\Site;
use App\User;
use Exception;
use SimpleEmailService;
use SimpleEmailServiceMessage;

/**
 * Class Notify
 */
class Notify
{
    /**
     * @var SimpleEmailService
     */
    protected static $mailService;

    /**
     * @var
     */
    protected static $from;

    /**
     * @param User $User
     * @param Site $Site
     * @param $password
     * @return array
     * @throws Exception
     */
    public static function inviteUser(User $User, Site $Site, $password)
    {
        self::init();
        $mailData = CFG::get("mails:inviteUser");
        $mailSubject = $mailData['subject'];
        $mailParams = [
            'mail_subject' => $mailSubject,
            'user'         => UserResponse::fromModel($User)->toArray(),
            'site'         => SiteResponse::fromModel($Site, false)->toArray(),
            'password'     => !!$password ? $password : null,
            'user_hash' => null,
            'config'       => CFG::get()
        ];
        try {
            $html = view("mail.{$mailData['template']}", $mailParams);
        } catch (Exception $e) {
            throw $e;
        }

        $m = new SimpleEmailServiceMessage();
        $m->setFrom(self::$from);
        $m->addTo($User->email);
        $m->setSubject($mailSubject);
        $m->setMessageFromString($html, $html);

        return self::$mailService->sendEmail($m);

    }

    /**
     * Инициализация класса
     */
    public static function init()
    {
        if (!!self::$mailService) return;

        self::$mailService = new SimpleEmailService(
            CFG::get('mail_services:amazon:key'),
            CFG::get('mail_services:amazon:secret'),
            CFG::get('mail_services:amazon:server')
        );
        self::$from = CFG::get('mail_services:amazon:from');
    }


    //region users

    /**
     * @param $message
     * @param $data
     * @return array
     * @throws Exception
     */
    public static function fatalNotify($message, $data = [])
    {
        self::init();
        $mailData = CFG::get("mails:fatalNotify");
        $mailSubject = $mailData['subject'];
        $mailParams = [
            'mail_subject' => $mailSubject,
            'message'      => $message,
            'data'         => $data
        ];
        try {
            $html = view("mail.{$mailData['template']}", $mailParams);
        } catch (Exception $e) {
            throw $e;
            ~rt($e->getMessage());
        }

        $m = new SimpleEmailServiceMessage();
        $m->setFrom(self::$from);
        $m->addTo('tehtroodon@ya.ru');
        $m->setSubject($mailSubject);
        $m->setMessageFromString($html, $html);

        return self::$mailService->sendEmail($m);
    }
    //endregion
}
