<?php

namespace App\Library;

use Exception;
use Throwable;

/**
 * Class TFatalException
 */
class TFatalException extends Exception
{
    /**
     * TFatalException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        try {
            Notify::fatalNotify($message);
        } catch (Exception $e) {

        }

        parent::__construct($message, $code, $previous);
    }

    public function render()
    {
        header('Content-Type: application/json');
        echo json_encode([
            'status' => 'error',
            'error'  => $this->getMessage()
        ]);
    }
}
