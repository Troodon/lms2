<?php

namespace App\Library;

use App\CFG;
use easmith\selectel\storage\SCurl;
use easmith\selectel\storage\SelectelContainer;
use easmith\selectel\storage\SelectelStorage;
use easmith\selectel\storage\SelectelStorageException;
use Exception;

class TSelectelStorage extends SelectelStorage
{

    /**
     * @return array
     */
    public function Token()
    {
        return $this->token;
    }

    /**
     * @param $containerName
     * @param $domain
     * @return SelectelContainer|mixed
     * @throws SelectelStorageException
     */
    public function setDomain($containerName, $domain)
    {
        $headers = ['X-Add-Container-Domains: ' . strtolower($domain)];
        $headers = array_merge($this->token, $headers);
        $info = SCurl::init($this->url . $containerName)
            ->setHeaders($headers)
            ->request("POST")
            ->getInfo();

        if (!in_array($info["http_code"], array(200, 201, 202, 204)))
            return $this->error($info["http_code"], __METHOD__);

        return $this->getContainer($containerName);
    }

    /**
     * @param $login
     * @param $password
     * @param $readContainers
     * @param $writeContainers
     * @return bool|mixed
     * @throws SelectelStorageException
     */
    public function saveUser($login, $password, $readContainers, $writeContainers)
    {
        $login = mb_strtolower($login);
        if (!preg_match('#^[a-z][\da-z]+$#', $login)) {
            throw new Exception("Login '{$login}' is not valid");
        }
        $headers = [
            'X-Auth-Key: ' . $password,
            'X-User-Active: on',
            'X-Store-Password: yes'
        ];
        if (!empty($readContainers)) {
            $headers[] = 'X-User-ACL-Containers-R: ' . join(', ', $readContainers);
        }
        if (!empty($writeContainers)) {
            $headers[] = 'X-User-ACL-Containers-W: ' . join(', ', $writeContainers);
        }

        $headers = array_merge($this->token, $headers);

        $info = SCurl::init("https://api.selcdn.ru/v1/users/{$login}")
            ->setHeaders($headers)
            ->request("PUT")
            ->getInfo();

        if (!in_array($info["http_code"], array(200, 201, 202, 204)))
            return $this->error($info["http_code"], __METHOD__);

        return true;
    }

    public function purgeCDN($urls)
    {
        $headers = $this->token;
        $curl = curl_init();
~rt($urls);
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.selcdn.ru/v1/cdn/purge",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 3000,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "PUT",
            CURLOPT_POSTFIELDS     => implode(',', $urls),
            CURLOPT_HTTPHEADER     => [
                "Host: auth.selcdn.ru",
                $this->token[0]
            ],
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            ~rt($response);
        }
//        ~rt($headers);
        $info = SCurl::init("https://api.selcdn.ru/v1/cdn/purge")
            ->setHeaders($headers)
            ->setParams($urls)
            ->request("PUT")
            ->getResult();
        ~rt($info, $urls);
        if (!in_array($info["http_code"], array(200, 201, 202, 204)))
            return $this->error($info["http_code"], __METHOD__);

        return true;
    }
}
