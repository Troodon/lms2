<?php

namespace App\Process;

use App\CFG;
use App\Models\Schedule;
use App\Models\Site;
use Exception;
use Requests;

/**
 * Class CDNProcess
 */
class CDNProcess
{
    /**
     * @param Schedule $Schedule
     * @throws Exception
     */
    public static function purgeCDN(Schedule $Schedule)
    {
        $Site = $Schedule->Site;

        if ($Site->usesCDN()) {
            self::purgeSelectelCDN($Site);
        }
    }

    /**
     * @param Site $Site
     * @return null
     * @throws Exception
     */
    protected static function purgeSelectelCDN(Site $Site)
    {
        $projectId = CFG::get('selectel:cdn:project');
        $cdnId = $Site->getMeta('uses_cdn');
        $resourceUrl = "https://api.selectel.ru/cdn/v1/projects/{$projectId}/resources/{$cdnId}";
        $res = Requests::get($resourceUrl, ['Accept' => 'application/json', 'X-token' => CFG::get('selectel:token')]);
        $activeRecordId = null;

        if (!!$res && !!$res->body) {
            $data = json_decode($res->body, true);
            if (isset($data['resource']['active_record_id'])) {
                $activeRecordId = $data['resource']['active_record_id'];
            }
        }

        if (!$activeRecordId) return null;

        $purgeUrl = "https://api.selectel.ru/cdn/v1/projects/{$projectId}/resources/{$cdnId}/records/{$activeRecordId}/purge";

        $res = Requests::put(
            $purgeUrl,
            [
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json',
                'X-token'      => CFG::get('selectel:token')
            ],
            '{"paths": []}');

        if ($res->status_code != 204) {
            throw new Exception("Ошибка сброса кэша сайта {$Site->id}");
        }
    }
}
