<?php

namespace App\Process;

use App\CFG;
use App\Library\TFatalException;
use App\Library\Utils;
use App\Models\Certificate;
use App\Models\Schedule;
use App\Models\Site;
use App\User;
use InvalidArgumentException;
use Jenssegers\Date\Date;
use LEClient\LEClient;
use LEClient\LEOrder;

/**
 * Class CertificateProcess
 */
class CertificateProcess
{
    /**
     * @param Schedule $Schedule
     */
    public static function installCertificate(Schedule $Schedule)
    {
        $User = User::where([['id', '=', +$Schedule->getMeta('user_id')]])->first();
        $Site = $Schedule->Site;
        $keysDir = self::getCertDir($Site, false);
        $client = new LEClient([$User->email], false, LEClient::LOG_OFF, $keysDir, '__account/');
        $acc = $client->getAccount();
        $accountPublicKey = file_get_contents("{$keysDir}/__account/public.pem");
        $accountPrivateKey = file_get_contents("{$keysDir}/__account/private.pem");
        $Site->setMetaValue('le_acc_private', $accountPrivateKey);
        $Site->setMetaValue('le_acc_public', $accountPublicKey);
        $order = $client->getOrCreateOrder($Site->domain, [$Site->domain, "www.{$Site->domain}"]);

        if (!$order->allAuthorizationsValid()) {
            $accountOrder = file_get_contents("{$keysDir}/order");
            $publicKey = file_get_contents("{$keysDir}/public.pem");
            $privateKey = file_get_contents("{$keysDir}/private.pem");
            $Site->setMetaValue('le_order', $accountOrder);
            $Site->setMetaValue('le_public', $publicKey);
            $Site->setMetaValue('le_private', $privateKey);
            // Get the HTTP challenges from the pending authorizations.
            $pending = $order->getPendingAuthorizations(LEOrder::CHALLENGE_TYPE_HTTP);
            // Walk the list of pending authorization HTTP challenges.
            if (!empty($pending)) {
                foreach ($pending as $challenge) {
                    $Site->interceptUrl("/.well-known/acme-challenge/{$challenge['filename']}", ['content' => $challenge['content'], 'type' => 'cert']);
                }
            }
        }

        $Site->save();
        self::deleteDir($keysDir);
        Utils::reloadSite($Site->id);

        // Запустим задачу проверки через 3 минуты
        $Schedule = Schedule::defaults($Site->id);
        $Schedule->type = 'install_certificate_verify';
        $Schedule->setMetaValue('user_id', +$User->id);
        $Schedule->date = (new Date())->modify('+3 minutes')->format('Y-m-d H:i:sO');
        $Schedule->save();
    }

    /**
     * @param Site $Site
     * @param bool $withCert
     * @return string
     */
    protected static function getCertDir(Site $Site, $withCert = true)
    {
        $keysDir = storage_path('framework/cache') . '/keys/' . uniqid();
        mkdir($keysDir, 0777, true);
        mkdir("{$keysDir}/__account", 0777, true);

        if (!!$withCert && $Site->getMeta('le_acc_private') && $Site->getMeta('le_acc_public')) {
            file_put_contents("{$keysDir}/__account/public.pem", $Site->getMeta('le_acc_public'));
            file_put_contents("{$keysDir}/__account/private.pem", $Site->getMeta('le_acc_private'));

            if ($Site->getMeta('le_order') && $Site->getMeta('le_public') && $Site->getMeta('le_private')) {
                file_put_contents("{$keysDir}/order", $Site->getMeta('le_order'));
                file_put_contents("{$keysDir}/public.pem", $Site->getMeta('le_public'));
                file_put_contents("{$keysDir}/private.pem", $Site->getMeta('le_private'));
            }
        }

        return $keysDir;
    }

    /**
     * @param $dirPath
     */
    public static function deleteDir($dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * @param Schedule $Schedule
     */
    public static function installCertificateVerify(Schedule $Schedule)
    {
        $User = User::where([['id', '=', +$Schedule->getMeta('user_id')]])->first();
        $Site = $Schedule->Site;
        $keysDir = self::getCertDir($Site);
        $certDir = CFG::get('certDir');
        $client = new LEClient([$User->email], false, LEClient::LOG_DEBUG, $keysDir, '__account/');
        $acc = $client->getAccount();
        $accountPublicKey = file_get_contents("{$keysDir}/__account/public.pem");
        $accountPrivateKey = file_get_contents("{$keysDir}/__account/private.pem");
        $Site->setMetaValue('le_acc_private', $accountPrivateKey);
        $Site->setMetaValue('le_acc_public', $accountPublicKey);
        $order = $client->getOrCreateOrder($Site->domain, [$Site->domain, "www.{$Site->domain}"]);

        if (!$order->allAuthorizationsValid()) {
            $accountOrder = file_get_contents("{$keysDir}/order");
            $publicKey = file_get_contents("{$keysDir}/public.pem");
            $privateKey = file_get_contents("{$keysDir}/private.pem");
            $Site->setMetaValue('le_order', $accountOrder);
            $Site->setMetaValue('le_public', $publicKey);
            $Site->setMetaValue('le_private', $privateKey);
            // Get the HTTP challenges from the pending authorizations.
            $pending = $order->getPendingAuthorizations(LEOrder::CHALLENGE_TYPE_HTTP);
            // Walk the list of pending authorization HTTP challenges.
            if (!empty($pending)) {
                foreach ($pending as $challenge) {
                    $order->verifyPendingOrderAuthorization($challenge['identifier'], LEOrder::CHALLENGE_TYPE_HTTP);
                }
            }
        }

        if ($order->allAuthorizationsValid()) {
            if (!$order->isFinalized()) $order->finalizeOrder();
            if ($order->isFinalized() && $order->getCertificate()) {
                $Certificate = Certificate::defaults($Site->id);
                $Certificate->domains = "{$Site->domain},www.{$Site->domain}";
                $Certificate->fullchain = file_get_contents("{$keysDir}/fullchain.crt");
                $Certificate->certificate = file_get_contents("{$keysDir}/certificate.crt");
                $Certificate->private_key = file_get_contents("{$keysDir}/private.pem");
                $Certificate->public_key = file_get_contents("{$keysDir}/public.pem");
                $Certificate->expires_at = (new Date())->modify('+4 months')->format('Y-m-d H:i:sO');
                $Certificate->email = $User->email;
                $Certificate->save();

                // Выключаем остальные сертификаты сайта
                Certificate::where([
                    ['id', '<>', +$Certificate->id],
                    ['site_id', '=', +$Certificate->site_id]
                ])
                    ->update(['status' => 'inactive']);

                if (!is_dir("{$certDir}/site{$Certificate->site_id}")) mkdir("{$certDir}/site{$Certificate->site_id}", 0755, true);
                file_put_contents("{$certDir}/site{$Certificate->site_id}/fullchain.pem", $Certificate->fullchain);
                file_put_contents("{$certDir}/site{$Certificate->site_id}/privkey.pem", $Certificate->private_key);

                $Site->setMetaValue('certificate', +$Certificate->id);
                $Site->interceptDelete('cert');
                $Site->save();
                Utils::reloadSite($Site->id);

                // Запустим задачу настройки nginx через 3 минуты
                $Schedule = Schedule::defaults($Site->id);
                $Schedule->type = 'rebuild_certificate_nginx';
                $Schedule->setMetaValue('user_id', +$User->id);
                $Schedule->date = (new Date())->modify('+3 minutes')->format('Y-m-d H:i:sO');
                $Schedule->save();
            }
        }
        self::deleteDir($keysDir);
    }

    /**
     * @param Schedule $Schedule
     * @throws TFatalException
     */
    public static function rebuildCertificateNginx(Schedule $Schedule)
    {
        $Certificates = Certificate::where([['status', '=', 'ok']])->get();
        $certDir = CFG::get('certDir');
        $nginxDir = CFG::get('nginxDir');
        $mask = "{$nginxDir}/*.conf";
        array_map('unlink', glob($mask));

        /** @var Certificate $Certificate */
        foreach ($Certificates as $Certificate) {
            if (!is_dir("{$certDir}/site{$Certificate->site_id}")) mkdir("{$certDir}/site{$Certificate->site_id}");
            file_put_contents("{$certDir}/site{$Certificate->site_id}/fullchain.pem", $Certificate->fullchain);
            file_put_contents("{$certDir}/site{$Certificate->site_id}/privkey.pem", $Certificate->private_key);
            $nginxParams = [
                'port'           => CFG::get('platform:port'),
                'domains'        => "{$Certificate->Site->domain} www.{$Certificate->Site->domain}",
                'fullchain_path' => realpath("{$certDir}/site{$Certificate->site_id}/fullchain.pem"),
                'privkey_path'   => realpath("{$certDir}/site{$Certificate->site_id}/privkey.pem")
            ];
            $nginxTemplate = view("ssl_nginx", $nginxParams);
            file_put_contents("{$nginxDir}/site{$Certificate->site_id}.conf", $nginxTemplate);
        }

        $output = exec('sudo /etc/init.d/nginx configtest');

        if (strpos(mb_strtolower($output), 'failed') !== false) {
            throw new TFatalException("Ошибка при установке сертификата!\n\n\n{$output}");
        }

        // Запустим задачу установки сертификата (обновление от LE)
        $renewSchedule = Schedule::defaults($Schedule->site_id);
        $renewSchedule->type = 'install_certificate';
        $renewSchedule->setMetaValue('user_id', +$Schedule->getMeta('user_id'));
        $renewSchedule->date = (new Date())->modify('+2 months')->format('Y-m-d H:i:sO');
        $renewSchedule->save();

        exec('sudo /etc/init.d/nginx reload');
    }
}
