<?php

namespace App\Process;

use App\CFG;
use App\Library\TabulaStorage;
use App\Models\Schedule;
use Exception;

/**
 * Class ScreenshotProcess
 */
class ScreenshotProcess
{
    /**
     * @param Schedule $Schedule
     */
    public static function siteHomePage(Schedule $Schedule)
    {
        $Site = $Schedule->Site;
        $apiParams = [
            'access_key'      => CFG::get('apiflash:key'),
            'format'          => 'jpeg',
            'fresh'           => 'true',
            'height'          => '800',
            'response_type'   => 'image',
            'thumbnail_width' => '300',
            'ttl'             => '22',
            'url'             => "http://{$Site->domain}",
            'width'           => '1400'
        ];
        $screenUrlApi = 'https://api.apiflash.com/v1/urltoimage?' . http_build_query($apiParams);

        try {
            $context = stream_context_create(array(
                'http' => array(
                    'ignore_errors' => true
                )
            ));
            $res = file_get_contents($screenUrlApi, false, $context);
        } catch (Exception $e) {
            $res = null;
        }

        if (strlen($res) > 1000) {
            $unicDir = substr(uniqid(), 0, 3);
            $unicKey = uniqid();
            $path = "/screenshot/{$unicDir}/{$unicKey}.jpg";
            $storage = new TabulaStorage('t0');
            $storage->writePath($path, $res);
            $Site->setMetaValue('screenshot', "https://t.tcdn.site{$path}");
            $Site->save();
        }
    }

}
