<?php

namespace App\Console\Commands;

use App\Models\Schedule;
use Exception;
use Illuminate\Console\Command;

class Schedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedules:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LMS schedules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    protected function checkPid()
    {
        $pidName = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', get_class($this)));
        $pidDir = storage_path('framework/cache');
        $pidFile = "{$pidDir}/{$pidName}.pid";


        if (is_file($pidFile)) {
            $pid = file_get_contents($pidFile);
            //проверяем на наличие процесса
            if (file_exists( "/proc/{$pid}" )) {
                exit();
            } else {
                //pid-файл есть, но процесса нет
                if (!unlink($pidFile)) {
                    //не могу уничтожить pid-файл. ошибка
                    exit(-1);
                }
            }
        }

        file_put_contents($pidFile, getmypid());
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkPid();

        /** @var  Schedule $Schedule */
        foreach (Schedule::fetchNext30() as $Schedule) {
            try {
                $Schedule->Run();
                $Schedule->status = 'ok';
            } catch (Exception $e) {
                $Schedule->status = 'error';
                $Schedule->logAction([
                    'm'    => $e->getMessage(),
                    'code' => $e->getCode()
                ]);
            }

            $Schedule->save();
        }
    }
}
