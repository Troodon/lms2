<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;

class RebuildFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LMS rebuild feed items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Feeds = Course::all();

        foreach ($Feeds as $Feed) {
            $Feed->createSearchIndex(true);
            $Feed->reindexItemsSearch();
        }
    }
}
