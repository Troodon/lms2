<?php

namespace App;

use App\Models\Role;
use App\Models\Site;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
use stdClass;

/**
 * @property int $id
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $firstname
 * @property string $middle_name
 * @property string $surname
 * @property string $status
 * @property string $meta
 * @property string $created_at
 * @property string $updated_at
 *
 *
 * @property Site[] $Sites
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    public $timestamps = false;



    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    protected $fillable = ['email', 'phone', 'password', 'firstname', 'middle_name', 'surname', 'status', 'meta', 'created_at', 'updated_at'];

    /**
     * @param $email
     * @param $password
     * @return User
     */
    public static function dive($email, $password)
    {
        $User = User::fromEmail($email);

        if (!$User) {
            $User = User::defaults();
            $User->email = $email;
            $User->password = $password;
            $User->status = 'ok';
            $User->save();
        }

        return $User;
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function fromEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * @return User
     */
    public static function defaults()
    {
        $User = new User();
        $User->status = 'ok';
        $User->meta = serialize(new stdClass());

        return $User;
    }

    /**
     * Get all of the tags for the post.
     */
    public function Sites()
    {
        return $this->belongsToMany(Site::class, 'role', 'user_id', 'site_id');

        return $this->morphToMany(Site::class, 'ok', 'role', 'user_id', 'site_id');
    }

    /**
     * @return mixed
     */
    public function siteRoles()
    {
        return Cache::get("user:roles:{$this->id}", function () {
            $Roles = Role::where([
                ['user_id', '=', $this->id],
                ['status', '=', 'ok']
            ]);
            $result = [];

            /** @var Role $Role */
            foreach ($Roles->get() as $Role) {
                $result[$Role->site_id] = [
                    'level'    => $Role->level,
                    'position' => $Role->position,
                ];
            }
            return $result;
        });
    }
}
