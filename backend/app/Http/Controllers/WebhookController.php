<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Exception;
use Illuminate\Http\Request;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use YooKassa\Model\NotificationEventType;

/**
 * Class WebhookController
 *
 * @OA\Tag(
 *     name="webhook",
 *     description="Вебхуки",
 * )
 */
class WebhookController extends Controller
{

    /**
     * @param Request $request
     * @return void
     * @OA\Get(
     *     path="/yandex-webhook",
     *     tags={"webhook"},
     *     summary="Вебхук оплат",
     *     description="Вебхук оплат",
     *     operationId="webhook",
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"webhook:read"}}}
     * )
     * @throws Exception
     */
    public function yandex(Request $request)
    {
        $requestBody = json_decode($request->getContent(), true);

        try {
            $notification = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
                ? new NotificationSucceeded($requestBody)
                : new NotificationWaitingForCapture($requestBody);

            if (isset($requestBody['object']['test']) && !!$requestBody['object']['test']) {
                throw  new Exception('Ошибка проверки оплаты');
            }
            $orderId = +$notification->object->metadata->order;

            $Payment = Payment::where('id', '=', $orderId)->first();
            $Payment->processPaid();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
