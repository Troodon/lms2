<?php

namespace App\Http\Controllers\Response;

use Phalcon\Cache\Backend\Apcu;
use Phalcon\Di;
use Phalcon\DiInterface;

/**
 * Class BaseResponse
 */
class BaseResponse
{

    /**
     * @param $path
     * @return string
     */
    public static function clearPath($path)
    {
//        $path = "static/{$path}";
//        $path = str_replace('//', '/', $path);
//        $path = str_replace('static/static', 'static/', $path);
//        $path = str_replace('//', '/', $path);
        $path = preg_replace('#^/(.+)#', '$1', $path);

        return '' . $path;
    }

    /**
     * @return DiInterface
     */
    protected static function DI()
    {
        return Di::getDefault();
    }

    /**
     * @return Apcu
     */
    protected static function cacheDI()
    {
        return (Di::getDefault())->get('cache');
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $result = [];

        foreach ($this as $key => $value) {
            if (!is_null($value)) {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}
