<?php

namespace App\Http\Controllers\Response\Sites;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Role;
use App\Models\Site;
use App\User;
use Exception;
use Illuminate\Http\Request;

/**
 * Class SiteResponse
 *
 * @OA\Schema(
 *     schema="Site",
 *     description="Схема модели сайта",
 *     title="Модель сайта",
 *     required={"id"}
 * )
 */
class SiteResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор сайта",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     description="Название сайта",
     *     title="Название сайта",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="Основной домен сайта",
     *     title="Основной домен сайта",
     * )
     *
     * @var string
     */
    public $domain;

    /**
     * @OA\Property(
     *     description="Тариф сайта",
     *     title="Тариф сайта",
     * )
     *
     * @var string
     */
    public $tariff;

    /**
     * @OA\Property(
     *     description="Сайт оплачен до указанной даты",
     *     title="Оплачен до",
     * )
     *
     * @var string
     */
    public $active_to;

    /**
     * @OA\Property(
     *     description="Отображение сайта на сабдомене www",
     *     title="Отображение сайта на сабдомене www",
     * )
     *
     * @var string
     */
    public $www_redirect;

    /**
     * @OA\Property(
     *     description="Отображение сайта по протоколу https",
     *     title="Отображение сайта по протоколу https",
     * )
     *
     * @var string
     */
    public $https_redirect;

    /**
     * @OA\Property(
     *     description="Получение сертифика",
     *     title="Получение сертифика",
     * )
     *
     * @var string
     */
    public $has_certificate;

    /**
     * @OA\Property(
     *     description="Отображение страниц со слешем",
     *     title="Отображение страниц со слешем",
     * )
     *
     * @var string
     */
    public $slash_redirect;

    /**
     * @OA\Property(
     *     description="Сертификат сайта",
     *     title="Сертификат сайта",
     * )
     *
     * @var string
     */
    public $certificate;

    /**
     * @OA\Property(
     *     description="Ссылка на последний скриншот сайта",
     *     title="Ссылка на последний скриншот сайта",
     * )
     *
     * @var string
     */
    public $screenshot;

    /**
     * @OA\Property(
     *     description="Сайт использует кэширующий CDN",
     *     title="Сайт использует кэширующий CDN",
     * )
     *
     * @var string
     */
    public $uses_cdn;


    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата создания сайта",
     *     title="Дата создания сайта",
     * )
     *
     * @var string
     */
    public $created_at;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата обновления сайта",
     *     title="Дата обновления сайта",
     * )
     *
     * @var string
     */
    public $updated_at;

    /**
     * @param User $User
     * @param Request $request
     * @return SiteResponse|array
     */
    public static function createSite(User $User, Request $request)
    {
        try {
            if (!request('name')) {
                throw new Exception('Укажите название');
            }

//        $domain = isset($rawRequest['domain']) && !!$rawRequest['domain'] ? $rawRequest['domain'] : '';
//        if (Domain::findDomain($domain)) {
//            throw new Exception('Эта ссылка уже занята');
//        }

            $Site = Site::defaults();
            $Site->name = request('name');
            $Site->save();

            Role::setRole($User->id, $Site->id, Role::ADMIN_LEVEL);

            $Site->domain = "t{$Site->id}.lms.space";

            if (!!request('www_redirect')) {
                $Site->setMetaValue('www_redirect', request('www_redirect'));
            }

            $Site->save();
            $Site->initSite();

            return [
                'success' => true,
                'site'    => self::fromModel($Site)->toArray()
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'error'   => $e->getMessage()
            ];
        }
    }

    /**
     * @param Site $Site
     * @return SiteResponse
     */
    public static function fromModel(Site $Site)
    {
//        ~rt($Site->created_at);
        $SiteResponse = new self();
        $SiteResponse->id = +$Site->id;
        $SiteResponse->name = $Site->name;
        $SiteResponse->domain = $Site->domain ? $Site->domain : '';
        $SiteResponse->screenshot = $Site->getMeta('screenshot');
        $SiteResponse->www_redirect = $Site->getMeta('www_redirect', '');
        $SiteResponse->slash_redirect = $Site->getMeta('slash_redirect', '');
        $SiteResponse->has_certificate = $Site->getMeta('has_certificate', false);
        $SiteResponse->certificate = $Site->getMeta('certificate', null);
        $SiteResponse->tariff = $Site->tariff;
        $SiteResponse->active_to = $Site->active_to;
        $SiteResponse->uses_cdn = $Site->usesCDN();
        $SiteResponse->created_at = $Site->created_at;
        $SiteResponse->updated_at = $Site->updated_at;

        if ($SiteResponse->has_certificate) {
            $SiteResponse->https_redirect = $Site->getMeta('https_redirect', '');
        } else {
            $SiteResponse->https_redirect = '';
        }

        return $SiteResponse;
    }
}
