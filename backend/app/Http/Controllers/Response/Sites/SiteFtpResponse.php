<?php

namespace App\Http\Controllers\Response\Sites;

use App\CFG;
use App\Http\Controllers\Response\BaseResponse;
use App\Library\TSelectelStorage;
use App\Models\Site;
use Exception;
use Jenssegers\Date\Date;

/**
 * Class SiteFtpResponse
 *
 * @OA\Schema(
 *     schema="SiteFtp",
 *     description="Схема ответа одного сайта",
 *     title="Сайт",
 *     required={"success"}
 * )
 */
class SiteFtpResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Логин для FTP",
     *     description="Логин для FTP",
     * )
     *
     * @var string
     */
    public $ftp_login;

    /**
     * @OA\Property(
     *     title="Пароль для FTP",
     *     description="Пароль для FTP",
     * )
     *
     * @var string
     */
    public $ftp_password = '';

    /**
     * @OA\Property(
     *     title="Не хранить пароль для FTP в открытом виде",
     *     description="Не хранить пароль для FTP в открытом виде",
     * )
     *
     * @var boolean
     */
    public $ftp_hide_password = false;

    /**
     * @OA\Property(
     *     title="FTP инициализирован",
     *     description="FTP инициализирован",
     * )
     *
     * @var string
     */
    public $ftp_created = false;

    /**
     * @param $siteId
     * @return array
     */
    public static function fromId($siteId)
    {
        $Response = new self();
        try {
            $Site = Site::where([['id', '=', $siteId]])->first();

            if ($Site->getMeta('ftp_created')) {
                $Response->ftp_created = true;

                if ($Site->getMeta('ftp_hide_password')) {
                    $Response->ftp_hide_password = true;
                } else {
                    $Response->ftp_password = $Site->getMeta('ftp_password', '');
                }
            }

            $Response->ftp_login = CFG::get('selectel:ftp:user_prefix') . "t{$Site->id}";
            $Response->success = true;
        } catch (Exception $e) {
            $Response->error = $e->getMessage();
        }

        return $Response->toArray();
    }

    /**
     * @param $siteId
     * @return array
     */
    public static function postById($siteId)
    {
        $Response = new self();
        try {
            if (!request('ftp_password') || strlen(request('ftp_password')) < 6) {
                throw new Exception('Минимальная длинна пароля 6 символов');
            }

            $Site = Site::where([['id', '=', $siteId]])->first();
            $FtpLogin = CFG::get('selectel:ftp:user_prefix') . "t{$Site->id}";
            $FtpPassword = request('ftp_password');
            $FtpHidePassword = !!request('ftp_hide_password') ? !!request('ftp_hide_password') : false;

            /** @var TSelectelStorage $Storage */
            $Storage = new TSelectelStorage(CFG::get('selectel:storage:user'), CFG::get('selectel:storage:key'));;
            $Storage->saveUser("t{$Site->id}", $FtpPassword, [$Site->getContainer()], [$Site->getContainer()]);

            if (!$FtpHidePassword) {
                $Response->ftp_password = $FtpPassword;

                $Site->setMetaValue('ftp_password', $FtpPassword);
                $Site->setMetaValue('ftp_hide_password', false);
            } else {
                $Response->ftp_hide_password = true;
                $Site->setMetaValue('ftp_password', null);
                $Site->setMetaValue('ftp_hide_password', true);
            }

            if (!$Site->getMeta('ftp_created')) {
                $Site->setMetaValue('ftp_created', (new Date())->toIso8601String());
            }

            $Site->save();

            $Response->ftp_login = $FtpLogin;
            $Response->success = true;
        } catch (Exception $e) {
            $Response->error = $e->getMessage();
        }

        return $Response->toArray();
    }
}
