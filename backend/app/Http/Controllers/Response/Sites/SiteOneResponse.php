<?php

namespace App\Http\Controllers\Response\Sites;

use App\CFG;
use App\Http\Controllers\Response\BaseResponse;
use App\Library\Utils;
use App\Models\Schedule;
use App\Models\Site;
use App\User;
use Exception;
use Jenssegers\Date\Date;

/**
 * Class SiteOneResponse
 *
 * @OA\Schema(
 *     schema="SiteOne",
 *     description="Схема ответа одного сайта",
 *     title="Сайт",
 *     required={"success"}
 * )
 */
class SiteOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Сайта",
     *     default=false,
     *     description="Участник",
     * )
     *
     * @var SiteResponse
     */
    public $site;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $siteId
     * @return SiteOneResponse
     */
    public static function fromId($siteId)
    {
        $result = new self();

        try {
            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!!$Site) {
                $result->site = SiteResponse::fromModel($Site)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Сайт не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @param $rawRequest
     * @param User $User
     * @return SiteOneResponse
     */
    public static function postById($siteId, User $User)
    {
        $result = new self();

        try {
            /** @var Site $Site */
            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!!$Site) {
                $installCertificate = !$Site->getMeta('has_certificate', false) && !!request('has_certificate');
                $Site->name = request('name');
                $Site->domain = filter_var(request('domain'), FILTER_VALIDATE_DOMAIN);
                $Site->setMetaValue('www_redirect', request('www_redirect'));
                $Site->setMetaValue('has_certificate', !!request('has_certificate'));
                $Site->setMetaValue('https_redirect', request('https_redirect'));
                $Site->setMetaValue('slash_redirect', request('slash_redirect'));


                if ($installCertificate) {
                    if (substr($Site->domain, -strlen('.lms.space')) === '.lms.space') {
                        throw new Exception('На доменах lms.space установлен сертификат!');
                    }

                    $ip = gethostbyname($Site->domain);
                    $ip = $ip == $Site->domain ? null : $ip;
                    $wwwIP = gethostbyname("www.{$Site->domain}");
                    $wwwIP = $wwwIP == "www.{$Site->domain}" ? null : $wwwIP;
                    $lms2IP = CFG::get('main_ip');

                    if ($ip != $lms2IP || $wwwIP != $lms2IP) {
                        throw new Exception("Доменам {$Site->domain} и www.{$Site->domain} необходимо прописать IP {$lms2IP}");
                    }

                    $Schedule = Schedule::defaults($Site->id);
                    $Schedule->type = 'install_certificate';
                    $Schedule->setMetaValue('user_id', +$User->id);
                    $Schedule->date = (new Date())->format('Y-m-d H:i:sO');
                    $Schedule->save();
                }

                $Site->save();
                Utils::reloadSite($Site->id);
                $result->site = SiteResponse::fromModel($Site)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Участник не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
