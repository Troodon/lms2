<?php

namespace App\Http\Controllers\Response\Sites;

use App\Http\Controllers\Response\BaseResponse;
use App\Http\Controllers\Response\Users\UserResponse;
use App\Library\Notify;
use App\Models\Role;
use App\Models\Site;
use App\User;
use Exception;
use Illuminate\Support\Facades\Cache;

/**
 * Class TeamResponse
 *
 * @OA\Schema(
 *     schema="Team",
 *     description="Схема модели ответа команды сайта",
 *     title="Команда сайта",
 *     required={"success"}
 * )
 */
class TeamResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Количество участников в выборке",
     *     default=0,
     *     description="Количество участников в выборке",
     * )
     *
     * @var integer
     */
    public $total;


    /**
     * @OA\Property(
     *     title="Массив с участниками",
     *     description="Массив с участниками",
     * )
     *
     * @var UserResponse[]
     */
    public $users;

    /**
     * @param $siteId
     * @param array $params
     * @return TeamResponse
     */
    public static function fromFilter($siteId, $params = [])
    {
        $TeamResponse = new self();

        try {
            $usersList = User::join('role', 'user.id', '=', 'role.user_id')
                ->where([
                    ['role.site_id', '=', $siteId],
                    ['role.status', '=', 'ok'],
                ])
                ->get('user.*');

            foreach ($usersList as $User) {
                $TeamResponse->users[] = UserResponse::fromModel($User, false)->toArray();
            }
            $TeamResponse->success = true;

        } catch (Exception $e) {
            $TeamResponse->success = false;
            $TeamResponse->error = $e->getMessage();
        }

        return $TeamResponse;
    }

    /**
     * @param $siteId
     * @param $userId
     * @return array
     */
    public static function deleteUser($siteId, $userId)
    {
        $response = [];

        try {
            $Role = Role::where([
                ['site_id', '=', $siteId],
                ['user_id', '=', $userId]
            ])->first();

            if (!!$Role) {
                $response['success'] = true;
                $Role->delete();
                Cache::forget("user:roles:{$userId}");
            } else {
                $response['success'] = false;
                $response['error'] = 'Не найдено';
            }
        } catch (Exception $e) {
            $response['success'] = false;
            $response['error'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param $siteId
     * @param $email
     * @return array
     */
    public static function inviteUser($siteId, $email)
    {
        $response = ['success' => false];

        try {
            $Site = Site::where([['id', '=', $siteId]])->first();
            $User = User::fromEmail($email);
            $password = !!$User ? null : self::randomPassword(10);
            $User = !!$User ? $User : User::dive($email, bcrypt($password));
            $Role = Role::where([
                ['site_id', '=', $siteId],
                ['user_id', '=', $User->id]
            ])->first();

            if (!$Role) {
                Role::setRole($User->id, $siteId, 1);
                Notify::inviteUser($User, $Site, $password);
                $response['success'] = true;
            } else {
                $response['error'] = 'Доступ уже есть';
            }
        } catch (Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param $length
     * @return string
     * @throws Exception
     */
    protected static function randomPassword($length)
    {
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@$%';
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

}
