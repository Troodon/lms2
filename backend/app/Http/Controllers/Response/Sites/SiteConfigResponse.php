<?php

namespace App\Http\Controllers\Response\Sites;

use App\Http\Controllers\Response\BaseResponse;
use App\Library\Utils;
use App\Models\Schedule;
use App\Models\Site;
use Exception;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use function request;

/**
 * Class SiteConfigResponse
 *
 * @OA\Schema(
 *     schema="SiteConfig",
 *     description="Схема ответа одного сайта",
 *     title="Сайт",
 *     required={"success"}
 * )
 */
class SiteConfigResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Сайта",
     *     default=false,
     *     description="Участник",
     * )
     *
     * @var SiteResponse
     */
    public $site;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $siteId
     * @return array
     */
    public static function fromId($siteId)
    {
        $response = [];
        try {
            /** @var Site $Site */
            $Site = Site::where([['id', '=', $siteId]])->first();

            $response = !!$Site->settings ? json_decode($Site->settings, true) : ['id' => +$Site->id];
        } catch (Exception $e) {
            $response['error'] = $e->getMessage();
        }
        if (!is_array($response)) {
            $response = ['id' => +$Site->id];
        }

        return $response;
    }

    /**
     * @param $siteId
     * @return array
     */
    public static function postById($siteId)
    {
        try {
            // try json settings
            $siteSettings = json_decode(request('settings'), true);

            if (!$siteSettings || !is_array($siteSettings) || array_keys($siteSettings) === range(0, count($siteSettings) - 1)) {
                throw new Exception('Ошибка при сохранении JSON объекта');
            }

            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!!$Site) {
                $Site->settings = json_encode($siteSettings);
                $Site->save();
                Utils::reloadSite($Site->id);

                return json_decode($Site->settings, true);
            } else {
                return null;
            }
        } catch (Exception $e) {
            return [
                'error' => $e->getMessage()
            ];
        }

        return null;
    }

    /**
     * @param $siteId
     * @return array
     */
    public static function purgeCDN($siteId)
    {
        $response = [];
        try {
            $User = auth::user();
            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!!$Site) {
                $Schedule = Schedule::defaults($Site->id);
                $Schedule->type = 'purge_cdn';
                $Schedule->setMetaValue('user_id', +$User->id);
                $Schedule->date = (new Date())->format('Y-m-d H:i:sO');
                $Schedule->save();

                return ['success' => true];
            }
        } catch (Exception $e) {
            return [
                'error' => $e->getMessage()
            ];
        }

        return $response;
    }

}
