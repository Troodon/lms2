<?php

namespace App\Http\Controllers\Response\Sites;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Site;
use Exception;
use Phalcon\Di;
use Phalcon\Mvc\Model\Manager as ModelsManager;

/**
 * Class SitesResponse
 *
 * @OA\Schema(
 *     schema="Sites",
 *     description="Схема модели ответа списка сайтов",
 *     title="Список сайтов",
 *     required={"success"}
 * )
 */
class SitesResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Количество участников в выборке",
     *     default=0,
     *     description="Количество участников в выборке",
     * )
     *
     * @var integer
     */
    public $total;


    /**
     * @OA\Property(
     *     title="Массив с сайтами",
     *     description="Массив с сайтами",
     * )
     *
     * @var SiteResponse[]
     */
    public $sites;

    /**
     * @param $orgId
     * @param array $params
     * @param int $page
     * @param int $perPage
     * @return SitesResponse
     */
    public static function fromFilter($orgId, $params = [], $page = 1, $perPage = 50)
    {
        $SitesResponse = new self();

        try {
            $phql = 'SELECT u.* FROM User u, Role r WHERE r.Site_id = :organization_id: AND r.user_id = u.id';
            $sqlParams = [
                'organization_id' => $orgId
            ];

            if (isset($params['status'])) {
                $phql .= ' AND u.status = :status:';
                $sqlParams['status'] = $params['status'];
            }

            if (isset($params['search'])) {
                $phql .= ' AND (u.firstname LIKE :search: OR u.surname LIKE :search:)';
                $sqlParams['search'] = "%{$params['search']}%";
            }

            /** @var ModelsManager $manager */
            $manager = Di::getDefault()->get('modelsManager');

            $limits = ' ORDER by u.id DESC LIMIT ' . (($page * $perPage) - $perPage) . ', ' . $perPage;
            $rows = $manager->executeQuery($phql . $limits, $sqlParams);
            $rowsCnt = $manager->executeQuery(str_replace('u.*', 'COUNT(*) as cnt', $phql), $sqlParams);
            $SitesResponse->sites = [];
            $SitesResponse->total = +$rowsCnt[0]['cnt'];

            /* @var  $Site Site */
            foreach ($rows as $Site) {
                $SitesResponse->sites[] = SiteResponse::fromModel($Site)->toArray();
            }

            $SitesResponse->success = true;
        } catch (Exception $e) {
            $SitesResponse->success = false;
            $SitesResponse->error = $e->getMessage();
        }

        return $SitesResponse;
    }
}
