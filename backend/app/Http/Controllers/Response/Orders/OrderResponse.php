<?php


namespace App\Http\Controllers\Response\Orders;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Payment;
use App\Models\Site;

/**
 * Class OrderResponse
 *
 * @OA\Schema(
 *     schema="Order",
 *     description="Схема модели заказа",
 *     title="Модель заказа",
 *     required={"id"}
 * )
 */
class OrderResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     description="Статус заказа",
     *     title="Статус",
     * )
     *
     * @var string
     */
    public $status;

    /**
     * @OA\Property(
     *     description="Сумма заказа",
     *     title="Сумма",
     * )
     *
     * @var float
     */
    public $amount;

    /**
     * @OA\Property(
     *     description="Количество месяцев для оплаты",
     *     title="Количество месяцев",
     * )
     *
     * @var int
     */
    public $months;

    /**
     * @OA\Property(
     *    property="sites",
     *    description="Сайты",
     *    type="array",
     *     @OA\Items(
     *       @OA\Property(
     *         property="id",
     *         description="Id Сайта",
     *         type="integer"
     *       ),
     *       @OA\Property(
     *         property="tariff",
     *         description="Тариф сайта",
     *         type="string"
     *       ),
     *     )
     * )
     *
     * @var string
     */
    public $sites;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата создания участника",
     *     title="Дата создания участника",
     * )
     *
     * @var string
     */
    public $created_at;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата обновления участника",
     *     title="Дата обновления участника",
     * )
     *
     * @var string
     */
    public $updated_at;

    /**
     * @param Payment $Payment
     * @param bool $full
     * @return OrderResponse
     */
    public static function fromPayment(Payment $Payment, $full = true)
    {
        if (!$Payment->getMeta('is_order')) {
            return null;
        }

        $UserResponse = new self();
        $UserResponse->id = +$Payment->id;
        $UserResponse->status = $Payment->status;
        $UserResponse->amount = +$Payment->amount;
        $UserResponse->months = $Payment->getMeta('months');
        $UserResponse->created_at = $Payment->created_at;
        $UserResponse->updated_at = $Payment->updated_at;
        $UserResponse->sites = [];

        foreach ($Payment->getMeta('sites') as $site) {
            /** @var Site $Site */
            $Site = Site::where([['id', '=', $site['id']]])->first();
            $UserResponse->sites[] = [
                'id'     => +$Site->id,
                'name'   => $Site->name,
                'domain' => $Site->domain,
                'tariff' => $site['tariff']
            ];
        }

        return $UserResponse;
    }
}
