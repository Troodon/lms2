<?php


namespace App\Http\Controllers\Response\Orders;

use App\CFG;
use App\Http\Controllers\Response\BaseResponse;
use App\Models\Payment;
use App\User;
use Exception;
use YooKassa\Client as YooKassaClient;

/**
 * Class OrderOneResponse
 *
 * @OA\Schema(
 *     schema="OrderOne",
 *     description="Схема ответа одного заказа",
 *     title="Заказ",
 *     required={"success"}
 * )
 */
class OrderOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Участник",
     *     default=false,
     *     description="Участник",
     * )
     *
     * @var OrderResponse
     */
    public $order;

    /**
     * @OA\Property(
     *     title="Ссылка для оплаты",
     *     description="Ссылка для оплаты заказа",
     * )
     *
     * @var string
     */
    public $payment_url;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $sites
     * @param $months
     * @param User $User
     * @return OrderOneResponse
     */
    public static function placeOrder($sites, $months, User $User)
    {
        $result = new self();

        try {
            $Payment = Payment::defaults($User->id);
            $siteRoles = $User->siteRoles();
            $tariffs = CFG::get('tariff');

            foreach ($sites as $site) {
                if (!isset($siteRoles[$site['id']])) {
                    throw new Exception("У вас нет доступа к сайту #{$site['id']}");
                }
                if (!isset($tariffs[$site['tariff']])) {
                    throw new Exception("У вас нет доступа к тарифу {$site['tariff']}");
                }

                $Payment->setSite($site['id'], $site['tariff'], $months);
                $Payment->amount += ($months >= 12 ? $tariffs[$site['tariff']]['amount_year'] : $tariffs[$site['tariff']]['amount']) * $months;
            }

            if ($Payment->amount < 1) {
                throw new Exception('Сумма оплаты меньше 1 рубля!');
            }

            $Payment->setMetaValue('months', $months);
            $Payment->setMetaValue('is_order', true);
            $Payment->save();

            $client = new YooKassaClient();
            $client->setAuth(CFG::get('yandex:kassa:shop_id'), CFG::get('yandex:kassa:secret'));
            $yandexPayment = $client->createPayment(
                [
                    'amount'       => [
                        'value'    => $Payment->amount,
                        'currency' => 'RUB',
                    ],
                    'confirmation' => [
                        'type'       => 'redirect',
                        'return_url' => $Payment->Link(),
                    ],
                    'metadata'     => [
                        'order' => +$Payment->id
                    ],
                    'capture'      => true,
                    'description'  => "Заказ #{$Payment->id}",
                ],
                uniqid('', true)
            );

            $result->order = OrderResponse::fromPayment($Payment)->toArray();
            $result->payment_url = $yandexPayment->confirmation->confirmation_url;
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param Payment $Payment
     * @return OrderOneResponse
     */
    public static function fromPayment(Payment $Payment)
    {
        $result = new self();

        try {
            $client = new YooKassaClient();
            $client->setAuth(CFG::get('yandex:kassa:shop_id'), CFG::get('yandex:kassa:secret'));

            $result->order = OrderResponse::fromPayment($Payment)->toArray();
            $yandexPayment = $client->createPayment(
                [
                    'amount'       => [
                        'value'    => $Payment->amount,
                        'currency' => 'RUB',
                    ],
                    'confirmation' => [
                        'type'       => 'redirect',
                        'return_url' => $Payment->Link(),
                    ],
                    'metadata'     => [
                        'order' => +$Payment->id
                    ],
                    'capture'      => true,
                    'description'  => "Заказ #{$Payment->id}",
                ],
                uniqid('', true)
            );

            $result->order = OrderResponse::fromPayment($Payment)->toArray();
            $result->payment_url = $yandexPayment->confirmation->confirmation_url;
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
