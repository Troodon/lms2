<?php

namespace App\Http\Controllers\Response\Storage;

use App\Http\Controllers\Response\BaseResponse;
use App\Library\TabulaStorage;
use App\Models\Site;
use Exception;

/**
 * Class FilesResponse.php
 *
 * @OA\Schema(
 *     schema="Files",
 *     description="Схема модели ответа списка файлов",
 *     title="Список файлов",
 *     required={"success"}
 * )
 */
class FilesResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;


    /**
     * @OA\Property(
     *     title="Массив с файлами",
     *     description="Массив с файлами",
     * )
     *
     * @var FileResponse[]
     */
    public $files;


    /**
     * @OA\Property(
     *     title="Массив с директориями",
     *     description="Массив с директориями",
     * )
     *
     * @var FileResponse[]
     */
    public $directories;

    /**
     * @param $siteId
     * @param $path
     * @return FilesResponse
     */
    public static function fromPath($siteId, $path)
    {
        $FilesResponse = new self();

        try {
            $path = self::clearPath($path);
            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!$Site) {
                throw new Exception('Сайт не найден');
            }

            $TabulaStorage = new TabulaStorage($siteId);
            $pathItems = $TabulaStorage->listPath($path);

            foreach ($pathItems as $path) {
                $FileResponse = FileResponse::fromCache($Site, $path);


                if (isset($path['subdir'])) {
                    $FilesResponse->directories[] = $FileResponse->toArray();
                } else {
                    $FilesResponse->files[] = $FileResponse->toArray();
                }
            }

            $FilesResponse->success = true;
        } catch (Exception $e) {
            $FilesResponse->success = false;
            $FilesResponse->error = $e->getTrace();
        }

        return $FilesResponse;
    }
}
