<?php

namespace App\Http\Controllers\Response\Storage;


use App\Http\Controllers\Response\BaseResponse;
use App\Library\TabulaStorage;
use App\Models\Site;
use Exception;
use Jenssegers\Date\Date;

/**
 * Class FileResponse
 *
 * @OA\Schema(
 *     schema="File",
 *     description="Схема модели файла",
 *     title="Модель файла",
 *     required={"path"}
 * )
 */
class FileResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     description="Путь файла",
     *     title="Путь файла",
     * )
     *
     * @var string
     */
    public $path;

    /**
     * @OA\Property(
     *     description="Название файла",
     *     title="Название файла",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="Публичная ссылка на файл",
     *     title="Урл страницы",
     * )
     *
     * @var string
     */
    public $url;

    /**
     * @OA\Property(
     *     description="Путь дериктории",
     *     title="Путь дериктории",
     * )
     *
     * @var boolean
     */
    public $is_dir;

    /**
     * @OA\Property(
     *     description="Контент страницы, доступный для ряда файлов",
     *     title="Контент страницы",
     * )
     *
     * @var string
     */
    public $content;

    /**
     * @OA\Property(
     *     description="Тип файла",
     *     title="Настройки страницы",
     * )
     *
     * @var string
     */
    public $content_type;

    /**
     * @OA\Property(
     *     title="Дата и время последнего изменения файла",
     *     description="Дата и время последнего изменения файла",
     * )
     *
     * @var string
     */
    public $last_modified;

    /**
     * @OA\Property(
     *     title="Рамер файла в байтах",
     *     description="Рамер файла в байтах",
     * )
     *
     * @var int
     */
    public $bytes;

    /**
     * @OA\Property(
     *     title="Файл можно отредактировать в платформе",
     *     description="Файл можно отредактировать в платформе",
     * )
     *
     * @var boolean
     */
    public $editable = false;

    /**
     * @param Site $Site
     * @param $pathCache
     * @return FileResponse
     */
    public static function fromCache(Site $Site, $pathCache)
    {
        $pathUrl = self::clearPath($pathCache['path']);
        $FileResponse = new self();
        $FileResponse->path = $pathUrl;
        $FileResponse->name = $pathCache['name'];
        $FileResponse->url = $Site->cdn() . '/' . $pathUrl;
        $FileResponse->is_dir = isset($pathCache['subdir']);

        if (isset($pathCache['subdir'])) {
            $FileResponse->content_type = 'application/directory';
        } else {
            $FileResponse->content_type = preg_replace('#(.+)(;.+)#', '$1', $pathCache['content_type']);
            $FileResponse->editable = self::checkEditable($FileResponse->name, $FileResponse->content_type);
            $FileResponse->bytes = $pathCache['bytes'];
            $FileResponse->last_modified = (new Date($pathCache['last_modified']))->toIso8601String();
        }

        return $FileResponse;
    }

    /**
     * @param $name
     * @param string $type
     * @return bool
     */
    protected static function checkEditable($name, $type = '')
    {
        $editable = ['css', 'js', 'txt', 'html', 'htm', 'markdown','md', 'json', 'csv', 'log', 'php', ''];
        $ext = mb_strtolower(pathinfo($name, PATHINFO_EXTENSION));

        return in_array($ext, $editable);
    }

    /**
     * @param Site $Site
     * @param $path
     * @param bool $withContent
     * @return FileResponse
     * @throws Exception
     */
    public static function fromPath(Site $Site, $path, $withContent = false)
    {
        $path = self::clearPath($path);
        $FileResponse = new self();
        $FileResponse->path = $path;
        $FileResponse->name = basename($path);
        $FileResponse->url = $Site->getPublicUrl() . '/' . $path;

//        $isDir = self::cacheDI()->get("s{$Site->id}:cd:{$path}");
//        $contentType = self::cacheDI()->get("s{$Site->id}:ct:{$path}");
//
//        if ($contentType && !$withContent) {
//            $FileResponse->is_dir = $isDir;
//            $FileResponse->content_type = $contentType;
//            $FileResponse->editable = self::checkEditable($FileResponse->name, $FileResponse->content_type);
//
//            return $FileResponse;
//        }


        $TabulaStorage = new TabulaStorage($Site->id);
        $pathInfo = $TabulaStorage->getPathInfo($path);

        if (!$pathInfo) {
            throw new Exception('Путь не обнаружен');
        }

        $FileResponse->content_type = $pathInfo['content_type'];
        $FileResponse->is_dir = $pathInfo['content_type'] == 'application/directory' ? true : false;
        $FileResponse->bytes = $pathInfo['bytes'];
        $FileResponse->last_modified = (new Date($pathInfo['last_modified']))->toIso8601String();

        if (!$FileResponse->is_dir) {
            $FileResponse->editable = self::checkEditable($FileResponse->name, $FileResponse->content_type);
        }
//        self::cacheDI()->save("s{$Site->id}:cd:{$path}", $FileResponse->is_dir);
//        self::cacheDI()->save("s{$Site->id}:ct:{$path}", $FileResponse->content_type);

        if ($FileResponse->editable && $withContent) {
            $FileResponse->content = $TabulaStorage->getContent($path);
        }


        return $FileResponse;
    }
}
