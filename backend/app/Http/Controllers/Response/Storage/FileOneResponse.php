<?php

namespace App\Http\Controllers\Response\Storage;


use App\Http\Controllers\Response\BaseResponse;
use App\Library\TabulaStorage;
use App\Models\Site;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class FileOneResponse
 *
 * @OA\Schema(
 *     schema="FileOne",
 *     description="Схема ответа одного файла",
 *     title="Файл",
 *     required={"success"}
 * )
 */
class FileOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Файл",
     *     default=false,
     *     description="Файл",
     * )
     *
     * @var FileResponse
     */
    public $file;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $siteId
     * @param $path
     * @return FileOneResponse
     */
    public static function fromPath($siteId, $path)
    {
        $result = new self();

        try {
            $Site = Site::where([['id', '=', $siteId]])->first();
            $result->file = FileResponse::fromPath($Site, $path, true)->toArray();
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @param $path
     * @param Request $request
     * @return FileOneResponse
     */
    public static function updatePath($siteId, $path, Request $request)
    {
        $result = new self();

        try {
            $Site = Site::where([['id', '=', $siteId]])->first();
            $TabulaStorage = new TabulaStorage($Site->id);
            $TabulaStorage->writePath($path, file_get_contents('php://input'));

            $result->file = FileResponse::fromPath($Site, $path, true)->toArray();
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }


    /**
     * @param $siteId
     * @param $path
     * @return FileOneResponse
     */
    public static function deletePath($siteId, $path)
    {
        $result = new self();

        try {
            $Site = Site::where([['id', '=', $siteId]])->first();
            $TabulaStorage = new TabulaStorage($Site->id);
            $TabulaStorage->deletePath($path);

            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }


    /**
     * @param $siteId
     * @param $path
     * @param File $file
     * @return FileOneResponse
     */
    public static function uploadPath($siteId, $path, File $file)
    {
        $result = new self();

        try {
            $pathInfo = explode('/', $path);
            $fileName = end($pathInfo);
            $path = str_replace($fileName, TabulaStorage::filterFilename($fileName), $path);
            $path = preg_replace('#^\/(.+)#', '$1', $path);
            $Site = Site::where([['id', '=', $siteId]])->first();
            $TabulaStorage = new TabulaStorage($Site->id);
            $TabulaStorage->writePath($path, file_get_contents($file->getRealPath()));

            $result->file = FileResponse::fromPath($Site, $path, false)->toArray();
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
