<?php

namespace App\Http\Controllers\Response\Pages;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Page;
use Exception;

/**
 * Class PagesResponse.php
 *
 * @OA\Schema(
 *     schema="Pages",
 *     description="Схема модели ответа списка страниц",
 *     title="Список сайтов",
 *     required={"success"}
 * )
 */
class PagesResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Количество участников в выборке",
     *     default=0,
     *     description="Количество участников в выборке",
     * )
     *
     * @var integer
     */
    public $total;


    /**
     * @OA\Property(
     *     title="Массив со страницами",
     *     description="Массив со страницами",
     * )
     *
     * @var PageResponse[]
     */
    public $pages;

    /**
     * @param $siteId
     * @return PagesResponse
     */
    public static function fromFilter($siteId)
    {
        $PagesResponse = new self();

        try {
            /* @var  $Page Page */
            foreach (Page::where([
                ['site_id', '=', $siteId],
                ['status', '=', 'ok'],
            ])->get() as $Page) {
                $PagesResponse->pages[] = PageResponse::fromModel($Page, false)->toArray();
            }
            $PagesResponse->success = true;
        } catch (Exception $e) {
            $PagesResponse->success = false;
            $PagesResponse->error = $e->getMessage();
        }

        return $PagesResponse;
    }
}
