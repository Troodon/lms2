<?php

namespace App\Http\Controllers\Response\Pages;

use App\Http\Controllers\Response\BaseResponse;
use App\Library\Utils;
use App\Models\Page;
use App\Models\Schedule;
use App\Models\Site;
use Exception;
use Jenssegers\Date\Date;

/**
 * Class PageOneResponse
 *
 * @OA\Schema(
 *     schema="PageOne",
 *     description="Схема ответа одной страницы",
 *     title="Страница",
 *     required={"success"}
 * )
 */
class PageOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Сайта",
     *     default=false,
     *     description="Участник",
     * )
     *
     * @var PagesResponse
     */
    public $page;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $siteId
     * @param $pageId
     * @return PageOneResponse
     */
    public static function fromId($siteId, $pageId)
    {
        $result = new self();

        try {
            $Page = Page::where([
                ['site_id', '=', $siteId],
                ['sid', '=', $pageId],
            ])->first();

            if (!!$Page) {
                $result->page = PageResponse::fromModel($Page)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Страница не найдена';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @param $pageId
     * @return PageOneResponse
     */
    public static function postById($siteId, $pageId)
    {
        $result = new self();

        try {
            $Page = Page::where([
                ['site_id', '=', $siteId],
                ['sid', '=', $pageId],
            ])->first();

            if (!!request('status') && $Page->parent == 0) {
                $result->error = 'Нельзя удалить корень';
            } else if (!!$Page) {
                if (!!request('status') && request('status') == 'archived') {
                    $Page->status = request('status');
                } else {
                    $Page->name = request('name');

                    if ($Page->parent !== 0) {
                        $Page->url = request('url');
                    }

                    $Page->disabled = +!!request('disabled');
                    $Page->content = request('content');
                    $Page->handler = !!request('handler') ? request('handler') : '';
                    $Page->settings = request('settings');
                    $Page->feed = (!!request('course')) ? request('course') : null;
                }
                // Проверка настроек
                if (!!$Page->settings) {
                    $pageSettings = json_decode($Page->settings, true);

                    if (isset($pageSettings['feeds'])) {
                        if (!is_array($pageSettings['feeds'])) {
                            throw new Exception('Ошибка: feeds должен быть объектом');
                        }
                        if (count(array_keys($pageSettings['feeds'])) >9) {
                            throw new Exception('Ошибка: Максимальное количество списков на странице – 10');
                        }
                        // @todo Сделать проверку полей
                    }
                }

                $Page->save();
                $Page->logVersion();

                if ($Page->parent == 0) {
                    // Запустим задачу создания скриншота через минуту
                    $Schedule = Schedule::defaults($Page->site_id);
                    $Schedule->type = 'screenshot';
                    $Schedule->date = (new Date())->modify('+1 minutes')->format('Y-m-d H:i:sO');
                    $Schedule->save();
                }
                Utils::reloadSite($Page->site_id);

                $result->page = PageResponse::fromModel($Page)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Страница не найдена';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @return PageOneResponse
     */
    public static function createPage($siteId)
    {
        $result = new self();

        try {
            $parentId = request('parent');
            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!$Site) {
                throw new Exception('Сайт не найден');
            }

            $parentPage = Page::where([
                ['site_id', '=', $siteId],
                ['sid', '=', $parentId],
            ])->first();

            if (!!$parentPage && $parentPage->site_id == $siteId) {
                $Page = Page::defaults($siteId);
                $Page->site_id = $Site->id;
                $Page->sid = $Site->sid;
                $Page->disabled = 1;
                $Page->url = uniqid('page');
                $Page->sort = $parentPage->getChildOrder();
                $Page->parent = +$parentId;
                $Page->feed = !!request('course') ? request('course') : null;
                $Page->save();

                $Site->sid++;
                $Site->save();
                Utils::reloadSite($Page->site_id);

                $result->page = PageResponse::fromModel($Page)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Страница не найдена';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
