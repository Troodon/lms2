<?php

namespace App\Http\Controllers\Response\Pages;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Page;

/**
 * Class PageResponse
 *
 * @OA\Schema(
 *     schema="Page",
 *     description="Схема модели страницы",
 *     title="Модель страницы",
 *     required={"id"}
 * )
 */
class PageResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор страницы",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор сайта",
     * )
     *
     * @var integer
     */
    public $site_id;

    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор родителя",
     * )
     *
     * @var integer
     */
    public $parent;

    /**
     * @OA\Property(
     *     description="Название страницы",
     *     title="Название страницы",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="Урл страницы",
     *     title="Урл страницы",
     * )
     *
     * @var string
     */
    public $url;

    /**
     * @OA\Property(
     *     description="Выключить страницу",
     *     title="Выключить страницу",
     * )
     *
     * @var boolean
     */
    public $disabled;

    /**
     * @OA\Property(
     *     description="Контент страницы",
     *     title="Контент страницы",
     * )
     *
     * @var string
     */
    public $content;

    /**
     * @OA\Property(
     *     description="Обработчик страницы",
     *     title="Обработчик страницы",
     * )
     *
     * @var string
     */
    public $handler;

    /**
     * @OA\Property(
     *     description="Настройки страницы",
     *     title="Настройки страницы",
     * )
     *
     * @var string
     */
    public $settings;

    /**
     * @OA\Property(
     *     description="Список страницы",
     *     title="Список страницы",
     * )
     *
     * @var string
     */
    public $feed;

    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Сортировка в списке",
     * )
     *
     * @var integer
     */
    public $sort;


    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата создания сайта",
     *     title="Дата создания сайта",
     * )
     *
     * @var string
     */
    public $created_at;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата обновления сайта",
     *     title="Дата обновления сайта",
     * )
     *
     * @var string
     */
    public $updated_at;

    /**
     * @param Page $Page
     * @param bool $full
     * @return PageResponse
     */
    public static function fromModel(Page $Page, $full = true)
    {
        $PageResponse = new self();
        $PageResponse->id = +$Page->sid;
        $PageResponse->site_id = +$Page->site_id;
        $PageResponse->parent = +$Page->parent;
        $PageResponse->disabled = !!$Page->disabled;
        $PageResponse->name = $Page->name;
        $PageResponse->url = $Page->url;
        $PageResponse->feed = $Page->feed;

        if ($full) {
            $PageResponse->content = $Page->content;
            $PageResponse->handler = $Page->handler;

            if (
                strlen($Page->settings)
                && $pageSettings = @json_decode($Page->settings, true)
            ) {
                $PageResponse->settings = is_array($pageSettings) ? @json_encode($pageSettings, JSON_PRETTY_PRINT) : '{}';
            } else {
                $PageResponse->settings = $Page->settings;
            }
        }

        $PageResponse->sort = +$Page->sort;
        $PageResponse->created_at = $Page->created_at;
        $PageResponse->updated_at = $Page->updated_at;

        return $PageResponse;
    }
}
