<?php

namespace App\Http\Controllers\Response\CourseUnits;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\CourseUnit;

/**
 * Class CourseUnit
 *
 * @OA\Schema(
 *     schema="CourseUnit",
 *     description="Схема модели элемента курса",
 *     title="Модель элемента курса",
 *     required={"id"}
 * )
 */
class CourseUnitResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор",
     * )
     *
     * @var integer
     */
    public int $id;

    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор сайта",
     * )
     *
     * @var integer
     */
    public int $site_id;

    /**
     * @OA\Property(
     *     format="int64",
     *     title="Курс",
     *     default=1,
     *     description="Идентификатор ленты",
     * )
     *
     * @var integer
     */
    public int $course_id;

    /**
     * @OA\Property(
     *     title="Статус элемента",
     *     description="Статус материала курса",
     * )
     *
     * @var string
     */
    public string $status;

    /**
     * @OA\Property(
     *     title="Тип",
     *     description="Тип материала курса",
     * )
     *
     * @var string
     */
    public string $type;

    /**
     * @OA\Property(
     *     description="Название",
     *     title="Название материала курса",
     * )
     *
     * @var string
     */
    public string $name;

    /**
     * @OA\Property(
     *     description="Название элемента",
     *     title="Название материала курса",
     * )
     *
     * @var string
     */
    public string $description;

    /**
     * @OA\Property(
     *     description="Теги элемента",
     *     title="Теги элемента",
     * )
     *
     * @var string
     */
    public $content;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата создания сайта",
     *     title="Дата создания сайта",
     * )
     *
     * @var string
     */
    public string $created_at;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата обновления сайта",
     *     title="Дата обновления сайта",
     * )
     *
     * @var string
     */
    public string $updated_at;

    /**
     * @param CourseUnit $CourseUnit $Feed
     * @return CourseUnitResponse
     */
    public static function fromModel(CourseUnit $CourseUnit): CourseUnitResponse
    {
        $CourseUnitResponse = new self();
        $CourseUnitResponse->id = +$CourseUnit->id;
        $CourseUnitResponse->site_id = +$CourseUnit->site_id;
        $CourseUnitResponse->course_id = +$CourseUnit->course_id;
        $CourseUnitResponse->status = $CourseUnit->status;
        $CourseUnitResponse->type = $CourseUnit->type;
        $CourseUnitResponse->name = $CourseUnit->name;
        $CourseUnitResponse->description = $CourseUnit->description;
        $CourseUnitResponse->content = $CourseUnit->content;
        $CourseUnitResponse->created_at = $CourseUnit->created_at;
        $CourseUnitResponse->updated_at = $CourseUnit->updated_at;

        return $CourseUnitResponse;
    }
}
