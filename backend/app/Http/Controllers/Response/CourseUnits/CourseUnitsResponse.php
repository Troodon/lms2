<?php

namespace App\Http\Controllers\Response\CourseUnits;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Course;
use App\Models\CourseUnit;
use Exception;

/**
 * Class CourseUnits
 *
 * @OA\Schema(
 *     schema="CourseUnits",
 *     description="Схема модели списка элементов курсов",
 *     title="Элементы курсов",
 *     required={"success"}
 * )
 */
class CourseUnitsResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Количество участников в выборке",
     *     default=0,
     *     description="Количество участников в выборке",
     * )
     *
     * @var integer
     */
    public $total;


    /**
     * @OA\Property(
     *     title="Элементы курса",
     *     description="Массив с элементами курсов",
     * )
     *
     * @var CourseUnitResponse[]
     */
    public $course_units;

    /**
     * @param $siteId
     * @param $courseId
     * @param int $skip
     * @param int $limit
     * @return CourseUnitsResponse
     */
    public static function fromFilter($siteId, $courseId, $skip = 0, $limit = 250)
    {
        $FeedItemsResponse = new self();

        try {
            /** @var Course $Course */
            $Course = Course::where([
                ['site_id', '=', $siteId],
                ['id', '=', $courseId],
            ])->first();

            if (!$Course || $Course->site_id != $siteId) {
                throw new Exception('Список не найден');
            }

            $selector = CourseUnit::where([
                ['site_id', '=', $siteId],
                ['course_id', '=', $Course->id],
                ['status', '<>', 'deleted'],
            ]);

            $FeedItemsResponse->total = +$selector->count();
            if (request('q')) {
//                $searchText = trim(request('q'));
//                $query = [
//                    'index' => "feed{$Feed->id}",
//                    'body'  => [
//                        "from"    => 0,
//                        "size"    => 1000,
//                        '_source' => [
//                            '_id'
//                        ],
//                        'query'   => [
//                            'query_string' => [
//                                'query'  => "*{$searchText}*",
//                                "fields" => ['name^5', 'key^5', 'tags^5', '_all'],
//                            ]
//                        ]
//                    ]
//                ];
//                echo json_encode($query);
//                exit();
//                /** @var Client $Elasticsearch */
//                $Elasticsearch = resolve(Client::class);
//                $searched = $Elasticsearch->search($query);
//                ~rt($searched);
            }

            /* @var  $FeedItem CourseUnit */
            foreach ($selector->offset($skip)->limit($limit)->get() as $Course) {
                $FeedItemsResponse->course_units[] = CourseUnitResponse::fromModel($Course)->toArray();
            }
            $FeedItemsResponse->success = true;
        } catch (Exception $e) {
            $FeedItemsResponse->success = false;
            $FeedItemsResponse->error = $e->getMessage();
        }

        return $FeedItemsResponse;
    }
}
