<?php

namespace App\Http\Controllers\Response\CourseUnits;

use App\Http\Controllers\Response\BaseResponse;
use App\Library\Utils;
use App\Models\Course;
use App\Models\CourseUnit;
use App\Models\Site;
use Exception;

/**
 * Class CourseUnitOne
 *
 * @OA\Schema(
 *     schema="CourseUnitOne",
 *     description="Схема ответа одного элемента",
 *     title="Элемент",
 *     required={"success"}
 * )
 */
class CourseUnitOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Сайта",
     *     default=false,
     *     description="Элемент курса",
     * )
     *
     * @var CourseUnitResponse
     */
    public $course_unit;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $siteId
     * @param $feedKey
     * @param $id
     * @return CourseUnitOneResponse
     */
    public static function fromId($siteId, $feedKey, $id)
    {
        $result = new self();

        try {
            $FeedItem = CourseUnit::where([
                ['site_id', '=', $siteId],
                ['id', '=', $id],
            ])->first();

            if (!!$FeedItem) {
                $result->course_unit = CourseUnitResponse::fromModel($FeedItem)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Лист не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @param $feedKey
     * @param $id
     * @return CourseUnitOneResponse
     */
    public static function postById($siteId, $courseId, $id)
    {
        $result = new self();

        try {
            /** @var CourseUnit $CourseUnit */
            $CourseUnit = CourseUnit::where([
                ['site_id', '=', $siteId],
                ['id', '=', $id],
            ])->first();

            if (!!$CourseUnit && $CourseUnit->course_id == $courseId) {
                if (!request('name') && request('status') == 'deleted') {
                    $CourseUnit->status = request('status');
                } else {
                    $CourseUnit->name = request('name');
                    $CourseUnit->content = request('content');
                    $CourseUnit->type = request('type');

                    if (request('status')) {
                        $CourseUnit->status = request('status');
                    }
                }
                $CourseUnit->save();
//                $CourseUnit->logVersion();

                Utils::reloadSite($CourseUnit->site_id);
                $result->course_unit = CourseUnitResponse::fromModel($CourseUnit)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Элемент листа не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @param $courseId
     * @return CourseUnitOneResponse
     */
    public static function createCourseUnit($siteId, $courseId)
    {
        $result = new self();

        try {
            /** @var Site $Site */
            $Site = Site::where([['id', '=', $siteId]])->first();
            /** @var Course $Course */
            $Course = Course::where([['id', '=', $courseId], ['site_id', '=', $siteId]])->first();

            if (!$Site || !$Course) {
                throw new Exception('Ошибка доступа');
            }

            $CourseUnit = CourseUnit::defaults($siteId);
            $CourseUnit->course_id = $Course->id;
            $CourseUnit->name = !!request('name') ? request('name') : 'Без названия';
            $CourseUnit->type = request('type');

            if (request('status')) {
                $CourseUnit->status = request('status');
            }

            $CourseUnit->save();

            Utils::reloadSite($CourseUnit->site_id);
            $result->course_unit = CourseUnitResponse::fromModel($CourseUnit)->toArray();
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
