<?php


namespace App\Http\Controllers\Response\Users;

use App\CFG;
use App\Http\Controllers\Response\BaseResponse;
use App\Http\Controllers\Response\Sites\SiteResponse;
use App\User;

/**
 * Class UserResponse
 *
 * @OA\Schema(
 *     schema="User",
 *     description="Схема модели пользователя системы",
 *     title="Модель Пользователя",
 *     required={"id"}
 * )
 */
class UserResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     description="Имя участника",
     *     title="Имя участника",
     * )
     *
     * @var string
     */
    public $firstname;

    /**
     * @OA\Property(
     *     description="Фамилия участника",
     *     title="Фамилия участника",
     * )
     *
     * @var string
     */
    public $surname;

    /**
     * @OA\Property(
     *     description="Отчество участника",
     *     title="Отчество участника",
     * )
     *
     * @var string
     */
    public $middle_name;

    /**
     * @OA\Property(
     *     description="Электропочта участника",
     *     title="Электропочта участника",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="Телефон студента",
     *     title="Телефон студента",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     description="Должность участника",
     *     title="Должность участника",
     * )
     *
     * @var string
     */
    public $position;


    /**
     * @OA\Property(
     *     description="Название должности участника",
     *     title="Название должности участника",
     * )
     *
     * @var string
     */
    public $position_title;

    /**
     * @OA\Property(
     *     description="Аватар участника",
     *     title="Аватар участника",
     * )
     *
     * @var string
     */
    public $avatar;

    /**
     * @OA\Property(
     *    property="sites",
     *    description="Сайты",
     *    type="array",
     *     @OA\Items(
     *       @OA\Property(
     *         property="number",
     *         description="Id Сайта",
     *         type="integer"
     *       ),
     *       @OA\Property(
     *         property="Название сайта",
     *         description="Название сайта",
     *         type="string"
     *       ),
     *     )
     * )
     *
     * @var string
     */
    public $sites;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата создания участника",
     *     title="Дата создания участника",
     * )
     *
     * @var string
     */
    public $created_at;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата обновления участника",
     *     title="Дата обновления участника",
     * )
     *
     * @var string
     */
    public $updated_at;

    /**
     * @param User $User
     * @param bool $full
     * @return UserResponse
     */
    public static function fromModel(User $User, $full = true)
    {
        $UserResponse = new self();
        $UserResponse->id = +$User->id;
        $UserResponse->email = $User->email;
        $UserResponse->firstname = $User->firstname;
        $UserResponse->surname = $User->surname;
        $UserResponse->middle_name = $User->middle_name;
        $UserResponse->avatar = 'https://www.gravatar.com/avatar/' . md5($User->email) . '?' . http_build_query(['d' => CFG::get('url') . 'static/img/default_avatar.png']);
        $UserResponse->phone = $User->phone;
        $UserResponse->created_at = $User->created_at;
        $UserResponse->updated_at = $User->updated_at;
        $UserResponse->sites = [];

        if ($full) {
            foreach ($User->Sites as $Site) {
                $UserResponse->sites[] = SiteResponse::fromModel($Site)->toArray();
            }
        }

        return $UserResponse;
    }
}
