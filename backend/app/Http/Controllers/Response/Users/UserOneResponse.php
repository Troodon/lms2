<?php


namespace App\Http\Controllers\Response\Users;
use App\Http\Controllers\Response\BaseResponse;
use Exception;
use Site;
use Phalcon\Di;
use Phalcon\Security;
use Role;
use User;

/**
 * Class UserOneResponse
 *
 * @OA\Schema(
 *     schema="UserOne",
 *     description="Схема ответа одного участника",
 *     title="Участник",
 *     required={"success"}
 * )
 */
class UserOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Участник",
     *     default=false,
     *     description="Участник",
     * )
     *
     * @var UserResponse
     */
    public $user;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $studentId
     * @param $orgId
     * @return SiteOneResponse
     */
    public static function fromId($studentId, $orgId)
    {
        $result = new self();

        try {
            $User = User::findFirst($studentId);

            if (!!$User) {
                $result->user = UserResponse::fromModel($User, $orgId)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Участник не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $studentId
     * @param $orgId
     * @param $rawRequest
     * @return SiteOneResponse
     */
    public static function postById($studentId, $orgId, $rawRequest)
    {
        $result = new self();

        try {
            $User = User::findFirst($studentId);

            if (!!$User) {
                $User->firstname = $rawRequest['firstname'];
                $User->middle_name = $rawRequest['middle_name'];
                $User->surname = $rawRequest['surname'];
                $User->email = $rawRequest['email'];
                $User->phone = $rawRequest['phone'];

                if (isset($rawRequest['new_password'])) {
                    /** @var Security $Security */
                    $Security = Di::getDefault()->get('security');

                    $User->password = $Security->hash($rawRequest['new_password']);
                }


                $User->save();
                $UserRole = Role::getRole($User->id, $orgId);

//                if ($UserRole->position != $rawRequest['position']) {
//                    $UserRole->position = $rawRequest['position'];
//                    $UserRole->save();
//                }

                $result->user = UserResponse::fromModel($User)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Участник не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
