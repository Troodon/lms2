<?php

namespace App\Http\Controllers\Response\Users;

use App\Http\Controllers\Response\BaseResponse;
use Exception;
use Phalcon\Di;
use Phalcon\Mvc\Model\Manager as ModelsManager;

/**
 * Class UsersResponse
 *
 * @OA\Schema(
 *     schema="Users",
 *     description="Схема модели ответа списка участников",
 *     title="Список участников",
 *     required={"success"}
 * )
 */
class UsersResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Количество участников в выборке",
     *     default=0,
     *     description="Количество участников в выборке",
     * )
     *
     * @var integer
     */
    public $total;


    /**
     * @OA\Property(
     *     title="Массив с участниками",
     *     description="Массив с участниками",
     * )
     *
     * @var UserResponse[]
     */
    public $users;

    /**
     * @param $orgId
     * @param array $params
     * @param int $page
     * @param int $perPage
     * @return SitesResponse
     */
    public static function fromFilter($orgId, $params = [], $page = 1, $perPage = 50)
    {
        $UsersResponse = new self();

        try {
            $phql = 'SELECT u.* FROM User u, Role r WHERE r.organization_id = :organization_id: AND r.user_id = u.id';
            $sqlParams = [
                'organization_id' => $orgId
            ];

            if (isset($params['status'])) {
                $phql .= ' AND u.status = :status:';
                $sqlParams['status'] = $params['status'];
            }

            if (isset($params['search'])) {
                $phql .= ' AND (u.firstname LIKE :search: OR u.surname LIKE :search:)';
                $sqlParams['search'] = "%{$params['search']}%";
            }

            /** @var ModelsManager $manager */
            $manager = Di::getDefault()->get('modelsManager');

            $limits = ' ORDER by u.id DESC LIMIT ' . (($page * $perPage) - $perPage) . ', ' . $perPage;
            $rows = $manager->executeQuery($phql . $limits, $sqlParams);
            $rowsCnt = $manager->executeQuery(str_replace('u.*', 'COUNT(*) as cnt', $phql), $sqlParams);
            $UsersResponse->users = [];
            $UsersResponse->total = +$rowsCnt[0]['cnt'];

            /* @var  $User User */
            foreach ($rows as $User) {
                $UsersResponse->users[] = UserResponse::fromModel($User, true, $orgId)->toArray();
            }

            $UsersResponse->success = true;
        } catch (Exception $e) {
            $UsersResponse->success = false;
            $UsersResponse->error = $e->getMessage();
        }

        return $UsersResponse;
    }
}
