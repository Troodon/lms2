<?php

namespace App\Http\Controllers\Response\Courses;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Course;
use App\Models\Site;
use Exception;

/**
 * Class CourseOneResponse
 *
 * @OA\Schema(
 *     schema="CourseOne",
 *     description="Схема ответа одного курса",
 *     title="Страница",
 *     required={"success"}
 * )
 */
class CourseOneResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success = false;


    /**
     * @OA\Property(
     *     title="Курс",
     *     default=false,
     *     description="Курса",
     * )
     *
     * @var CourseResponse
     */
    public $course;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @param $siteId
     * @param $id
     * @return CourseOneResponse
     */
    public static function fromKey($siteId, $id)
    {
        $result = new self();

        try {
            $Feed = Course::where([
                ['site_id', '=', $siteId],
                ['id', '=', $id],
                ['status', '<>', 'deleted'],
            ])->first();

            if (!!$Feed) {
                $result->course = CourseResponse::fromModel($Feed)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Лист не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @param $id
     * @return CourseOneResponse
     */
    public static function postByKey($siteId, $id)
    {
        $result = new self();

        try {
            /** @var Course $Course */
            $Course = Course::where([
                ['site_id', '=', $siteId],
                ['id', '=', $id],
                ['status', '<>', 'deleted'],
            ])->first();
            if (!!$Course) {
                if (!!request('status') && request('status') == 'deleted') {
                    $Course->status = request('status');
                } else {
                    $Course->name = request('name');
                    $Course->description = request('description');
                }

                $Course->save();
                $Course->logVersion();
//                SearchRebuildCourse::dispatch($Feed->id);

                $result->course = CourseResponse::fromModel($Course)->toArray();
                $result->success = true;
            } else {
                $result->error = 'Лист не найден';
            }
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $siteId
     * @return CourseOneResponse
     */
    public static function createCourse($siteId)
    {
        $result = new self();

        try {
            $Site = Site::where([['id', '=', $siteId]])->first();

            if (!$Site) {
                throw new Exception('Сайт не найден');
            }

            $Course = Course::defaults($siteId);
            $Course->name = !!request('name') ? request('name') : '';
            $Course->description = !!request('description') ? request('description') : '';
            $Course->save();
//            SearchRebuildCourse::dispatch($Course->id);

            $result->course = CourseResponse::fromModel($Course)->toArray();
            $result->success = true;
        } catch (Exception $e) {
            $result->error = $e->getMessage();
        }

        return $result;
    }
}
