<?php

namespace App\Http\Controllers\Response\Courses;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Course;

/**
 * Class CourseResponse
 *
 * @OA\Schema(
 *     schema="Course",
 *     description="Схема модели курса",
 *     title="Модель курса",
 *     required={"id"}
 * )
 */
class CourseResponse extends BaseResponse
{

    /**
     * @OA\Property(
     *     description="Идентификатор курса",
     *     title="Идентификатор",
     * )
     *
     * @var string
     */
    public $id;

    /**
     * @OA\Property(
     *     format="int64",
     *     title="ID",
     *     default=1,
     *     description="Идентификатор сайта",
     * )
     *
     * @var integer
     */
    public $site_id;

    /**
     * @OA\Property(
     *     title="Статус",
     *     description="Статус курса",
     * )
     *
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *     description="Название листа",
     *     title="Название листа",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="Описание курса",
     *     title="Описание",
     * )
     *
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *     description="Изображение курса",
     *     title="Изображение",
     * )
     *
     * @var string
     */
    public $image;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата создания сайта",
     *     title="Дата создания сайта",
     * )
     *
     * @var string
     */
    public $created_at;

    /**
     * @OA\Property(
     *     format="msisdn",
     *     description="Дата обновления сайта",
     *     title="Дата обновления сайта",
     * )
     *
     * @var string
     */
    public $updated_at;

    /**
     * @param Course $Feed
     * @return CourseResponse
     */
    public static function fromModel(Course $Feed)
    {
        $FeedResponse = new self();
        $FeedResponse->site_id = +$Feed->site_id;
        $FeedResponse->id = +$Feed->id;
        $FeedResponse->status = $Feed->status;
        $FeedResponse->name = $Feed->name;
        $FeedResponse->description = $Feed->description;
        $FeedResponse->image = $Feed->image;
        $FeedResponse->created_at = $Feed->created_at;
        $FeedResponse->updated_at = $Feed->updated_at;


        return $FeedResponse;
    }
}
