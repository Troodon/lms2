<?php

namespace App\Http\Controllers\Response\Courses;

use App\Http\Controllers\Response\BaseResponse;
use App\Models\Course;
use Exception;

/**
 * Class CoursesResponse
 *
 * @OA\Schema(
 *     schema="Courses",
 *     description="Схема модели ответа курсов",
 *     title="Курсы",
 *     required={"success"}
 * )
 */
class CoursesResponse extends BaseResponse
{
    /**
     * @OA\Property(
     *     title="Запрос выполнен успешно",
     *     default=false,
     *     description="Запрос выполнен успешно",
     * )
     *
     * @var boolean
     */
    public $success;

    /**
     * @OA\Property(
     *     title="Пояснение ошибки в запросе",
     *     description="Пояснение ошибки в запросе",
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="Количество участников в выборке",
     *     default=0,
     *     description="Количество участников в выборке",
     * )
     *
     * @var integer
     */
    public $total;


    /**
     * @OA\Property(
     *     title="Курсы",
     *     description="Массив с курсами",
     * )
     *
     * @var CourseResponse[]
     */
    public $courses;

    /**
     * @param $siteId
     * @return CoursesResponse
     */
    public static function fromFilter($siteId)
    {
        $PagesResponse = new self();

        try {
            /* @var  $Feed Course */
            foreach (Course::where([
                ['site_id', '=', $siteId],
                ['status', '<>', 'deleted'],
            ])->get() as $Feed) {
                $PagesResponse->courses[] = CourseResponse::fromModel($Feed)->toArray();
            }
            $PagesResponse->success = true;
        } catch (Exception $e) {
            $PagesResponse->success = false;
            $PagesResponse->error = $e->getMessage();
        }

        return $PagesResponse;
    }
}
