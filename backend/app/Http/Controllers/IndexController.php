<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    public function index()
    {
        return view('home');
    }

    /**
     * @param $key
     * @return Factory|View
     * @throws Exception
     */
    public function payment($key)
    {
        if (!preg_match('#^[a-f0-9]{32,}$#', $key)) {
            return abort(404);
        }

        $hash = substr($key, -40);
        $id = str_replace($hash, '', $key);
        /** @var Payment $Payment */
        $Payment = Payment::where([['id', '=', $id]])->first();

        if (!$Payment || $Payment->hashkey() != $hash) {
            return abort(404);
        }

        return view('home');
    }
}
