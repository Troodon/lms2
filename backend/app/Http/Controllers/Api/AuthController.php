<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Response\Orders\OrderOneResponse;
use App\Http\Controllers\Response\Users\UserResponse;
use App\Models\Payment;
use App\Models\Role;
use App\Models\Site;
use App\User;
use easmith\selectel\storage\SelectelStorageException;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthController
 * @package App\Http\Controllers\Api
 */
class AuthController extends Controller
{
    public $successStatus = 200;

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws SelectelStorageException
     */
    public function register(Request $request)
    {
        $existUser = User::fromEmail(\request('email'));

        if ($existUser) {
            return response()->json(['success' => false, 'error' => 'Email занят!'], $this->successStatus);
        }

        $User = User::dive(\request('email'), bcrypt(request('password')));

        $Site = Site::defaults();
        $Site->save();

        Role::setRole($User->id, $Site->id, Role::ADMIN_LEVEL);

        $Site->domain = "t{$Site->id}.lms.space";
        $Site->name = "T{$Site->id}";

        $Site->save();

        return response()->json(['success' => !!$Site->initSite()], $this->successStatus);
    }

    /**
     * @return array|JsonResponse
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('LMS')->accessToken;
            return ['success' => $success];
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);

        }
    }

    /**
     * @return JsonResponse
     */
    public function info()
    {
        $isAuth = Auth::check();
        $user = Auth::user();
        return response()->json(['success' => $isAuth, 'user' => UserResponse::fromModel($user)->toArray()], $this->successStatus);
    }

    /**
     *
     */
    public function order()
    {
        $isAuth = Auth::check();
        $User = Auth::user();
        $months = +request('months');
        $sites = request('sites');

        return response()->json(OrderOneResponse::placeOrder($sites, $months, $User)->toArray(), $this->successStatus);
    }

    /**
     * @param $key
     * @return array|void
     * @throws Exception
     */
    public function payment($key)
    {
        $isAuth = Auth::check();

        if (!preg_match('#^[a-f0-9]{32,}$#', $key) || !$isAuth) {
            return abort(404);
        }

        $hash = substr($key, -40);
        $id = str_replace($hash, '', $key);
        $User = Auth::user();
        /** @var Payment $Payment */
        $Payment = Payment::where([['id', '=', $id]])->first();

        if (!$Payment || $Payment->hashkey() != $hash || $Payment->user_id != $User->id) {
            return abort(404);
        }

        return OrderOneResponse::fromPayment($Payment)->toArray();
    }
}
