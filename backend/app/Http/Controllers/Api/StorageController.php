<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Response\Storage\FileOneResponse;
use App\Http\Controllers\Response\Storage\FilesResponse;
use Illuminate\Http\Request;

/**
 * Class StorageController
 *
 * @OA\Tag(
 *     name="storage",
 *     description="Управление файлами",
 * )
 */
class StorageController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/{siteId}/storage",
     *     tags={"storage"},
     *     summary="Список файлов",
     *     description="Список файлов",
     *     operationId="getFiles",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/storage"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"files:read"}}}
     * )
     * @param $siteId
     * @return
     */
    public function index($siteId)
    {
        $path = request('path', '');

        return FilesResponse::fromPath(+$siteId, $path)->toArray();
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/storage/read",
     *     tags={"storage"},
     *     summary="Данные файла",
     *     description="Данные файла",
     *     operationId="getFile",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="Идентификатор сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="path",
     *         in="query",
     *         description="Путь к файлу",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/FileOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"files:read"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function show($siteId)
    {
        $path = request('path', '');

        return FileOneResponse::fromPath($siteId, $path)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/storage/update",
     *     tags={"storage"},
     *     summary="Обновление файла",
     *     description="Обновление файла",
     *     operationId="updateFile",
     *     @OA\Parameter(
     *         name="orgId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="path",
     *         in="query",
     *         description="Путь к файлу",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/PageOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"pages:read", "pages:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Page"}
     * )
     * @param Request $request
     * @param $siteId
     * @return array
     */
    public function update(Request $request, $siteId)
    {
        $path = request('path', '');

        return FileOneResponse::updatePath($siteId, $path, $request)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/storage/delete",
     *     tags={"storage"},
     *     summary="Удаление файла",
     *     description="Удаление файла",
     *     operationId="deleteFile",
     *     @OA\Parameter(
     *         name="orgId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="path",
     *         in="query",
     *         description="Путь к файлу",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/PageOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"pages:read", "pages:write"}}},
     * )
     * @param Request $request
     * @param $siteId
     * @return array
     */
    public function delete(Request $request, $siteId)
    {
        $path = request('path', '');

        return FileOneResponse::deletePath($siteId, $path)->toArray();
    }


    /**
     * @OA\Post(
     *     path="/api/{siteId}/storage/upload",
     *     tags={"storage"},
     *     summary="Загрузка файла",
     *     description="Загрузка файла",
     *     operationId="uploadFile",
     *     @OA\Parameter(
     *         name="orgId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="path",
     *         in="query",
     *         description="Путь к файлу",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/FileOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"files:read", "files:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/File"}
     * )
     * @param Request $request
     * @param $siteId
     * @return array
     */
    public function upload(Request $request, $siteId)
    {
        $path = request('path', '');
        $file = request('file'); // get the file user sent via POST
        return FileOneResponse::uploadPath($siteId, $path, $file)->toArray();
    }
}
