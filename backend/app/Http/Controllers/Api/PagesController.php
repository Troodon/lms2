<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Response\Pages\PageOneResponse;
use App\Http\Controllers\Response\Pages\PagesResponse;

/**
 * Class PagesController
 *
 * @OA\Tag(
 *     name="pages",
 *     description="Управление страницами",
 * )
 */
class PagesController extends ApiController
{

    /**
     * @param $siteId
     * @return array
     * @OA\Get(
     *     path="/api/{siteId}/pages",
     *     tags={"pages"},
     *     summary="Список страниц",
     *     description="Список страниц",
     *     operationId="getPages",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/Pages"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"pages:read"}}}
     * )
     *
     */

    public function index($siteId)
    {
        return PagesResponse::fromFilter($siteId)->toArray();
    }

    /**
     *
     * @OA\Get(
     *     path="/api/{siteId}/pages/{pageId}",
     *     tags={"pages"},
     *     summary="Получение информации о странице по ID",
     *     description="Получает информацию о странице",
     *     operationId="getPageById",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="Идентификатор сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pageId",
     *         in="path",
     *         description="Идентификатор страницы",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/PageOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"pages:read"}}}
     * )
     * @param $siteId
     * @param $pageId
     * @return array
     */
    public function show($siteId, $pageId)
    {
        return PageOneResponse::fromId($siteId, $pageId)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/pages/{pageId}",
     *     tags={"pages"},
     *     summary="Обновление данных страницы",
     *     description="Обновление данных страницы",
     *     operationId="updatePage",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pageId",
     *         in="path",
     *         description="ID оплаты",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/PageOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"pages:read", "pages:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Page"}
     * )
     * @param $siteId
     * @param $pageId
     * @return array
     */
    public function update($siteId, $pageId)
    {
        return PageOneResponse::postById($siteId, $pageId)->toArray();
    }


    /**
     * @OA\Post(
     *     path="/api/{siteId}/pages/new",
     *     tags={"pages"},
     *     summary="Создание новой страницы",
     *     description="Создание новой страницы",
     *     operationId="createPage",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/PageOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"pages:read", "pages:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Page"}
     * )
     * @param $siteId
     * @return array
     */
    public function create($siteId)
    {
        return PageOneResponse::createPage($siteId)->toArray();
    }
}
