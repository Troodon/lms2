<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Response\Courses\CourseOneResponse;
use App\Http\Controllers\Response\Courses\CoursesResponse;
use App\Http\Controllers\Response\CourseUnits\CourseUnitOneResponse;
use App\Http\Controllers\Response\CourseUnits\CourseUnitsResponse;

/**
 * Class CoursesController
 *
 * @OA\Tag(
 *     name="courses",
 *     description="Управление курсами",
 * )
 */
class CoursesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/{siteId}/courses",
     *     tags={"lists"},
     *     summary="Списки",
     *     description="Списки",
     *     operationId="getLists",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/Courses"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function index($siteId)
    {
        return CoursesResponse::fromFilter(+$siteId)->toArray();
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/course/{key}",
     *     tags={"courses"},
     *     summary="Получение информации о списке по ключу",
     *     description="Получает информацию о списке",
     *     operationId="getListByKey",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="Идентификатор сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="key",
     *         in="path",
     *         description="Идентификатор списка",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/CourseOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read"}}}
     * )
     * @param $siteId
     * @param $courseKey
     * @return array
     */
    public function show($siteId, $courseKey)
    {
        return CourseOneResponse::fromKey($siteId, $courseKey)->toArray();
    }

    /**
     * @param $siteId
     * @param $courseKey
     * @return array
     *
     * @OA\Post(
     *     path="/api/{siteId}/courses/{key}",
     *     tags={"courses"},
     *     summary="Обновление данных списка",
     *     description="Обновление данных списка",
     *     operationId="updatePage",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="key",
     *         in="path",
     *         description="Идентификатор списка",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/CourseOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read", "lists:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/List"}
     * )
     */
    public function update($siteId, $courseKey)
    {
        return CourseOneResponse::postByKey($siteId, $courseKey)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/courses/new",
     *     tags={"lists"},
     *     summary="Создание нового списка",
     *     description="Создание нового списка",
     *     operationId="createPage",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/CourseOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read", "lists:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/List"}
     * )
     * @param $siteId
     * @return array
     */
    public function create($siteId)
    {
        return CourseOneResponse::createCourse($siteId)->toArray();
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/courses/{course}/items",
     *     tags={"lists"},
     *     summary="Списки",
     *     description="Списки",
     *     operationId="getCourses",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="course",
     *         in="path",
     *         description="Ключ списка",
     *         required=true,
     *         @OA\Schema(
     *             type="strintg"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/Courses"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read"}}}
     * )
     * @param $siteId
     * @param $courseId
     * @return array
     */
    public function items($siteId, $courseId)
    {
        $skip = request('skip', 0);
        $skip = ($skip > 0) ? $skip : 0;
        $limit = request('limit', 250);
        $limit = ($limit < 251 && $limit > 0) ? $limit : 250;
        return CourseUnitsResponse::fromFilter(+$siteId, $courseId, $skip, $limit)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/courses/{course}/new",
     *     tags={"lists"},
     *     summary="Создание нового списка",
     *     description="Создание нового списка",
     *     operationId="createPage",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/CourseOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read", "lists:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Course"}
     * )
     * @param $siteId
     * @param $courseId
     * @return array
     */
    public function createItem($siteId, $courseId)
    {
        return CourseUnitOneResponse::createCourseUnit($siteId, $courseId)->toArray();
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/courses/{course}/items/{item}",
     *     tags={"lists"},
     *     summary="Списки",
     *     description="Списки",
     *     operationId="getCourses",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/Courses"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"courses:read"}}}
     * )
     * @param $siteId
     * @param $courseKey
     * @param $itemId
     * @return array
     */
    public function showItem($siteId, $courseKey, $itemId)
    {
        return CourseUnitOneResponse::fromId($siteId, $courseKey, $itemId)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/courses/{key}/{id}",
     *     tags={"courses"},
     *     summary="Обновление данных списка",
     *     description="Обновление данных списка",
     *     operationId="updatePage",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="key",
     *         in="path",
     *         description="Идентификатор списка",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Идентификатор элемента списка",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/CourseUnitOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"list_items:read", "list_items:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/CourseUnit"}
     * )
     * @param $siteId
     * @param $courseKey
     * @param $itemId
     * @return array
     */
    public function updateItem($siteId, $courseKey, $itemId)
    {
        return CourseUnitOneResponse::postById($siteId, $courseKey, $itemId)->toArray();
    }
}
