<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Response\Sites\SiteConfigResponse;
use App\Http\Controllers\Response\Sites\SiteFtpResponse;
use App\Http\Controllers\Response\Sites\SiteOneResponse;
use App\Http\Controllers\Response\Sites\SiteResponse;
use App\Http\Controllers\Response\Sites\TeamResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class SitesController
 *
 * @OA\Tag(
 *     name="sites",
 *     description="Управление сайтами",
 * )
 */
class SitesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/{siteId}",
     *     tags={"sites"},
     *     summary="Получение информации о сайте по его ID",
     *     description="Получает информацию о сайте",
     *     operationId="getSiteById",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/SiteOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function show($siteId)
    {
        return SiteOneResponse::fromId($siteId)->toArray();
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}",
     *     tags={"sites"},
     *     summary="Обновление данных сайта",
     *     description="Обновление данных сайта",
     *     operationId="updateSite",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/SiteOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read", "sites:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Site"}
     * )
     * @param $siteId
     * @return array
     */
    public function update($siteId)
    {
        $User = Auth::user();
        return SiteOneResponse::postById($siteId, $User)->toArray();
    }


    /**
     * @OA\Post(
     *     path="/api/sites/new",
     *     tags={"sites"},
     *     summary="Создание нового сайта",
     *     description="Создание нового сайта",
     *     operationId="createSite",
     *     @OA\Parameter(
     *         name="orgId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/SiteOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read", "sites:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Site"}
     * )
     * @param Request $request
     * @return SiteResponse|array
     */
    public function create(Request $request)
    {
        $User = auth::user();

        return SiteResponse::createSite($User, $request);
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/config",
     *     tags={"sites"},
     *     summary="Получение информации о настройках сайта по его ID",
     *     description="Получает информацию о настройках сайта",
     *     operationId="getSiteConfigById",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function config($siteId)
    {
        return SiteConfigResponse::fromId($siteId);
    }


    /**
     * @OA\Get(
     *     path="/api/{siteId}/ftp",
     *     tags={"sites"},
     *     summary="Получение информации о настройках сайта по его ID",
     *     description="Получает информацию о настройках сайта",
     *     operationId="getSiteFtpById",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function ftp($siteId)
    {
        return SiteFtpResponse::fromId($siteId);
    }

    /**
     * @OA\Post(
     *     path="/api/{siteId}/ftp",
     *     tags={"sites"},
     *     summary="Обновление ftp сайта",
     *     description="Обновление ftp сайта",
     *     operationId="updateSiteFtp",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/SiteOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read", "sites:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Site"}
     * )
     * @param $siteId
     * @return array
     */
    public function ftpSave($siteId)
    {
        return SiteFtpResponse::postById($siteId);
    }


    /**
     * @OA\Post(
     *     path="/api/sites/{siteId}/config",
     *     tags={"sites"},
     *     summary="Обновление конфига сайта",
     *     description="Обновление конфига сайта",
     *     operationId="updateSiteConfig",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/SiteOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read", "sites:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Site"}
     * )
     * @param $siteId
     * @return array
     */
    public function configSave($siteId)
    {
        return SiteConfigResponse::postById($siteId);
    }


    /**
     * @OA\Post(
     *     path="/api/sites/{siteId}/purge_cdn",
     *     tags={"sites"},
     *     summary="Очистка кэша CDN",
     *     description="Очистка кэша CDN",
     *     operationId="purgeCDN",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Сайта",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/SiteOne"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"sites:read", "sites:write"}}},
     *     requestBody={"$ref": "#/components/requestBodies/Site"}
     * )
     * @param $siteId
     * @return array
     */
    public function purgeCDN($siteId)
    {
        return SiteConfigResponse::purgeCDN($siteId);
    }

    public function team($siteId)
    {
        return TeamResponse::fromFilter(+$siteId)->toArray();
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/team/invite",
     *     tags={"team"},
     *     summary="Пригласить в команду",
     *     description="Пригласить в команду",
     *     operationId="teamInvite",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/Team"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"team:write"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function teamInvite($siteId)
    {
        return TeamResponse::inviteUser(+$siteId, request('email'));;
    }

    /**
     * @OA\Get(
     *     path="/api/{siteId}/team/delete",
     *     tags={"team"},
     *     summary="Команда",
     *     description="Команда",
     *     operationId="teamDelete",
     *     @OA\Parameter(
     *         name="siteId",
     *         in="path",
     *         description="ID Организации",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успех",
     *         @OA\JsonContent(ref="#/components/schemas/Team"),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Доступ запрещён"
     *     ),
     *     security={{"Bearer":{"team:write"}}}
     * )
     * @param $siteId
     * @return array
     */
    public function teamDelete($siteId)
    {
        $response = ['success' => false];

        $user = Auth::user();
        $userForDelete = request('id');

        if ($user->id == $userForDelete) {
            $response['error'] = 'Нельзя удалить самого себя';
        } else {
            $response = TeamResponse::deleteUser(+$siteId, +$userForDelete);
        }

        return $response;
    }
}
