<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 * @package App\Http\Controllers\Api
 */
class ApiController extends Controller
{
    /**
     * @param string $method
     * @param array $parameters
     * @return array|Response
     */
    public function callAction($method, $parameters)
    {
        $User = Auth::user();

        if (isset($parameters['site'])) {
            if (!$User) {
                return response()->json([
                    'success' => false,
                    'error'   => 'Ошибка авторизации'
                ],403);
            }
            $roles = $User->siteRoles();

            if (!isset($roles[$parameters['site']])) {
                return response()->json([
                    'success' => false,
                    'error'   => 'Ошибка доступа'
                ],403);
            }
        }

        return parent::callAction($method, $parameters);
    }
}
