<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if ($request->expectsJson() || mb_substr($request->path(), 0, 3) == 'api') {
            response()->json([
                'success' => false,
                'error'   => 'Ошибка авторизации'
            ],403)->send();
        } else {
            return route('login');
        }
    }
}
