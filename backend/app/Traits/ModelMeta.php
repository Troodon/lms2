<?php

namespace App\Traits;

/**
 * Trait ModelMeta
 */
trait ModelMeta
{

    /**
     * @param $key
     * @param $value
     */
    public function setMetaValue($key, $value = null)
    {
        $meta = !!$this->meta ? unserialize($this->meta) : [];

        if (is_null($value)) {
            unset($meta[$key]);
        } else {
            $meta[$key] = $value;
        }

        $this->meta = serialize($meta);
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function getMeta($key = null, $default = null)
    {
        $meta = !!$this->meta ? unserialize($this->meta) : [];

        if (is_null($key)) {
            return $meta;
        }

        return isset($meta[$key]) ? $meta[$key] : $default;
    }
}
