<?php

namespace App\Traits;
/**
 * Trait ModelJsonMeta
 */
trait ModelJsonMeta
{

    /**
     * @param $key
     * @param $value
     */
    public function setMetaValue($key, $value = null)
    {
        $meta = !!$this->meta ? json_decode($this->meta, true) : [];

        if (is_null($value)) {
            unset($meta[$key]);
        } else {
            $meta[$key] = $value;
        }

        $this->meta = json_encode($meta);
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function getMeta($key = null, $default = null)
    {
        $meta = !!$this->meta ? json_decode($this->meta, true) : [];

        if (is_null($key)) {
            return $meta;
        }

        return isset($meta[$key]) ? $meta[$key] : $default;
    }
}
