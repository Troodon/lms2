<?php

namespace App;

/**
 * Class Settings
 */
class CFG
{
    /**
     * @var
     */
    protected static $config;

    /**
     * @param $config_path string
     */
    public static function init($config_path)
    {
        $configName = basename($config_path);
        $configPath = str_replace($configName, '', $config_path);
        $config = yaml_parse(file_get_contents($config_path));

        if (getenv("APPLICATION_ENV") == 'PRODUCTION' && file_exists($configPath . 'production.' . $configName)) {
            $subConfig = yaml_parse(file_get_contents($configPath . 'production.' . $configName));
            $config = array_replace_recursive($config, $subConfig);
        } else if (getenv("APPLICATION_ENV") == 'STAGING' && file_exists($configPath . 'staging.' . $configName)) {
            $subConfig = yaml_parse(file_get_contents($configPath . 'staging.' . $configName));
            $config = array_replace_recursive($config, $subConfig);
        } else if (file_exists($configPath . 'local.' . $configName)) {
            $subConfig = yaml_parse(file_get_contents($configPath . 'local.' . $configName));
            $config = array_replace_recursive($config, $subConfig);
        }

        self::$config = $config;
    }

    /**
     * @param $path
     * @return mixed|null
     */
    public static function get($path = null)
    {
        if (!$path) return self::$config;

        $pathList = explode(':', $path);

        return self::subGet(self::$config, $pathList);
    }

    /**
     * @param $config   array
     * @param $pathList string[]
     * @return mixed|null
     */
    protected static function subGet($config, $pathList)
    {
        $currentPath = array_shift($pathList);

        if (!isset($config[$currentPath])) {
            return null;
        }

        $subConfig = $config[$currentPath];

        if (empty($pathList)) {
            return $subConfig;
        }

        return self::subGet($subConfig, $pathList);
    }
}
