<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $site
 * @property string $domain
 * @property string $created_at
 * @property string $updated_at
 */
class Domain extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'domain';

    /**
     * @var array
     */
    protected $fillable = ['site', 'domain', 'created_at', 'updated_at'];

}
