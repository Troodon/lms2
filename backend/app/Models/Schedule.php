<?php

namespace App\Models;

use App\Library\TFatalException;
use App\Process\CDNProcess;
use App\Process\CertificateProcess;
use App\Process\ScreenshotProcess;
use App\Traits\ModelMeta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Jenssegers\Date\Date;

/**
 * @property int $id
 * @property int $site_id
 * @property string $type
 * @property string $date
 * @property string $status
 * @property string $meta
 * @property string $log
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Site $Site
 */
class Schedule extends BaseModel
{
    use ModelMeta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'schedule';

    /**
     * @var array
     */
    protected $fillable = ['site_id', 'type', 'date', 'status', 'meta', 'log', 'created_at', 'updated_at'];

    /**
     * @param $siteId
     * @return Schedule
     */
    public static function defaults($siteId)
    {
        $Schedule = new Schedule();
        $Schedule->site_id = $siteId;
        $Schedule->status = 'pending';
        $Schedule->meta = serialize([]);

        return $Schedule;
    }

    /**
     * @return Builder[]|Collection
     */
    public static function fetchNext30()
    {
        return Schedule::where([
            ['status', '=', 'pending'],
            ['date', '<=', (new Date())->toIso8601String()]
        ])->get();
    }

    /**
     * @return HasOne
     */
    public function Site()
    {
        return $this->hasOne(Site::class, 'id', 'site_id');
    }

    /**
     * @throws TFatalException
     */
    public function Run()
    {
        if ($this->type == 'screenshot') {
            ScreenshotProcess::siteHomePage($this);
        } else if ($this->type == 'install_certificate') {
            CertificateProcess::installCertificate($this);
        } else if ($this->type == 'install_certificate_verify') {
            CertificateProcess::installCertificateVerify($this);
        } else if ($this->type == 'rebuild_certificate_nginx') {
            CertificateProcess::rebuildCertificateNginx($this);
        } else if ($this->type == 'purge_cdn') {
            CDNProcess::purgeCDN($this);
        }
    }

    /**
     * @param $data
     */
    public function logAction($data)
    {
        $logs = is_array(unserialize($this->log)) ? unserialize($this->log) : [];
        $logs[] = [
            'date' => (new Date())->toIso8601String(),
            'l'    => $data
        ];
        $this->log = serialize($logs);
    }
}
