<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $site_id
 * @property string $status
 * @property string $domains
 * @property string $certificate
 * @property string $public_key
 * @property string $private_key
 * @property string $fullchain
 * @property string $email
 * @property string $expires_at
 * @property string $meta
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Site $Site
 */
class Certificate extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'certificate';

    /**
     * @var array
     */
    protected $fillable = ['site_id', 'status', 'domains', 'certificate', 'public_key', 'private_key', 'fullchain', 'email', 'expires_at', 'meta', 'created_at', 'updated_at'];

    /**
     * @param $siteId
     * @return Certificate
     */
    public static function defaults($siteId)
    {
        $Certificate = new Certificate();
        $Certificate->site_id = +$siteId;
        $Certificate->status = 'ok';
        $Certificate->email = '';
        $Certificate->domains = '';
        $Certificate->certificate = '';
        $Certificate->public_key = '';
        $Certificate->private_key = '';
        $Certificate->fullchain = '';
        $Certificate->meta = serialize([]);

        return $Certificate;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Site()
    {
        return $this->hasOne(Site::class, 'id', 'site_id');
    }
}
