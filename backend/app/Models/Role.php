<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Phalcon\Mvc\Model\ResultInterface;

/**
 * @property int $id
 * @property int $site_id
 * @property int $user_id
 * @property int $level
 * @property string $status
 * @property string $position
 * @property string $created_at
 * @property string $updated_at
 */
class Role extends BaseModel
{
    /**
     *
     */
    const ADMIN_LEVEL = 1;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role';

    /**
     * @var array
     */
    protected $fillable = ['site_id', 'user_id', 'level', 'status', 'position', 'created_at', 'updated_at'];


    /**
     * @param $userId
     * @param $orgId
     * @return bool
     */
    public static function checkRole($userId, $orgId)
    {
        return !!self::getRole($userId, $orgId);
    }

    /**
     * @param $userId
     * @param $siteId
     * @return Role
     */
    public static function getRole($userId, $siteId)
    {
        return Role::where([
            ['site_id', '=', $siteId],
            ['user_id', '=', $userId]
        ])->first();
    }

    /**
     * @param $userId
     * @param $orgId
     * @param int $level
     * @param string $position
     * @return Role
     */
    public static function setRole($userId, $orgId, $level=1, $position='admin')
    {
        $Role = new Role();

        $Role->user_id = $userId;
        $Role->site_id = $orgId;
        $Role->level = $level;
        $Role->position = $position;

        $Role->save();
        Cache::forget("user:roles:{$userId}");

        return $Role;
    }
}
