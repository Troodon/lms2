<?php

namespace App\Models;

use App\Library\TabulaStorage;
use App\Traits\ModelJsonMeta;
use Elasticsearch\Client;
use Illuminate\Support\Carbon;
use Jenssegers\Date\Date;
use stdClass;

/**
 * @property int $id
 * @property int $site_id
 * @property int $course_id
 * @property string $status
 * @property string $type
 * @property string $name
 * @property string $content
 * @property string $description
 * @property string $meta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class CourseUnit extends BaseModel
{
    use ModelJsonMeta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'course_unit';

    /**
     * @var array
     */
    protected $fillable = ['site_id', 'course_id', 'status', 'type', 'name', 'description', 'content', 'meta', 'created_at', 'updated_at'];

    /**
     * @param $siteId
     * @return CourseUnit
     */
    public static function defaults($siteId)
    {

        $FeedItem = new CourseUnit();
        $FeedItem->site_id = +$siteId;
        $FeedItem->status = 'ok';
        $FeedItem->type = 'content';
        $FeedItem->content = '';
        $FeedItem->name = '';
        $FeedItem->description = '';
        $FeedItem->meta = json_encode(new stdClass());

        return $FeedItem;
    }

    /**
     * @param array $options
     * @return bool|void
     */
    public function save(array $options = [])
    {
        parent::save($options);
        $this->indexSearch();
    }

    /**
     *
     */
    public function indexSearch()
    {
        return;

        if (!$this->course_id || !$this->site_id) {
            return;
        }

        $tags = explode(',', $this->tags);
        $tags = array_filter($tags);
        $tags = array_map('trim', $tags);
        $indexModel = [
            'index' => "feed{$this->course_id}",
            'id'    => "{$this->id}",
            'body'  => [
                'site'       => +$this->site_id,
                'feed'       => +$this->course_id,
                'key'        => $this->key,
                'tags'       => $tags,
                'status'     => $this->status,
                'name'       => $this->name,
                'created_at' => $this->created_at->toIso8601ZuluString(),
                'updated_at' => $this->updated_at->toIso8601ZuluString()
            ]
        ];

        foreach ($this->getFields() as $fieldKey => $field) {
            if (!isset($indexModel['body'][$fieldKey])) {
                $indexModel['body'][$fieldKey] = $field;
            }
        }

        /** @var Client $Elasticsearch */
        $Elasticsearch = resolve(Client::class);
        $res = $Elasticsearch->index($indexModel);
    }

    /**
     * @return array
     */
    public function getFields()
    {
        $result = [];

        if (strlen($this->fields) > 3) {
            $data = @json_decode($this->fields, true);
            $data = (!!$data && is_array($data)) ? $data : [];

            foreach ($data as $field => $fieldData) {
                $result[$field] = $fieldData;
            }
        }

        return $result;
    }

    /**
     *
     */
    public function logVersion()
    {
        $versionDate = (new Date())->format('Y-m-d-H:i:s');
        $path = "versions/feed_item/{$this->id}/{$versionDate}.json";
        $version = [
            'name'    => $this->name,
            'status'  => $this->status,
            'key'     => $this->key,
            'fields'  => $this->fields,
            'feed_id' => $this->course_id,
            'tags'    => $this->tags,
        ];
        $TabulaStorage = new TabulaStorage($this->site_id);
        $TabulaStorage->writePath($path, json_encode($version));
    }
}
