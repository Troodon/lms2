<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * @mixin Eloquent
 *
 * @method static Builder where($arg)
 */
class BaseModel extends Model
{
    /**
     * Дата в формате PostgreSQL
     *
     * @var string
     */
    protected $dateFormat;

}
