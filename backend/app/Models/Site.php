<?php

namespace App\Models;

use App\CFG;
use App\Library\TSelectelStorage;
use App\Traits\ModelJsonMeta;
use easmith\selectel\storage\SelectelStorageException;

/**
 * @property int $id
 * @property int $sid
 * @property string $status
 * @property string $name
 * @property string $domain
 * @property string $settings
 * @property string $meta
 * @property string $tariff
 * @property string $active_to
 * @property string $created_at
 * @property string $updated_at
 */
class Site extends BaseModel
{
    use ModelJsonMeta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'site';

    /**
     * @var array
     */
    protected $fillable = ['status', 'name', 'domain', 'settings', 'meta', 'tariff', 'active_to', 'created_at', 'updated_at'];

    /**
     * @return Site
     */
    public static function defaults()
    {
        $Site = new self();
        $Site->status = 'ok';
        $Site->tariff = 'demo';
        $Site->domain = 'tmp' . rand(11111, 99999);
        $Site->name = $Site->domain;
        $Site->settings = '{}';
        $Site->meta = serialize([]);
        $Site->setMetaValue('www_redirect', true);

        return $Site;
    }

    /**
     * @return $this
     * @throws SelectelStorageException
     */
    public function initSite()
    {

        $storageParams = [
            'X-Container-Meta-Type: public',
            'X-Container-Domains: ' . CFG::get('selectel:storage:domain') . $this->id . '.tcdn.site',
            'x-container-meta-access-control-allow-methods: GET,HEAD',
            'x-container-meta-access-control-max-age: 86400',
            'x-container-meta-cache-control: public',
        ];
        $selectelStorage = new TSelectelStorage(CFG::get('selectel:storage:user'), CFG::get('selectel:storage:key'));
        $containerName = CFG::get('selectel:storage:prefix') . $this->id;
        $container = $selectelStorage->createContainer($containerName, $storageParams);

        // Установка домена CDN хранилищу
        $selectelStorage->setDomain($containerName, CFG::get('selectel:storage:domain') . $this->id . '.tcdn.site');
//        $headers = [
//            'X-Auth-Token'            => str_replace('X-Auth-Token: ', '', $selectelStorage->token[0]),
//            'X-Add-Container-Domains' => Config::get('selectel:storage:domain') . $this->id . '.tcdn.site'
//        ];
//        $requestUrl = 'https://' . Config::get('selectel:storage:user') . '.selcdn.ru/' . Config::get('selectel:storage:prefix') . $this->id;
//        $res = Requests::post($requestUrl, $headers);
//~rt($requestUrl, $headers, $res->body);
        // Запись информации о домене
        $Domain = new Domain();
        $Domain->site = $this->id;
        $Domain->domain = $this->domain;
        $Domain->save();

        return $this;
    }

    /**
     * @return string
     */
    public function getPublicUrl()
    {
        return !!$this->domain ? 'http://' . $this->domain : 'http://' . $this->url . '.lms.space';
    }

    /**
     * @return string
     */
    public function getContainer()
    {
        return CFG::get('selectel:storage:prefix') . $this->id;
    }

    /**
     * @return string
     */
    public function cdn()
    {
        return "https://t{$this->id}.tcdn.site";
    }

    /**
     * @return bool
     */
    public function usesCDN()
    {
        return !!$this->getMeta('uses_cdn', false);
    }

    /**
     * @param $url
     * @param $data
     */
    public function interceptUrl($url, $data)
    {
        $intercepted = $this->getMeta('intercepted', []);
        $intercepted[$url] = $data;
        $this->setMetaValue('intercepted', $intercepted);
    }

    /**
     * @param $type
     */
    public function interceptDelete($type)
    {
        $intercepted = $this->getMeta('intercepted', []);
        $newUrls = [];

        if (!!$type) {
            foreach ($intercepted as $key => $item) {
                if (isset($item['type']) && $item['type'] == $type) {
                    continue;
                }
                $newUrls[$key] = $item;
            }
        }

        $this->setMetaValue('intercepted', $newUrls);
    }
}
