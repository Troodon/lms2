<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $user
 * @property string $token
 * @property string $expires_at
 * @property string $created_at
 */
class Token extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'token';

    /**
     * @var array
     */
    protected $fillable = ['user', 'token', 'expires_at', 'created_at'];

}
