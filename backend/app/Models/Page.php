<?php

namespace App\Models;

use App\Library\TabulaStorage;
use Jenssegers\Date\Date;

/**
 * @property int $id
 * @property int $site_id
 * @property int $sid
 * @property int $parent
 * @property int $disabled
 * @property string $status
 * @property string $name
 * @property string $url
 * @property string $content
 * @property string $handler
 * @property string $settings
 * @property string $feed
 * @property int $sort
 * @property string $created_at
 * @property string $updated_at
 */
class Page extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'page';

    /**
     * @var array
     */
    protected $fillable = ['site_id', 'sid', 'parent', 'disabled', 'status', 'name', 'url', 'content', 'handler', 'settings', 'feed', 'sort', 'created_at', 'updated_at'];

    /**
     * @param $siteId
     * @return Page
     */
    public static function defaults($siteId)
    {
        $Page = new Page();
        $Page->sid = 0;
        $Page->sort = 0;
        $Page->site_id = +$siteId;
        $Page->parent = 0;
        $Page->disabled = 0;
        $Page->name = 'Новая страница';
        $Page->content = '';
        $Page->settings = '';

        return $Page;
    }

    /**
     * @return int
     */
    public function getChildOrder()
    {
        $lastOrderPage = Page::where([['parent', '=', +$this->id]])->first();

        if ($lastOrderPage) {
            return $lastOrderPage->sort + 10;
        }

        return 0;
    }


    /**
     *
     */
    public function logVersion()
    {
        $versionDate = (new Date())->format('Y-m-d-H:i:s');
        $path = "versions/page/{$this->id}/{$versionDate}.json";
        $version = [
            'name'     => $this->name,
            'status'   => $this->status,
            'url'      => $this->url,
            'feed'     => $this->feed,
            'content'  => $this->content,
            'handler'  => $this->handler,
            'settings' => $this->settings
        ];

        $TabulaStorage = new TabulaStorage();
        $TabulaStorage->writePath($path, json_encode($version));
    }
}
