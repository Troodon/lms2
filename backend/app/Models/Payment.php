<?php

namespace App\Models;

use App\Traits\ModelJsonMeta;
use Exception;
use Jenssegers\Date\Date;

/**
 * Class Payment
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property string $amount
 * @property string $paid_to
 * @property string $meta
 * @property string $created_at
 * @property string $updated_at
 */
class Payment extends BaseModel
{
    use ModelJsonMeta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'status', 'amount', 'paid_to', 'meta', 'created_at', 'updated_at'];

    /**
     * @param $userId
     * @return Payment
     */
    public static function defaults($userId)
    {
        $Payment = new Payment();
        $Payment->user_id = +$userId;
        $Payment->status = 'notpaid';
        $Payment->amount = 0;
        $Payment->meta = serialize([]);
        $Payment->paid_to = (new Date())->add('3 days')->format('Y-m-d H:i:s');

        return $Payment;
    }

    /**
     * @param $siteId
     * @param $tariff
     * @param $months
     */
    public function setSite($siteId, $tariff, $months)
    {
        $sites = $this->getMeta('sites', []);
        $sites[$siteId] = ['tariff' => $tariff, 'months' => +$months, 'id' => +$siteId];
        $this->setMetaValue('sites', $sites);
    }

    /**
     *
     */
    public function processPaid()
    {
        if ($this->status == 'paid') {
            throw new Exception("Платёж #{$this->id} уже оплачен!");
        }

        $this->status = 'paid';
        $sites = $this->getMeta('sites', []);
        $months = $this->getMeta('months', 0);

        foreach ($sites as $site) {
            /** @var Site $Site */
            $Site = Site::where('id', '=', $site['id'])->first();
            $Site->tariff = $site['tariff'];
            $Site->active_to = (new Date())->add("{$months} months")->format('Y-m-d H:i:s');
            $Site->save();
        }

        $this->save();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function Link()
    {
        return "https://lms.space/billing/payment/{$this->id}{$this->hashkey()}";
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hashkey()
    {
        if (!$this->created_at) {
            throw new Exception('Платёж ещё не сохранён');
        }
        return hash('ripemd160', "{$this->user_id}:{$this->id}:{$this->created_at}");
    }
}
