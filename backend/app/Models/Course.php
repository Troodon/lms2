<?php

namespace App\Models;

use App\Library\TabulaStorage;
use App\Traits\ModelJsonMeta;
use Elasticsearch\Client;
use Exception;
use Jenssegers\Date\Date;

/**
 * @property int $id
 * @property int $site_id
 * @property string $status
 * @property string $name
 * @property string $description
 * @property string $image
 * @property string $meta
 * @property string $created_at
 * @property string $updated_at
 */
class Course extends BaseModel
{
    use ModelJsonMeta;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'course';

    /**
     * @var array
     */
    protected $fillable = ['site_id', 'status', 'name', 'image', 'description', 'meta', 'created_at', 'updated_at'];

    /**
     * @return Course
     */
    public static function defaults($siteId)
    {
        $Course = new Course();
        $Course->site_id = +$siteId;
        $Course->image = '';
        $Course->status = 'ok';
        $Course->name = '';
        $Course->meta = serialize([]);
        $Course->description = '';

        return $Course;
    }

    /**
     *
     */
    public function logVersion()
    {
        $versionDate = (new Date())->format('Y-m-d-H:i:s');
        $path = "versions/course/{$this->id}/{$versionDate}.json";
        $version = [
            'name'   => $this->name,
            'status' => $this->status,
            'fields' => $this->fields,
            'meta'   => $this->meta,
        ];
        $TabulaStorage = new TabulaStorage($this->site_id);
        $TabulaStorage->writePath($path, json_encode($version));
    }

    /**
     * @param bool $recreate
     * @throws Exception
     */
    public function createSearchIndex($recreate = false)
    {
        /** @var Client $Elasticsearch */
        $Elasticsearch = resolve(Client::class);
        $params = [
            'index' => "course{$this->id}",
            'body'  =>
                [
                    'mappings' => [
                        'properties' => [
                            'site'       => ['type' => 'long'],
                            'course'     => ['type' => 'long'],
                            'key'        => ['type' => 'keyword'],
                            'tags'       => ['type' => 'keyword'],
                            'status'     => ['type' => 'keyword'],
                            'name'       => ['type' => 'text'],
                            'created_at' => ['type' => 'date'],
                            'updated_at' => ['type' => 'date']
                        ]
                    ]
                ]
        ];

        foreach ($this->getFields() as $field) {
            if ($field['type'] == 'number') {
                $params['body']['mappings']['properties'][$field['key']] = ['type' => 'float'];
            }

            if (in_array($field['type'], ['string', 'file', 'feed_item'])) {
                $params['body']['mappings']['properties'][$field['key']] = ['type' => 'keyword', 'ignore_above' => 256];
            }

            if (in_array($field['type'], ['text', 'html'])) {
                $params['body']['mappings']['properties'][$field['key']] = ['type' => 'text'];
            }

            if ($field['type'] == 'bool') {
                $params['body']['mappings']['properties'][$field['key']] = ['type' => 'boolean'];
            }
        }

        $existIndex = $Elasticsearch->indices()->exists(['index' => "course{$this->id}"]);

        if ($existIndex) {
            if ($recreate) {
                $Elasticsearch->indices()->delete(['index' => "course{$this->id}"]);
            } else {
                throw new Exception('Индекс уже существует');
            }
        }

        $Elasticsearch->indices()->create($params);
    }

    /**
     * @return array|mixed
     */
    public function getFields()
    {
        $result = [];

        if (strlen($this->fields)) {
            $data = @json_decode($this->fields, true);
            $data = (!!$data && is_array($data)) ? $data : [];

            foreach ($data as $fieldData) {
                if ($fieldData['key'] && $fieldData['type']) {
                    $result[] = $fieldData;
                }
            }
        }

        return $result;
    }

    /**
     *
     */
    public function reindexItemsSearch()
    {
        $query = CourseUnit::where([
            ['feed_id', '=', $this->id],
            ['status', '=', 'ok'],
        ]);

        /** @var CourseUnit $item */
        foreach ($query->get() as $item) {
            $item->indexSearch();
        }
    }
}
