<?php
namespace Deployer;
require 'recipe/common.php';
require 'recipe/npm.php';

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@gitlab.com:troodon/lms2.git');
set('shared_files', ['backend/.env', 'platform/ormconfig.json', 'backend/storage/oauth-private.key', 'backend/storage/oauth-public.key']);
set('shared_dirs', ['backend/storage/app/public', 'backend/storage/framework/sessions'
                    , 'backend/storage/framework/testing'], 'backend/storage/framework/views', 'backend/storage/logs');

// Servers

host('lms.space')
//    ->stage('production')
    ->user('lms2')
    ->identityFile( '~/.ssh/id_rsa')
    ->set('deploy_path', '/var/www/lms.space');


// Tasks

desc('Restart PHP-FPM service');

task('frond:build', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/node_modules ]')) {
            run('cp -R {{previous_release}}/node_modules {{release_path}}');
        }
    }

    run("cd {{release_path}}/front && {{bin/npm}} install && {{bin/npm}} run build");
});

task('platform:build', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/node_modules ]')) {
            run('cp -R {{previous_release}}/node_modules {{release_path}}');
        }
    }

    run("cd {{release_path}}/platform && {{bin/npm}} install && {{bin/npm}} run build");
});

task('docs:build', function () {
    run("cd {{release_path}}/docs && {{bin/npm}} install && {{bin/npm}} run build");
});


task('reload:php-fpm', function () {
    run('sudo /etc/init.d/php7.4-fpm restart'); // Using SysV Init scripts
});

task('reload:nginx', function () {
    run('sudo /etc/init.d/nginx reload'); // Using SysV Init scripts
});

task('reload:platform', function () {
    run("cd {{release_path}}/platform && pm2 reload all");
});

task('reload:elasticsearch', function () {
    run("sudo /etc/init.d/elasticsearch restart");
});


task('deploy:vendors', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/backend/vendor ]')) {
            run('cp -R {{previous_release}}/backend/vendor {{release_path}}backend/');
        }
    }

    run("cd {{release_path}}/backend && {{bin/composer}} install --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest");
});

task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'frond:build',
    'platform:build',
    'docs:build',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'reload:php-fpm',
    'reload:elasticsearch',
    'reload:nginx',
    'reload:platform',
    'cleanup',
    'success'
]);


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
