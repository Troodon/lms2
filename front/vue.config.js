const path = require('path')

module.exports = {
    outputDir: 'dist',
    publicPath: undefined,
    // assetsDir: '../public/dist',
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,
    css: undefined,
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        disableHostCheck: true,
        inline:false
    },
    indexPath: path.resolve(__dirname, '../backend/resources/views/home.blade.php')
};
