import Vue from 'vue';
import Router from 'vue-router';
import PagesIndex from '../views/pages/index.vue';
import PagesEdit from '../views/pages/edit.vue';
import SettingsMain from '../views/settings/main.vue';
import ProfileMain from '../views/profile/main.vue';
import ProfileBilling from '../views/profile/billing.vue';
import ProfileBillingPayment from '../views/profile/billing.payment.vue';
import Home from '../views/home/home.vue';
import FilesIndex from '../views/files/index.vue';
import CoursesIndex from '../views/courses/index.vue';
import CourseEdit from '../views/courses/edit.vue';
import CourseUnits from '../views/courses/units.vue';
import CourseUnitEdit from '../views/courses/unit.edit.vue';
import TeamMain from '../views/team/main.vue';
import SiteInfo from '../views/home/site_info.vue';
import SettingsWelcome from '../views/settings/welcome.vue';
import FilesFTP from '../views/files/ftp.vue';
import HomeTerms from '../views/home/terms.vue';
import HomeTariffs from '../views/home/tariffs.vue';
import HomeSignUp from '../views/home/signup.vue';
import HomeLogin from '../views/home/login.vue';
import UsersIndex from '../views/users/index.vue';


const originalPush = Router.prototype.push
// @ts-ignore
Router.prototype.push = function push(location: any, onResolve: any, onReject: any) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)

    // @ts-ignore
    return originalPush.call(this, location).catch((err: any) => err)
}

Vue.use(Router);
/**
 *
 */
export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/tariffs',
            name: 'tariffs',
            component: HomeTariffs,
        },
        {
            path: '/signup',
            name: 'signup',
            component: HomeSignUp,
        },
        {
            path: '/login',
            name: 'login',
            component: HomeLogin,
        },
        {
            path: '/terms',
            name: 'terms',
            component: HomeTerms,
        },
        {
            path: '/profile',
            name: 'profile',
            component: ProfileMain
        },
        {
            path: '/billing',
            name: 'billing',
            component: ProfileBilling
        },
        {
            path: '/billing/payment/:paymentId',
            name: 'ProfileBillingPayment',
            component: ProfileBillingPayment
        },
        {
            path: '/:site/handlers',
            name: 'handlers',
            component: PagesIndex,
        },
        {
            path: '/:site/users',
            name: 'users',
            component: UsersIndex,
        },
        {
            path: '/:site/users/:user',
            name: 'user',
            component: PagesEdit
        },
        {
            path: '/:site/courses',
            name: 'courses',
            component: CoursesIndex,
        },
        {
            path: '/:site/courses/:course',
            name: 'course',
            component: CourseEdit
        },

        {
            path: '/:site/courses/:course/units',
            name: 'course_units',
            component: CourseUnits
        },
        {
            path: '/:site/courses/:course/units/new',
            name: 'course_unit_new',
            component: CourseUnitEdit
        },
        {
            path: '/:site/courses/:course/units/:id',
            name: 'course_unit',
            component: CourseUnitEdit
        },
        {
            path: '/:site/courses/new',
            name: 'course_new',
            component: CourseEdit
        },
        {
            path: '/:site/files',
            name: 'files',
            component: FilesIndex,
        },
        {
            path: '/:site/ftp',
            name: 'ftp',
            component: FilesFTP,
        },
        {
            path: '/:site/files/:path',
            name: 'files_path',
            component: FilesIndex,
        },
        {
            path: '/:site',
            name: 'site',
            component: SiteInfo
        },
        {
            path: '/:site/settings',
            name: 'settings',
            component: SettingsMain
        },
        {
            path: '/:site/control',
            name: 'control',
            component: SettingsMain
        },
        {
            path: '/:site/welcome',
            name: 'welcome',
            component: SettingsWelcome
        },
        {
            path: '/:site/team',
            name: 'team',
            component: TeamMain
        },
    ],
});
