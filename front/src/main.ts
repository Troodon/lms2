import Vue from 'vue';
import App from './components/App.vue';
import router from './router/router';
import store  from './store/index'
import "./components";
import VueYandexMetrika from 'vue-yandex-metrika';

Vue.use(VueYandexMetrika, {
  id: 54888034,
  router: router,
  env: process.env.NODE_ENV
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
