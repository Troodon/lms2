import Vue from "vue";
import Component from "vue-class-component";
import {Profile} from "../../types/api/profile";
import Layout from "../../components/layout";

@Component<ProfileMain>({
    name: "ProfileMain",
    components: {
        layout: Layout
    }
})
export default class ProfileMain extends Vue {
    profile_settings: Profile = {new_password: "", avatar: "", email: "", id: 1};

    created() {
        this.profile_settings=this.$store.state.profile.user;
    }
}
