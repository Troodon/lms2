import Vue from "vue";
import Component from "vue-class-component";
import Layout from "../../components/layout";
import axios from "axios";
import {format_number} from "../../helpers/misc_help";

@Component<ProfileBillingPayment>({
    name: "ProfileBillingPayment",
    components: {
        layout: Layout
    },
    filters: {
        format_number(val: string): string {
            return format_number(val);
        }
    }
})
export default class ProfileBillingPayment extends Vue {
    order: PaymentOrder = {amount: 0, id: 0, months: 0, sites: [], status: "notpaid"};
    payment_url: string = '';
    tariffs: { key: string, name: string, amount: number, amount_year: number } [] = [
        {
            key: 'basic',
            name: 'Базовый',
            amount: 500,
            amount_year: 400
        },
        {
            key: 'pro',
            name: 'Про',
            amount: 1000,
            amount_year: 800
        }
    ];

    /**
     *
     */
    created() {
        axios.get(
            `/api/payment/` + this.$route.params.paymentId
        )
            .then((response: { data: { success: boolean, error?: string, order?: any, payment_url: string } }) => {
                if (response.data.success) {
                    if (!!response.data.order) {
                        this.order = response.data.order;
                        console.log(this.order);
                        this.payment_url = response.data.payment_url
                    }
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            });
    }
}

interface PaymentOrder {
    id: number,
    status: string,
    amount: number,
    months: number,
    sites: { id: number, tariff: string, domain: string, name: string }[]
}
