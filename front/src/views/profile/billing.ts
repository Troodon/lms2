import Vue from "vue";
import Component from "vue-class-component";
import {Profile} from "../../types/api/profile";
import Layout from "../../components/layout";
import {Site} from "../../types/model/Site";
import axios from "axios";
import {User} from "../../types/common";
import {format_number} from "../../helpers/misc_help";

@Component<ProfileBilling>({
    name: "ProfileBilling",
    components: {
        layout: Layout
    },
    filters: {
        format_number(val: string): string {
            return format_number(val);
        }
    }
})
export default class ProfileBilling extends Vue {
    profile_settings: Profile = {new_password: "", avatar: "", email: "", id: 1};
    sites: Site[] = [];
    tariffs: { key: string, name: string, amount: number, amount_year: number } [] = [
        {
            key: 'basic',
            name: 'Базовый',
            amount: 500,
            amount_year: 400
        },
        {
            key: 'pro',
            name: 'Про',
            amount: 1000,
            amount_year: 800
        }
    ];
    payment_months: number = 1;
    payment_intervals: { name: string, months: number }[] = [
        {
            name: '1 Месяц',
            months: 1
        },
        {
            name: '3 Месяца',
            months: 3
        },
        {
            name: '1 год',
            months: 12
        }
    ];
    order: {
        id: number,
        name: string,
        domain: string,
        paid_to: string,
        tariff: string,
        amount: number,
        active: boolean
    }[] = [];
    form_sending: boolean = false;

    /**
     *
     */
    created() {
        this.profile_settings = this.$store.state.profile.user;
        this.load_sites();
    }

    /**
     *
     * @param key
     * @param months
     */
    tariff_amount(key: string, months: number = 1) {
        for (let tariff of this.tariffs) {
            if (tariff.key == key) {
                return (!!months && months >= 12) ? tariff.amount_year : tariff.amount;
            }
        }

        return 0;
    }

    /**
     *
     */
    total_amount() {
        let result = 0;

        for (let site of this.order) {
            if (site.active) {
                result += this.tariff_amount(site.tariff, this.payment_months) * this.payment_months;
            }
        }

        return result;
    }

    /**
     *
     */
    load_sites() {
        axios.get(
            `/api/userinfo`
        )
            .then((response: { data: { success: boolean, error?: string, user?: User } }) => {
                if (response.data.success) {
                    if (!!response.data.user && 'sites' in response.data.user && !!response.data.user.sites) {
                        this.sites = response.data.user.sites;

                        for (let site of this.sites) {
                            this.order.push({
                                active: true,
                                amount: 0,
                                id: site.id,
                                name: site.name,
                                domain: site.domain,
                                paid_to: !!site.active_to ? site.active_to : '–',
                                tariff: site.tariff
                            });
                        }
                    }
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

    place_order() {
        if (this.form_sending) return;

        this.form_sending = true;
        let order: { sites: { id: number, tariff: string }[], months: number, amount: number } = {
            sites: [],
            months: this.payment_months,
            amount: this.total_amount()
        };

        for (let site of this.order) {
            if (!site.active || site.tariff == 'demo') {
                continue;
            }
            order.sites.push({
                id: site.id,
                tariff: site.tariff
            });
        }

        axios.post(
            `/api/order`,
            order
        )
            .then((response: { data: { success: boolean, error?: string, payment_url?: string } }) => {
                if (response.data.success && !!response.data.payment_url) {
                    window.location.replace(response.data.payment_url);
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
                this.form_sending = false;
            })
            .catch(e => {
                this.form_sending = false;
                console.log(e)
            });
    }
}
