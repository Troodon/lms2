import Vue from "vue";
import Component from "vue-class-component";
import Layout from "../../components/layout.vue";
import {FileItem} from "../../types/api/file.item";
import Ace from "../../components/editor/ace.vue";
import FilesList from "./list.vue";
import FilesEdit from "./edit.vue";
import {ManagerPathItem} from "../../types/common";

@Component<FilesIndex>({
    name: "FilesIndex",
    components: {
        layout: Layout,
        ace: Ace,
        files_list: FilesList,
        files_edit: FilesEdit,
    },
    watch: {
        '$route.query.path'(newPath, oldPath) {
            this.path = !!newPath ? newPath : '/';
        },
        '$route.query.edit'(newPath, oldPath) {
            this.editor = !!newPath;
        }
    },
})
export default class FilesIndex extends Vue {
    current_site_id: number = 0;
    root_path: ManagerPathItem = {
        name: 'Корень /',
        path: '/'
    };
    loading: boolean = false;
    editor: boolean = false;
    current_path: ManagerPathItem[] = [this.root_path];
    path: string = '';
    parent_directory: FileItem | null = null;
    show_ftp: boolean = true;
    ftp_login: string = '';
    ftp_password: string = '';
    ftp_store_password: boolean = true;
    is_dragover: boolean = false;

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.editor = !!this.$route.query.edit;
    }
}
