import Vue from "vue";
import axios from "axios";
import Component from "vue-class-component";
import Layout from "../../components/layout.vue";
import {FileItem} from "../../types/api/file.item";
import Ace from "../../components/editor/ace.vue";

@Component<FilesFTP>({
    name: "files_index",
    components: {
        layout: Layout
    }
})
export default class FilesFTP extends Vue {
    current_site_id: number = 0;
    loading: boolean = false;
    is_saving: boolean = false;
    ftp_login: string = '';
    ftp_password: string = '';
    ftp_store_password: boolean = true;
    ftp_created: boolean = false;

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        axios.get(
            `/api/${this.current_site_id}/ftp`
        )
            .then(response => {
                if (response.data.success) {
                    this.ftp_login = response.data.ftp_login;
                    this.ftp_password = response.data.ftp_password;
                    this.ftp_created = response.data.ftp_created;
                    this.ftp_store_password = !response.data.ftp_hide_password;
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }



    /**
     *
     */
    saveFtp() {
        if (this.ftp_password.length < 6) {
            this.$toasted.error('Минимальная длинна пароля 6 символов', {type: 'error'});
            return;
        }

        this.is_saving = true;

        axios.post(
            `/api/${this.current_site_id}/ftp`,
            {
                ftp_password: this.ftp_password,
                ftp_hide_password: !this.ftp_store_password
            }
        )
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.$toasted.show('Настройки FTP успешно сохранёны');
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

}
