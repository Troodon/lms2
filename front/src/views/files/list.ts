import Vue from "vue";
import axios from "axios";
import Component from "vue-class-component";
import {FileItem} from "../../types/api/file.item";
import Ace from "../../components/editor/ace.vue";
import {clear_files_path} from "../../helpers/files.helper";

@Component<FilesList>({
    name: "FilesList",
    components: {
        ace: Ace,
    },
    watch: {
        '$route.query.path'(newPath, oldPath) {
            this.processPath(!!newPath ? newPath : '/');
        }
    },
    filters: {
        size: (bytes: number) => {
            let result: string = '';

            if (bytes >= 1073741824) {
                result = (bytes / 1073741824).toFixed(2) + " Гб";
            } else if (bytes >= 1048576) {
                result = (bytes / 1048576).toFixed(2) + " Мб";
            } else if (bytes >= 1024) {
                result = (bytes / 1024).toFixed(2) + " Кб";
            } else if (bytes > 1) {
                result = bytes + " байта";
            } else if (bytes == 1) {
                result = bytes + " байт";
            } else {
                result = "0";
            }

            return result;
        }
    }
})
export default class FilesList extends Vue {
    current_site_id: number = 0;
    root_path: ManagerPathItem = {
        name: 'Корень /',
        path: '/'
    };
    loading: boolean = false;
    editor: boolean = false;
    is_saving: boolean = false;
    current_path: ManagerPathItem[] = [this.root_path];
    path: string = '';
    files: any[] = [];
    directories: any[] = [];
    parent_directory: FileItem | null = null;
    parentdiname: string = '';
    file: FileItem = {
        bytes: 0,
        content: "",
        content_type: "",
        editable: false,
        id: 0,
        is_dir: false,
        last_modified: "",
        name: "",
        path: "",
        url: ""
    };
    file_type: string = 'text';
    show_files: boolean = true;
    show_ftp: boolean = true;
    ftp_login: string = '';
    ftp_password: string = '';
    ftp_store_password: boolean = true;
    is_dragover: boolean = false;

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.processPath(this.$route.query.path ? this.$route.query.path.toString() : '');
    }

    /**
     *
     * @param path
     */
    processPath(path: string) {
        this.path = clear_files_path(path);
        this.__update_parent();

        if (this.path == '' || !!this.path.match(/\/$/)) {
            this.navigate_manager(this.path);
        } else {
            axios.get(
                `/api/${this.current_site_id}/files/read?path=${this.path}`
            )
                .then((response: { data: { success: boolean, file?: FileItem } }) => {
                    if (response.data.success && response.data.file) {
                        if (response.data.file.is_dir) {
                            this.navigate_manager(this.path);
                        } else {
                            this.file = response.data.file;

                            if (this.file.content_type.startsWith('text/css')) {
                                this.file_type = 'css';
                            } else if (this.file.content_type.startsWith('application/javascript')) {
                                this.file_type = 'javascript';
                            }
                            this.parentdiname = this.path
                                .substring(0, this.path.lastIndexOf('/') + 1)
                                .replace(/(.+)\/$/, '$1');
                            this.editor = true;
                        }
                    } else {
                        this.$toasted.show('Произошла ошибка', {type: 'error'});
                    }
                })
                .catch(e => {
                    console.log(e)
                });
        }
    }

    /**
     *
     * @private
     */
    __update_parent() {
        this.path = clear_files_path(this.path);
        let parent_path = this.path.substr(0, this.path.replace(/(.+)\/$/, '$1').lastIndexOf('/')) + '/';

        if (this.path == '') {
            this.parent_directory = null;
        } else {
            this.parent_directory = {
                bytes: 0,
                content: "",
                content_type: "application/directory",
                editable: false,
                id: 0,
                is_dir: true,
                last_modified: '',
                name: parent_path.replace(/\/$/, "").split('/')[parent_path.replace(/\/$/, "").split('/').length - 1],
                path: parent_path,
                url: ''
            };
        }
    }

    /**
     *
     * @private
     */
    __update_breadcrumb() {
        this.current_path = [
            this.root_path
        ];
        let last_path = '';

        for (let item of this.path.split('/')) {
            let path = last_path + item;

            if (!!item && path != '') {
                this.current_path.push({name: item, path: clear_files_path(path)});
            }

            last_path = item + '/';
        }
    }

    /**
     *
     * @param path
     */
    navigate_manager(path: string) {
        if (this.loading) return;
        this.editor = false;
        this.loading = true;
        this.files = [];
        this.directories = [];
        this.path = clear_files_path(path);

        this.__update_parent();
        this.__update_breadcrumb();
        axios.get(
            `/api/${this.current_site_id}/files?path=${this.path}`
        )
            .then((response: { data: { success: boolean, files?: FileItem[], directories: FileItem[] } }) => {
                if (response.data.success) {
                    this.files = response.data.files ? response.data.files : [];
                    this.directories = response.data.directories ? response.data.directories : [];

                    this.loading = false;
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });

    }

    /**
     *
     * @param file
     */
    deleteFile(file: FileItem) {
        if (!confirm(`Удалить файл ${file.name}`)) {
            return;
        }

        this.is_saving = false;

        axios.post(`/api/${this.current_site_id}/files/delete`, {path: file.path})
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.navigate_manager(this.path)
                } else alert('Ошибка');
            })
            .catch(e => {
                console.log(e)
            })
    }


    /**
     *
     * @param event
     */
    async onFilesSelected(event: any) {
        event.preventDefault();

        if (!!event.target && !!event.target.files) {
            for (let file of event.target.files) {
                await this.load_file(file);
            }

            this.navigate_manager(this.path);
        }
    }

    /**
     *
     * @param file
     */
    async load_file(file: File) {
        const fd = new FormData();
        fd.append('file', file);
        fd.append('path', `${this.path}${file.name}`);
        const {data} = await axios.post(`/api/${this.current_site_id}/files/upload`, fd);
        this.$toasted.show(`Файл успешно ${data.file.name} загружен`);

        return {
            ext: data.file.name.replace(/.+\.([^\.]+)/, '$1'),
            name: data.file.name.replace(/^.*[\\\/]/, ''),
            path: data.file.path.toString(),
            size: "",
            type: ""
        };
    }

    /**
     *
     */
    handleDragover(e: DragEvent) {
        e.preventDefault();

        if (!!e.dataTransfer && !!e.dataTransfer.types && e.dataTransfer.types[0] && e.dataTransfer.types[0] == 'Files') {
            this.is_dragover = true;
        } else {
            this.is_dragover = false;
        }
    }

    /**
     *
     */
    handleDragleave(e: Event) {
        e.preventDefault();
        this.is_dragover = false;
    }

    /**
     *
     * @param event
     */
    async handleDrag(event: any) {
        event.preventDefault();

        if (!!event.dataTransfer && !!event.dataTransfer.files) {
            let need_refresh = false;
            for (let file of event.dataTransfer.files) {
                await this.load_file(file);
                need_refresh = true;
            }

            if (need_refresh) this.navigate_manager(this.path);
        }

        this.is_dragover = false;
    }

}

/**
 *
 */
interface ManagerPathItem {
    name: string;
    path: string;
}
