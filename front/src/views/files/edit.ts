import Vue from "vue";
import Component from "vue-class-component";
import {FileItem} from "../../types/api/file.item";
import Ace from "../../components/editor/ace.vue";
import FilesList from "./list";
import {ManagerPathItem} from "../../types/common";
import {clear_files_path} from "../../helpers/files.helper";
import axios from "axios";

@Component<FilesEdit>({
    name: "FilesEdit",
    components: {
        ace: Ace,
        files_list: FilesList,
    },
    watch: {
        '$route.params.edit'(newPath, oldPath) {
            this.path = this.$route.query.edit ? this.$route.query.edit.toString() : '';
            this.loadFile();
        }
    },
    filters: {
        size: (bytes: number) => {
            let result: string = '';

            if (bytes >= 1073741824) {
                result = (bytes / 1073741824).toFixed(2) + " Гб";
            } else if (bytes >= 1048576) {
                result = (bytes / 1048576).toFixed(2) + " Мб";
            } else if (bytes >= 1024) {
                result = (bytes / 1024).toFixed(2) + " Кб";
            } else if (bytes > 1) {
                result = bytes + " байта";
            } else if (bytes == 1) {
                result = bytes + " байт";
            } else {
                result = "0";
            }

            return result;
        }
    }
})
export default class FilesEdit extends Vue {
    current_site_id: number = 0;
    root_path: ManagerPathItem = {
        name: 'Корень /',
        path: '/'
    };
    loading: boolean = false;
    editor: boolean = false;
    is_saving: boolean = false;
    current_path: ManagerPathItem[] = [this.root_path];
    path: string = '';
    files: any[] = [];
    directories: any[] = [];
    parent_directory: FileItem | null = null;
    file: FileItem = {
        bytes: 0,
        content: "",
        content_type: "",
        editable: false,
        id: 0,
        is_dir: false,
        last_modified: "",
        name: "",
        path: "",
        url: ""
    };
    file_type: string = 'text';
    show_files: boolean = true;
    show_ftp: boolean = true;
    ftp_login: string = '';
    ftp_password: string = '';
    ftp_store_password: boolean = true;
    is_dragover: boolean = false;

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.path = this.$route.query.edit ? this.$route.query.edit.toString() : '';
        this.__update_parent();
        this.loadFile();
        this.$root.$on('file_change', (val: string) => {
            this.file.content = val;
        });
    }

    loadFile() {
        axios.get(
            `/api/${this.current_site_id}/files/read?path=${this.path}`
        )
            .then((response: { data: { success: boolean, file?: FileItem } }) => {
                if (response.data.success && response.data.file) {
                    if (!response.data.file.is_dir) {
                        this.file = response.data.file;

                        if (this.file.content_type.startsWith('text/css')) {
                            this.file_type = 'css';
                        } else if (this.file.content_type.startsWith('application/javascript')) {
                            this.file_type = 'javascript';
                        }

                        this.editor = true;
                    } else {
                        this.$toasted.show('Произошла ошибка', {type: 'error'});
                    }
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })

    }

    /**
     *
     */
    saveFile() {
        this.is_saving = true;

        axios.post(
            `/api/${this.current_site_id}/files/update?path=${this.path}`,
            this.file.content
        )
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.file = response.data.file;
                    this.$toasted.show('Файл успешно сохранён');
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

    /**
     *
     */
    __update_parent() {
        this.path = clear_files_path(this.path);
        let parent_path = this.path.substr(0, this.path.replace(/(.+)\/$/, '$1').lastIndexOf('/')) + '/';

        if (this.path == '') {
            this.parent_directory = null;
        } else {
            this.parent_directory = {
                bytes: 0,
                content: "",
                content_type: "application/directory",
                editable: false,
                id: 0,
                is_dir: true,
                last_modified: '',
                name: parent_path.replace(/\/$/, "").split('/')[parent_path.replace(/\/$/, "").split('/').length - 1],
                path: parent_path,
                url: ''
            };
        }
    }

}
