import {Component, Vue} from 'vue-property-decorator';
import Layout from "../../components/layout";

@Component({
    components: {
        layout: Layout
    },
})
export default class HomeLogin extends Vue {
    user_email: string = '';
    user_password: string = '';
    processing: boolean = false;
    show_password: boolean = false;

    /**
     *
     */
    created() {
        document.title = 'LMS – Вход';

        if (this.$store.state.profile.authorized) {
            this.$router.push({name: 'home'});
        }
    }

    /**
     *
     */
    signIn(e: Event) {
        e.preventDefault();
        if (this.processing || !this.user_email) return;
        this.processing = true;

        this.$store.dispatch("profile/login", {
            email: this.user_email,
            password: this.user_password
        }).then(([result, error]) => {
            this.processing = false;
            if (result) {
                window.location.replace('/');
            } else {
                this.$toasted.error(`Неправильный логин или пароль`);
            }
        });
    }

}
