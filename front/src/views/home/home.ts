import {Component, Vue} from 'vue-property-decorator';
import Layout from '../../components/layout';
import axios from 'axios';
import {Site} from '../../types/model/Site';
import SmoothScrollbar from 'smooth-scrollbar'

@Component({
    components: {
        layout: Layout
    },
})
export default class Home extends Vue {
    user: any = null;
    agree: boolean = false;
    user_email: string = '';
    user_password: string = '';
    user_url: string = '';
    form_signin: boolean = false;
    sites: Site[] = [];
    show_form: boolean = false;
    site_name: string = '';
    site_sending: boolean = false;
    processing: boolean = false;
    is_chrome = false;
    timeout: number = 0;
    scrollbar: SmoothScrollbar | null = null;
    scenes: any[] = [];

    /**
     *
     */
    created() {
        this.is_chrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        this.$store
            .dispatch('profile/userinfo')
            .then(([result, error]) => {
                this.user = result;
                this.load_sites();
            });
        if (!!this.$store.state.profile.authorized && !!this.$store.state.profile.user) {
            this.user = this.$store.state.profile.user;
        }
    }

    mounted() {
        const titleScrollBar: any = this.$refs.titleScrollBar;

        if (titleScrollBar) {
            this.scrollbar = titleScrollBar.scrollbar;
        }
    }

    /**
     *
     */
    load_sites() {
        this.sites = this.$store.getters['profile/sites'];
    }

    /**
     *
     */
    change_form(e: Event) {
        this.form_signin = !this.form_signin;
        e.preventDefault();

        return false;
    }

    /**
     *
     */
    scrollToSignUp() {
        // @ts-ignore
        this.$refs.sign_up.scrollIntoView();
    }

    /**
     *
     */
    refreshAnimation() {
        // console.log('refreshAnimation')
        this.scenes.forEach(function (scene: any) {
            scene.refresh();
        });
        // if (this.is_chrome) {
        //     this.$scrollmagic.update();
        // }
    }

    /**
     *
     * @param slide_ref
     */
    scroll_to_slide(slide_ref: string) {
        if (this.scrollbar && slide_ref in this.$refs && this.$refs[slide_ref] && this.$refs[slide_ref].constructor.name == 'HTMLElement') {
            // @ts-ignore
            let slide: HTMLElement = this.$refs[slide_ref];
            // @ts-ignore
            this.scrollbar.scrollIntoView(document.getElementById(slide_ref), {
                onlyScrollIfNeeded: true,
            });
            this.timeout = 1500;
            this.refreshAnimation();
            //this.scrollbar.scrollTo(0, slide.offsetTop, 500, this.refreshAnimation);
        }
    }

    /**
     *
     */
    signIn(e: Event) {
        e.preventDefault();
        if (this.processing || !this.user_email) return;
        this.processing = true;
        this.$store.dispatch('profile/login', {
            email: this.user_email,
            password: this.user_password
        }).then(([result, error]) => {
            this.processing = false;
            if (result) {
                window.location.reload();
            } else alert(error);
        });
    }

    /**
     *
     */
    signUp(e: Event) {
        e.preventDefault();
        if (this.processing || !this.user_email) return;
        this.processing = true;

        axios.post(
            `/api/register`,
            {
                email: this.user_email,
                password: this.user_password
            }
        )
            .then(response => {
                this.processing = false;
                if (response.data.success) {
                    this.$store.dispatch('profile/login', {
                        email: this.user_email,
                        password: this.user_password
                    }).then(([result, error]) => {
                        if (result) {
                            window.location.reload();
                        }
                        if (error) {
                            console.log(error);
                        }
                    });
                } else if (response.data.error) {
                    alert(response.data.error);

                }
            })
            .catch(e => {
                this.processing = false;
                console.log(e)
            })
    }

    /**
     *
     * @param site
     */
    set_site(site: any) {
        this.$store.dispatch('set_site_id', site.id);
        this.$router.push(`/${site.id}`)
    }

    /**
     *
     */
    new_site() {
        this.show_form = true;
    }

    /**
     *
     */
    create_site() {
        if (this.site_sending) {
            this.$toasted.error('Идёт создание сайта');
            return;
        }
        this.site_sending = true;
        axios.post(`/api/sites/create`,
            {
                name: this.site_name
            })
            .then(response => {
                this.site_sending = false;
                if (response.data.success && response.data.site) {
                    this.$store.dispatch('set_site_id', response.data.site);
                    this.$router.push(`/${response.data.site.id}/welcome`)
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                this.site_sending = false;
                this.$toasted.show('Произошла ошибка', {type: 'error'});
                console.log(e)
            });
    }
}
