import {Component, Vue} from 'vue-property-decorator';
import Layout from "../../components/layout";

@Component({
    components: {
        layout: Layout
    },
})
export default class HomeTariffs extends Vue {
    authorized: boolean = false;

    created() {
        this.authorized = this.$store.state.profile.authorized;
    }
}
