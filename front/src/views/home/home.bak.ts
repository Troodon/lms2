import {Component, Vue} from 'vue-property-decorator';
import Layout from "../../components/layout";
import axios from "axios";
import {Site} from "../../types/model/Site";

@Component({
    components: {
        layout: Layout
    },
})
export default class Home extends Vue {
    user: any = null;
    agree: boolean = false;
    user_email: string = '';
    user_password: string = '';
    user_url: string = '';
    form_signup: boolean = false;
    sites: Site[] = [];
    show_form: boolean = false;
    site_name: string = '';
    site_sending: boolean = false;

    /**
     *
     */
    created() {
        this.$store
            .dispatch("profile/userinfo")
            .then(([result, error]) => {
                this.user = result;
                this.load_sites();
            });
        if (!!this.$store.state.profile.authorized && !!this.$store.state.profile.user) {
            this.user = this.$store.state.profile.user;
        }
    }

    /**
     *
     */
    load_sites() {
        this.sites = this.$store.getters['profile/sites'];
    }

    /**
     *
     */
    change_form(e: Event) {
        this.form_signup = !this.form_signup;
        e.preventDefault();

        return false;
    }

    /**
     *
     */
    signIn() {
        console.log({
            email: this.user_email,
            password: this.user_password
        });
        this.$store.dispatch("profile/login", {
            email: this.user_email,
            password: this.user_password
        }).then(([result, error]) => {
            if (result) {
                window.location.reload();
            } else alert(error);
        });
    }

    /**
     *
     */
    signUp() {
        axios.post(
            `/api/register`,
            {
                email: this.user_email,
                url: this.user_url,
                password: this.user_password
            }
        )
            .then(response => {
                if (response.data.success) {
                    this.$store.dispatch("profile/login", {
                        email: this.user_email,
                        password: this.user_password
                    }).then(([result, error]) => {
                        if (result) {
                            this.$router.push("/dashboard/1")
                        }
                        if (error) {
                            alert(error);

                        }
                    });
                } else if (response.data.error) {
                    alert(response.data.error);

                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    /**
     *
     * @param site
     */
    set_site(site: any) {
        this.$store.dispatch('set_site_id', site.id);
        this.$router.push(`/${site.id}`)
    }

    /**
     *
     */
    new_site() {
        this.show_form = true;
    }

    /**
     *
     */
    create_site() {
        if (this.site_sending) {
            this.$toasted.error('Идёт создание сайта');
            return;
        }
        this.site_sending = true;
        axios.post(`/api/sites/create`,
            {
                name: this.site_name
            })
            .then(response => {
                this.site_sending = false;
                if (response.data.success && response.data.site) {
                    this.$store.dispatch('set_site_id', response.data.site);
                    this.$router.push(`/${response.data.site.id}/welcome`)
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                this.site_sending = false;
                this.$toasted.show('Произошла ошибка', {type: 'error'});
                console.log(e)
            });
    }
}
