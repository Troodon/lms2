import {Component, Vue} from 'vue-property-decorator';
import Layout from '../../components/layout';
import axios from 'axios';

@Component({
    components: {
        layout: Layout
    },
})
export default class HomeSignUp extends Vue {
    user_email: string = '';
    user_domain: string = '';
    user_password: string = '';
    processing: boolean = false;
    authorized: boolean = false;
    show_password: boolean = false;

    /**
     *
     */
    created() {
        document.title = 'LMS – Регистрация';

        if (this.$store.state.profile.authorized) {
            this.$router.push({name: 'home'});
        }
    }

    /**
     *
     * @param e
     */
    async signUp(e: Event) {
        e.preventDefault();
        if (this.processing || !this.user_email) return;
        this.processing = true;

        const {data} = await axios.post(
            `/api/register`,
            {
                domain: this.user_domain,
                email: this.user_email,
                password: this.user_password
            }
        );

        if (data.success) {
            this.$store.dispatch('profile/login', {
                email: this.user_email,
                password: this.user_password
            }).then(([result, error]) => {
                if (result) {
                    window.location.replace('/');
                }
                if (error) {
                    console.log(error);
                }
            });
            this.processing = false;
        } else if (data.error) {
            alert(data.error);
            this.processing = false;
        } else {
            this.$toasted.error(`Ошибка регистрации`);
        }
    }

}
