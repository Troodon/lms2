import {Component, Vue} from 'vue-property-decorator';
import Layout from "../../components/layout";
import axios from "axios";
import {Site} from "../../types/model/Site";

@Component({
    components: {
        layout: Layout
    },
})
export default class HomeTerms extends Vue {
    authorized: boolean = false;

    created() {
        this.authorized = this.$store.state.profile.authorized;
    }
}
