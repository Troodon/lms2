import Vue from 'vue';
import Component from 'vue-class-component';
import Layout from '../../components/layout.vue';
import {DraggableTree} from 'vue-draggable-nested-tree'
import axios from 'axios';
import {Feed} from '../../types/api/feed';
import ApiCourses from '../../helpers/api.courses';
import {Course} from '../../types/api/course';

@Component<CoursesIndex>({
    name: 'CoursesIndex',
    components: {
        tree: DraggableTree,
        layout: Layout
    }
})
export default class CoursesIndex extends Vue {
    is_saving: boolean = false;
    router_tree: any;
    current_site_id: number = 0;
    courses: Course[] = [];

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.loadLists();
    }

    /**
     *
     */
    async loadLists() {
        const {data} = await ApiCourses.list(this.current_site_id);

        if (data.success) {
            if (data.courses) {
                this.courses = data.courses;
            }
        } else if (data.error) {
            this.$toasted.show(data.error, {type: 'error'});
        } else {
            this.$toasted.show('Произошла ошибка', {type: 'error'});
        }
    }

    /**
     *
     * @param feed
     */
    deleteList(feed: Feed) {
        if (!confirm('Удалить лист и его элементы?')) {
            return;
        }
        this.is_saving = false;

        axios.post(`/api/${this.current_site_id}/feeds/${feed.key}`, {status: 'deleted'})
            .then(response => {
                this.is_saving = false;
                this.$toasted.show('Лист удалён');

                if (response.data.success) {
                    this.loadLists();
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            })
    }
}
