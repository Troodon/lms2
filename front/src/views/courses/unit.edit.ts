import Vue from 'vue';
import Component from 'vue-class-component';
import Layout from '../../components/layout';
import axios from 'axios';
import Editor from '@tinymce/tinymce-vue'
import moment from 'moment/moment';
import {CoolSelect} from 'vue-cool-select';
import {CourseUnit} from '../../types/api/course.unit';
import {Course} from '../../types/api/course';
import ApiCourses from '../../helpers/api.courses';

@Component<CourseUnitEdit>({
    name: 'CourseUnitEdit',
    components: {
        layout: Layout,
        editor: Editor,
        CoolSelect
    }
})
export default class CourseUnitEdit extends Vue {
    course: Course | null = null;
    course_unit: CourseUnit = {
        description: '', type: '', content: '', image: '',
        id: 0,
        name: '',
        site_id: 0,
        status: '',
        created_at: '',
        updated_at: ''
    };
    current_site_id: number = 0;
    current_course_id: number = 0;
    is_saving: boolean = false;
    course_unit_public: boolean = true;

    /**
     *
     * @param blobInfo
     * @param success
     * @param failure
     * @param progress
     */
    async tinymce_image_upload(blobInfo: any, success: (a: string) => void, failure: (a: string) => void, progress: any) {
        let current_date = moment(),
            cdn_url = `https://s${this.current_site_id}.tcdn.site/`,
            upload_dir = `uploads/courses/${this.current_course_id}/${+this.course_unit.id}/${current_date.format('YYYY-MM-DD')}/`,
            upload_path = upload_dir + blobInfo.filename().replace(/[^\w\d\-\_\.]/gi, '-').replace(/[\-]+/gi, '-');
        try {
            const fd = new FormData();
            fd.append('file', blobInfo.blob());
            fd.append('path', upload_path);
            const {data} = await axios.post(`/api/${this.current_site_id}/storage/upload`, fd);

            if (data.error) {
                return failure(data.error);
            }

            this.$toasted.show(`Файл успешно ${blobInfo.filename()} загружен`);
            success(cdn_url + data.file.path.toString());
        } catch (e) {
            return failure('Error: ' + e.message);
        }
    }

    /**
     *
     */
    async created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.current_course_id = (this.$route.params.course && this.$route.params.course != 'new') ? +this.$route.params.course : 0;
        this.course_unit.id = this.$route.params.id ? +this.$route.params.id : 0;

        if (this.current_course_id) {
            const {data} = await ApiCourses.get(this.current_site_id, this.current_course_id);

            if (data.success && data.course) {
                this.course = data.course;
            } else if (data.error) {
                this.$toasted.show(data.error, {type: 'error'});
            } else {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
            }
        } else {
            this.$router.push(`/${this.current_site_id}/courses`);
            return;
        }

        if (this.course_unit.id && this.$route.params.course != 'new') {
            const {data} = await ApiCourses.unit(this.current_site_id, this.current_course_id, this.course_unit.id);

            if (data.success && data.course_unit) {
                this.course_unit = data.course_unit;
            } else if (data.error) {
                this.$toasted.show(data.error, {type: 'error'});
            } else {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
            }
        }
    }

    /**
     *
     * @param e
     */
    async save_course_unit(e: Event) {
        e.preventDefault();
        try {
            if (this.is_saving) return;
            let url_request = `/api/${this.current_site_id}/courses/${this.current_course_id}/units/` + (this.course_unit.id ? this.course_unit.id : 'new');
            this.is_saving = true;
            this.course_unit.id = +this.course_unit.id;
            this.course_unit.status = !!this.course_unit_public ? 'ok' : 'draft';

            const {data} = await axios.post(url_request, this.course_unit);
            this.is_saving = false;
            if (data.success) {
                this.$toasted.show('Материал курса успешно сохранён');
                this.course_unit = data.course_unit;
                this.$router.push({
                    name: 'course_unit', params: {
                        site: '' + this.current_site_id,
                        course: '' + this.current_course_id,
                        id: data.feed_item.id
                    }
                })
            } else if (data.error) {
                this.$toasted.show(data.error, {type: 'error'});
            } else {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
            }
        } catch (e) {
            console.log(e)

        }
    }

    /**
     *
     * @param event
     */
    async onFilesSelected(event: any) {
        event.preventDefault();

        if (!!event.target && !!event.target.files) {

            for (let file of event.target.files) {
                let uploaded = await this.upload_file(file);

                if (uploaded) {
                    this.course_unit.content = uploaded.path;
                    this.$forceUpdate();
                }
            }
        }
    }

    /**
     *
     * @param file
     */
    async upload_file(file: File) {
        const fd = new FormData();
        let current_date = new Date();
        let path = `/uploads/courses/${this.current_course_id}/${this.course_unit.id}/${current_date.getFullYear()}/${current_date.getMonth() + 1}/${current_date.getDate()}`;
        fd.append('file', file);
        fd.append('path', `${path}/${file.name}`);
        const {data} = await axios.post(`/api/${this.current_site_id}/storage/upload`, fd);
        this.$toasted.show(`Файл успешно ${data.file.name} загружен`);

        return {
            ext: data.file.name.replace(/.+\.([^\.]+)/, '$1'),
            name: data.file.name.replace(/^.*[\\\/]/, ''),
            path: data.file.path.toString(),
            size: '',
            type: ''
        };
    }
}
