import Vue from 'vue';
import axios from 'axios';
import Component from 'vue-class-component';
import Layout from '../../components/layout.vue';
import {DraggableTree} from 'vue-draggable-nested-tree'
import {FeedItem} from '../../types/api/feed.item';
import * as JsSearch from 'js-search';
import {CourseUnit} from '../../types/api/course.unit';
import {Course} from '../../types/api/course';
import ApiCourses from '../../helpers/api.courses';

@Component<CourseUnits>({
    name: 'CourseUnits',
    components: {
        tree: DraggableTree,
        layout: Layout
    },
    computed: {
        filtered_feed_items() {
            if (this.search_engine && this.filter_text) {
                return this.search_engine.search(this.filter_text);
            }
            return this.course_units;
        }
    }
})
export default class CourseUnits extends Vue {
    course: Course = {
        description: '', id: 0,
        name: '',
        site_id: 0,
        status: '',
        created_at: '',
        updated_at: ''
    };
    protected is_saving: boolean = false;
    protected router_tree: any;
    protected current_site_id: number = 0;
    protected current_course_id: number = 0;
    protected course_units: CourseUnit[] = [];
    protected current_page = 1;
    protected per_page = 250;
    protected filter_text: string = '';
    protected search_engine?: JsSearch.Search;

    /**
     *
     */
    async created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.current_course_id = this.$route.params.course ? +this.$route.params.course : 0;
        this.search_engine = new JsSearch.Search('id');
        this.search_engine.addIndex('name');
        this.search_engine.addIndex('description');

        if (this.current_course_id) {
            const {data} = await ApiCourses.get(this.current_site_id, this.current_course_id);

            if (data.success && data.course) {
                this.course = data.course;
            } else if (data.error) {
                this.$toasted.show(data.error, {type: 'error'});
            } else {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
            }
        }

        this.load_course_units();
    }

    /**
     *
     * @param skip
     * @param silent
     */
    async load_course_units(skip: number = 0, silent: boolean = false) {
        // const {data}: { data: { success: boolean, total?: number, error?: string, course_units?: CourseUnit[] } } = await axios.get(`/api/${this.current_site_id}/courses/${this.current_course_id}/units?limit=${thi.per_page}&skip=${skip}${silent ? '#silent' : ''}`);
        const {data}: { data: { success: boolean, total?: number, error?: string, course_units?: CourseUnit[] } } = await ApiCourses.units(this.current_site_id, this.current_course_id, this.per_page, skip, true);

        if (data.success && data.course_units && data.total) {
            this.course_units = this.course_units.concat(data.course_units);

            if (this.search_engine && this.course_units[0]) {
                this.search_engine.addDocuments(this.course_units);
            }

            if (data.total > this.course_units.length) {
                this.load_course_units(skip + this.per_page, true)
            }
        } else if (data.error) {
            this.$toasted.show(data.error, {type: 'error'});
        } else {
            this.$toasted.show('Произошла ошибка', {type: 'error'});
        }
    }

    /**
     *
     * @param e
     * @param course_unit
     */
    delete_course_unit(e: Event, course_unit: CourseUnit) {
        if (!confirm('Удалить запись?')) {
            return;
        }
        this.is_saving = false;

        axios.post(`/api/${this.current_site_id}/courses/${this.current_course_id}/units/${course_unit.id}`, {status: 'deleted'})
            .then((response: {
                data: { success: boolean, error?: string, feed_item?: FeedItem }
            }) => {
                this.is_saving = false;

                if (response.data.success) {
                    this.load_course_units();
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            })
    }
}
