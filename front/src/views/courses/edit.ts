import Vue from 'vue';
import Component from 'vue-class-component';
import Layout from '../../components/layout';
import axios from 'axios';
import {Course} from '../../types/api/course';

@Component<CourseEdit>({
    name: 'CourseEdit',
    components: {
        layout: Layout
    }
})
export default class CourseEdit extends Vue {
    course: Course = {
        description: '', id: 0,
        name: '',
        site_id: 0,
        status: '',
        created_at: '',
        updated_at: ''
    };
    current_site_id: number = 0;
    current_course_id: number = 0;
    is_saving: boolean = false;

    /**
     *
     */
    async created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.current_course_id = this.$route.params.course ? +this.$route.params.course : 0;
        this.course.site_id = this.current_site_id;
        this.course.id = this.current_course_id;

        if (this.current_course_id) {
            const {data} = await axios.get(
                `/api/${this.current_site_id}/courses/${this.current_course_id}`
            );
            if (data.success && data.course) {
                this.course = data.course;
            } else if (data.error) {
                this.$toasted.show(data.error, {type: 'error'});
            } else {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
            }
        }
    }

    /**
     *
     */
    saveCourse(e: Event) {
        e.preventDefault();
        if (this.is_saving) return;
        let url_request = (!!this.$route.params.course && this.$route.params.course != 'new') ? `/api/${this.current_site_id}/courses/${this.current_course_id}` : `/api/${this.current_site_id}/courses/new`;
        this.is_saving = true;
        this.course.id = +this.course.id;

        axios.post(url_request, this.course)
            .then(response => {
                this.is_saving = false;

                if (response.data.success) {
                    this.course = response.data.course;
                    this.$router.push(`/${this.current_site_id}/courses/${this.course.id}`);
                    this.$toasted.show('Лист успешно сохранён');
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

}
