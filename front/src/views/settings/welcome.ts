import Vue from "vue";
import Component from "vue-class-component";
import {SiteSettings} from "../../types/api/site.settings";
import Layout from "../../components/layout";
import axios from 'axios';

@Component<SettingsWelcome>({
    name: "SettingsWelcome",
    components: {
        layout: Layout
    }
})
export default class SettingsWelcome extends Vue {
    org_id: number = 0;
    site_settings: SiteSettings = {
        tariff: "basic",
        slash_redirect: "",
        created_at: "", updated_at: "",
        has_certificate: false,
        uses_cdn: false,
        https_redirect: "", www_redirect: "", domain: "unic.edu.ru", name: "УНИК"};
    is_saving: boolean = false;

    /**
     *
     */
    created() {
        this.org_id = Number(this.$route.params.site);
        axios.get(
            `/api/sites/${this.org_id}`
        )
            .then(response => {
                if (response.data.success) {
                    this.site_settings.domain = response.data.site.domain;
                    this.site_settings.name = response.data.site.name;
                } else alert('Ошибка');
            })
            .catch(e => {
                console.log(e)
            });

    }

    /**
     *
     */
    saveSite() {
        this.is_saving = true;
        axios.post(`/api/sites/${this.org_id}`,
            {
                domain:this.site_settings.domain,
                name:this.site_settings.name
            })
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.site_settings.domain = response.data.site.domain;
                    this.site_settings.name = response.data.site.name;
                } else alert('Ошибка');
            })
            .catch(e => {
                console.log(e)
            });
    }
}
