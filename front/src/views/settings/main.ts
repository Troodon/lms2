import Vue from 'vue';
import Component from 'vue-class-component';
import {SiteSettings} from '../../types/api/site.settings';
import Layout from '../../components/layout';
import axios from 'axios';
import {format_site} from '../../helpers/site.helper';

@Component<SettingsMain>({
    name: 'SettingsMain',
    components: {
        layout: Layout
    }
})
export default class SettingsMain extends Vue {
    org_id: number = 0;
    site_settings: SiteSettings = {
        tariff: 'pro',
        created_at: '', updated_at: '',
        has_certificate: false,
        uses_cdn: false,
        https_redirect: '', www_redirect: '', domain: 'unic.edu.ru', name: 'УНИК', slash_redirect: ''
    };
    is_saving: boolean = false;

    /**
     *
     */
    created() {
        this.org_id = Number(this.$route.params.site);
        axios.get(
            `/api/${this.org_id}`
        )
            .then(response => {
                if (response.data.success) {
                    this.site_settings = format_site(response.data.site);
                } else if (response.data.error) {
                    this.$toasted.error(response.data.error);
                } else {
                    alert('Ошибка')
                }

            })
            .catch(e => {
                this.$toasted.error(e.message);
                console.log(e)
            });

    }

    /**
     *
     * @param e
     */
    certificate_deactivate(e: Event) {
        if (!this.site_settings.has_certificate) {
            this.site_settings.https_redirect = '';
        } else {
            this.$toasted.show('Получение сертификата может занять до 30 минут!');
        }
    }

    /**
     *
     */
    saveSite() {
        this.is_saving = true;
        axios.post(`/api/${this.org_id}`,
            {
                domain: this.site_settings.domain,
                name: this.site_settings.name,
                www_redirect: this.site_settings.www_redirect,
                https_redirect: this.site_settings.https_redirect,
                slash_redirect: this.site_settings.slash_redirect,
                has_certificate: this.site_settings.has_certificate
            })
            .then((response: { data: { site?: SiteSettings, success: boolean, error?: string } }) => {
                this.is_saving = false;
                if (response.data.success && response.data.site) {
                    this.site_settings = format_site(response.data.site);
                } else if (response.data.error) {
                    this.$toasted.error(response.data.error);
                } else {
                    alert('Ошибка')
                }

            })
            .catch(e => {
                console.log(e)
            });
    }

    /**
     *
     */
    purge_cdn() {
        this.is_saving = true;
        axios.post(`/api/${this.org_id}/purge_cdn`,
            {
                purge: true
            })
            .then((response: { data: { site: SiteSettings, success: boolean } }) => {
                this.is_saving = false;
                if (response.data.success) {
                    this.$toasted.show('Очистка кэша может занять до 30 минут!');
                } else alert('Ошибка');
            })
            .catch(e => {
                console.log(e)
            });
    }
}
