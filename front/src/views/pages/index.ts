import Vue from "vue";
import Component from "vue-class-component";
import Layout from "../../components/layout.vue";
import {RouterItem} from "../../types/model/RouterItem";
import {DraggableTree} from 'vue-draggable-nested-tree'
import axios from "axios";
import {format_page_router_item} from "../../helpers/pages.helper";
import {Page} from "../../types/api/page";
import * as JsSearch from 'js-search';

@Component<PagesIndex>({
    name: "pages_index",
    components: {
        tree: DraggableTree,
        layout: Layout
    },
    watch: {
        filter_text: function (query: string) {
            if (this.search_engine && query) {
                let result = this.search_engine.search(query).map((x: any) => x.id)
                this.filter_pages(result);
            } else {
                this.filter_pages();
            }
        }
    },
})
export default class PagesIndex extends Vue {
    router_tree: any;
    newTree: object = {};
    router_data: RouterItem[] = [
        {
            id: 1,
            hide: false,
            site_id: 1,
            draggable: false,
            name: 'Главная',
            children: []
        },
    ];
    protected current_site_id: number = 0;
    protected search_engine?: JsSearch.Search;
    private is_saving: boolean = false;
    private test: RouterItem[] = [];
    private filter_text: string = '';

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.search_engine = new JsSearch.Search('id');
        this.search_engine.addIndex('url');
        this.search_engine.addIndex('name');
        this.search_engine.addIndex('tags')

        this.loadPages();
    }

    /**
     *
     * @param show_ids
     * @param parent
     */
    filter_pages(show_ids?: number[], parent?: RouterItem[]) {
        if (!this.router_data) return;

        const cursor = (!!parent && Array.isArray(parent)) ? parent : this.router_data;

        for (let item of cursor) {
            if (show_ids && Array.isArray(show_ids)) {
                item.hide = !show_ids.includes(item.id);
            } else {
                item.hide = false;
            }

            if (item.children && Array.isArray(item.children)) {
                this.filter_pages(show_ids, item.children)
            }
        }
    }

    /**
     *
     */
    loadPages() {
        axios.get(
            `/api/${this.current_site_id}/pages`
        )
            .then((response: { data: { success: boolean, pages?: Page[] } }) => {
                if (response.data.success && response.data.pages) {
                    if (this.search_engine) this.search_engine.addDocuments(response.data.pages);

                    this.router_data = this._buildRouter(response.data.pages, 0);
                    // for (let page of response.data.pages) {
                    //     this.router_data.push(format_page_router_item(this.current_site_id, page));
                    // }
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

    /**
     *
     * @param pages
     * @param parent
     * @private
     */
    _buildRouter(pages: Page[], parent: number = 0) {
        let result: RouterItem[] = [],
            routeByParents = pages.reduce(function (r, a) {
                r[a.parent] = r[a.parent] || [];
                r[a.parent].push(a);
                return r;
            }, Object.create(null));


        for (let page of pages) {
            if (page.parent == parent) {
                let current_page = format_page_router_item(this.current_site_id, page);

                if (routeByParents[current_page.id]) {
                    current_page.children = this._buildRouter(pages, current_page.id)
                }
                result.push(current_page);
            }
        }


        return result;
    }


    drag_start(node: any, draggableHelperInfo: { event: Event }) {
        this.test = this.router_data;
    }

    drag_end(node: any, draggableHelperInfo: { event: Event }) {
        this.router_data = this.test
    }

    /**
     * Добавление подстраницы
     *
     * @param page
     */
    createSubPage(page: RouterItem) {
        this.is_saving = false;

        axios.post(`/api/${this.current_site_id}/pages/new`, {parent: page.id})
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    let new_page: RouterItem = format_page_router_item(this.current_site_id, response.data.page);
                    if (!page.children) page.children = [];
                    page.children.unshift(new_page)
                    this.$toasted.show('Добавлена новая страница');
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    /**
     *
     * @param page
     */
    deletePage(page: RouterItem) {
        if (!confirm('Удалить страницу и её подстраницы?')) {
            return;
        }

        this.is_saving = false;

        axios.post(`/api/${this.current_site_id}/pages/${page.id}`, {status: 'archived'})
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.loadPages();
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

}
