import Vue from 'vue';
import Component from 'vue-class-component';
import Layout from '../../components/layout';
import {Page} from '../../types/api/page';
import axios from 'axios';
import {format_page} from '../../helpers/pages.helper';
import {Feed} from '../../types/api/feed';
import Editor from '../../components/editor/Editor.vue';
import Ace from '../../components/editor/ace.vue';
import AcePage from '../../components/editor/ace.page.vue';
import {JSONView} from 'vue-json-component';
import {PageFeed} from '../../types/model/Page';

@Component<PagesEdit>({
    name: 'pages_edit',
    components: {
        layout: Layout,
        ace: Ace,
        'ace-page': AcePage,
        editor: Editor,
        'json-view': JSONView,
    },
    computed: {
        edited_filter_fields: function () {
            let result = [];
            result.push({
                key: 'tags',
                type: 'string',
                title: 'Тэг',
                value: '',
                on: !!this.feed_edit_data.filter && 'tags' in this.feed_edit_data.filter
            });
            result.push({
                key: 'name',
                type: 'text',
                title: 'Название',
                value: '',
                on: !!this.feed_edit_data.filter && 'name' in this.feed_edit_data.filter
            });

            if (this.feed_edit_feed && this.feed_edit_feed.fields) {
                for (let field of this.feed_edit_feed.fields) {
                    result.push({
                        key: field.key,
                        type: field.type,
                        title: field.title,
                        value: '',
                        on: !!this.feed_edit_data.filter && field.key in this.feed_edit_data.filter
                    })
                }
            }

            return result;
        }
    },
    watch: {
        feed_settings: {
            handler: function (val, oldVal) {
                for (let index in val) {
                    Object.keys(val[index]).forEach((k) => val[index][k] == null && delete val[index][k]);
                }
                this.page_settings['feeds'] = val;
                this.page.settings = JSON.stringify(this.page_settings, null, '\t');
            },
            deep: true
        }
    }
})
export default class PagesEdit extends Vue {
    page: Page = {
        site_id: 0,
        disabled: false,
        parent: 0,
        sort: 0,
        id: 1,
        name: 'Главная страница',
        url: '',
        feed: '',
        content: '',
        handler: '',
        settings: '',
        created_at: '',
        updated_at: ''
    };
    current_site_id: number = 0;
    current_page_id: number = 0;
    is_saving: boolean = false;
    is_loaded: boolean = false;
    expert_mode: boolean = false;
    feeds: Feed[] = [];
    edited_key: string = '';
    edited_content: string = '';
    edited_is_number: boolean = false;
    modal_show: boolean = false;
    view_tab: boolean = false;
    page_settings: any = {useWrapMode: true};
    block_modal: boolean = false;
    page_public: boolean = false;
    handler_active: boolean = false;
    show_settings: boolean = false;
    show_content: boolean = true;
    feed_settings: { [key: string]: PageFeed } = {};
    new_feed_key: string = '';
    feed_edit: boolean = false;
    feed_edit_key: string = '';
    feed_edit_data: PageFeed = {feed: ''};
    feed_edit_feed: Feed = {created_at: '', fields: [], key: '', name: '', site_id: 0, status: '', updated_at: ''};
    feed_edit_fields: { [key: string]: boolean } = {};

    /**
     *
     */
    created() {
        this.current_site_id = this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site'];
        this.current_page_id = this.$route.params.page ? +this.$route.params.page : 0;
        this.page.site_id = this.current_site_id;
        this.page.id = this.current_page_id;

        axios.get(
            `/api/${this.current_site_id}/pages/${this.current_page_id}`
        )
            .then((response: { data: { success: boolean, page?: Page } }) => {
                if (response.data.success && response.data.page) {
                    this.page = format_page(this.current_site_id, response.data.page);
                    this.page_public = !this.page.disabled;
                    if (!this.page.settings) this.page.settings = '{}';

                    try {
                        this.page_settings = JSON.parse(this.page.settings);
                        this.handler_active = !!this.page.handler;

                        if ('feeds' in this.page_settings && this.page_settings['feeds']) {
                            this.feed_settings = this.page_settings['feeds'];
                        }

                        this.block_modal = false;
                    } catch (e) {
                        this.block_modal = true;
                        this.page_settings = {
                            error: e.message
                        };
                    }

                    if (this.page.feed) {
                        this.expert_mode = true;
                    }
                    this.is_loaded = true;
                } else {
                    this.$toasted.show('Ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
        this.loadLists();
        this.$root.$on('page_settings_change', (val: string) => {
            this.page.settings = val;

            try {
                this.page_settings = JSON.parse(val);
                this.block_modal = false;
            } catch (e) {
                this.block_modal = true;
                this.page_settings = {
                    error: e.message
                };
            }
        });
        this.$root.$on('page_handler_change', (val: string) => {
            this.page.handler = val;
        });
        this.$root.$on('page_content_change', (val: string) => {
            this.page.content = val;
        });
    }

    /**
     *
     * @param item
     */
    select_json_item(item: { key: string, value: string, path: string }) {
        if (this.block_modal) {
            return;
        }

        this.modal_show = true;
        this.edited_key = item.path;
        this.edited_content = item.value;
        this.edited_is_number = (typeof item.value === 'number')
    }

    /**
     *
     * @param e
     */
    expert_deactivate(e: Event) {
        this.page.feed = null;
    }


    /**
     *
     * @param e
     */
    handler_deactivate(e: Event) {
        if (this.handler_active) {
            if (!this.page.handler || this.page.handler.length < 2) {
                this.page.handler = 'render();';
            }
        } else {
            if (confirm('Деактивация обработчика удалит его')) {
                this.page.handler = '';
            } else {
                e.preventDefault();
            }
        }
    }

    /**
     *
     * @param feed
     */
    feed_fields(feed: string) {
        if (feed) {
            for (let i in this.feeds) {
                if (this.feeds[i].key == feed) {
                    let result_fields = this.feeds[i].fields.map(a => a.type == 'number' ? a.key : null);
                    result_fields = result_fields.filter(n => n);
                    result_fields.push('key');
                    result_fields.push('created_at');
                    result_fields.push('updated_at');

                    return result_fields;
                }
            }
        }

        return [];
    }

    /**
     *
     */
    add_page_feed() {
        if (this.new_feed_key.length < 2) {
            return this.$toasted.error('Минимальная длинна ключ – 2 символа')
        }
        if (Object.keys(this.feed_settings).length > 9) {
            return this.$toasted.error('Максимальное количество списков на страницу')
        }
        if (this.new_feed_key in this.feed_settings || !!this.feed_settings[this.new_feed_key]) {
            return this.$toasted.error(`Ключ ${this.new_feed_key} уже существует`)
        }

        Vue.set(this.feed_settings, this.new_feed_key, {
                feed: this.new_feed_key,
                page: 1,
                pager: false,
                per_page: 100,
            }
        );
        this.new_feed_key = '';
    }

    /**
     *
     * @param key
     */
    delete_feed_key(key: string) {
        if (confirm(`Удалить ключ ${key}?`)) {
            Vue.delete(this.feed_settings, key);
        }
    }

    /**
     *
     * @param key
     * @param feed_data
     */
    set_up_filter(key: string, feed_data: any) {
        if (!!feed_data && !!key && typeof feed_data === 'object') {
            if (!('filter' in feed_data) || !feed_data.filter) {
                feed_data.filter = {};
            }
            this.feed_edit_data = feed_data;

            for (let index in this.feeds) {
                if (this.feeds[index].key == feed_data.feed) {
                    this.feed_edit_feed = this.feeds[index];
                    this.feed_edit_fields = {};
                    this.feed_edit_fields['tags'] = !!(this.feed_edit_data.filter && 'tags' in this.feed_edit_data.filter);
                    this.feed_edit_fields['name'] = !!(this.feed_edit_data.filter && 'name' in this.feed_edit_data.filter);

                    for (let field of this.feed_edit_feed.fields) {
                        this.feed_edit_fields[field.key] = !!(this.feed_edit_data.filter && field.key in this.feed_edit_data.filter)
                    }
                }
            }
            this.feed_edit_key = key;
            this.feed_edit = true;
        }
    }

    close_filter() {
        this.feed_edit = false;
        this.page_settings['feeds'] = this.feed_settings;
        this.page.settings = JSON.stringify(this.page_settings, null, '\t');
    }

    /**
     *
     * @param key
     * @param $event
     */
    change_active_filter(field: any, $event: Event) {
        let key = field.key;

        if (field.on) {
            if (this.feed_edit_feed && this.feed_edit_feed.fields && this.feed_edit_data.filter) {
                if (['tags', 'name'].includes(key)) {
                    this.feed_edit_data.filter[key] = '';
                    this.feed_edit_fields[key] = true;
                    this.$forceUpdate();
                    return;
                }

                for (let field of this.feed_edit_feed.fields) {
                    if (field.key == key) {
                        if (field.type == 'number') {
                            this.feed_edit_data.filter[key] = 0;
                        } else {
                            this.feed_edit_data.filter[key] = '';
                        }

                        this.feed_edit_fields[key] = true;
                        this.$forceUpdate();
                        return;
                    }
                }
            }
        } else if (this.feed_edit_data.filter) {
            delete this.feed_edit_data.filter[key];
            this.feed_edit_fields[key] = false;
            this.$forceUpdate();
        }
    }

    /**
     *
     */
    save_json_item() {
        if (!this.block_modal) {
            if (this.edited_is_number) {
                PagesEdit.update_object_by_path(this.page_settings, this.edited_key.replace('config.', ''), +this.edited_content);
            } else {
                PagesEdit.update_object_by_path(this.page_settings, this.edited_key.replace('config.', ''), this.edited_content);
            }

            this.page.settings = JSON.stringify(this.page_settings, null, '\t');
            try {
                this.page_settings = JSON.parse(this.page.settings);
                this.block_modal = false;
            } catch (e) {
                this.block_modal = true;
                this.page_settings = {
                    error: e.message
                };
            }
        }


        this.modal_show = false;
    }
    /**
     *
     * @param obj
     * @param keyPath
     * @param value
     */
    static update_object_by_path(obj: any, keyPath: string, value: any) {
        let keyPaths = keyPath.split('.'),
            lastKeyIndex = keyPaths.length - 1;
        for (let i = 0; i < lastKeyIndex; ++i) {
            let key = keyPaths[i];

            // choose if nested object is array or hash based on if key is number
            if (!(key in obj)) obj[key] = parseInt(key) !== parseInt(key) ? {} : [];
            obj = obj[key];
        }
        obj[keyPaths[lastKeyIndex]] = value;
    }


    /**
     *
     */
    loadLists() {
        axios.get(
            `/api/${this.current_site_id}/feeds`
        )
            .then((response: { data: { success: boolean, error?: string, feeds?: Feed[] } }) => {
                if (response.data.success) {
                    if (response.data.feeds) {
                        this.feeds = response.data.feeds;
                    }
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка сохранения', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

    /**
     *
     */
    savePage() {
        this.is_saving = true;
        this.page.disabled = !this.page_public;

        axios.post(`/api/${this.current_site_id}/pages/${this.current_page_id}`, this.page)
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.page = format_page(this.current_site_id, response.data.page);
                    this.$toasted.show('Страница успешно сохранена');
                } else {
                    this.$toasted.show('Произошла ошибка сохранения', {type: 'error'});
                }
            })
            .catch(e => {
                console.log(e)
            });
    }

}
