import Vue from "vue";
import Component from "vue-class-component";
import Layout from "../../components/layout";
import axios from 'axios';
import {User} from "../../types/common";

@Component<TeamMain>({
    name: "TeamMain",
    components: {
        layout: Layout
    }
})
export default class TeamMain extends Vue {
    org_id: number = 0;
    users: User[] = [];
    is_saving: boolean = false;
    invite_user_email:string='';
    show_invite = false;

    /**
     *
     */
    created() {
        this.org_id = Number(this.$route.params.site);
        this.reloadTeam();
    }

    /**
     *
     */
    reloadTeam() {
        axios.get(
            `/api/${this.org_id}/team`
        )
            .then(response => {
                if (response.data.success) {
                    this.users = response.data.users;
                } else alert('Ошибка');
            })
            .catch(e => {
                console.log(e)
            });
    }

    /**
     *
     */
    delete_user(user: User) {
        if (!confirm(`Удалить доступ для пользователя ${user.email}?`)) {
            return;
        }

        this.is_saving = true;
        axios.post(`/api/${this.org_id}/team/delete`,
            {
                id: user.id
            })
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.reloadTeam();
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
                console.log(e)
            });
    }

    /**
     *
     */
    invite_user() {
        this.is_saving = true;
        axios.post(`/api/${this.org_id}/team/invite`,
            {
                email: this.invite_user_email
            })
            .then(response => {
                this.is_saving = false;
                if (response.data.success) {
                    this.reloadTeam();
                } else if (response.data.error) {
                    this.$toasted.show(response.data.error, {type: 'error'});
                } else {
                    this.$toasted.show('Произошла ошибка', {type: 'error'});
                }
            })
            .catch(e => {
                this.$toasted.show('Произошла ошибка', {type: 'error'});
                console.log(e)
            });

    }
}
