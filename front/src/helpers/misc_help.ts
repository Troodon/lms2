/**
 *
 * @param phone
 */
export function format_phone(phone: string): string {
    phone = phone.trim();
    phone = phone.replace(/\D/, '');
    phone = phone.replace(/^89/, '79');
    phone = phone.replace(/^849/, '749');
    var len = phone.length;

    if (len == 7) {
        phone = phone.replace(/([0-9]{2})([0-9]{2})([0-9]{3})/, '$1 $2 $3');
    } else if (len == 8) {
        phone = phone.replace(/([0-9]{3})([0-9]{2})([0-9]{3})/, '$1 - $2 $3');
    } else if (len == 9) {
        phone = phone.replace(/([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{2})/, '$1 - $2 $3 $4');
    } else if (len == 10) {
        phone = phone.replace(/([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{3})/, '$1 - $2 $3 $4');
    } else if (len == 11) {
        phone = phone.replace(/([0-9])([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})/, '+$1 ($2) $3 $4-$5');
    }

    return phone;
}

/**
 *
 * @param number
 */
export function format_number(number: string): string {
    return number_format(number, 2, '.', ' ').replace('.00', '');
}

/**
 *
 * @param number
 * @param one
 * @param two
 * @param five
 */
export function plural(number:number, one:any, two:any, five:any):any {
    let n = Math.abs(number);
    n %= 100;
    if (n >= 5 && n <= 20) {
        return five;
    }
    n %= 10;
    if (n === 1) {
        return one;
    }
    if (n >= 2 && n <= 4) {
        return two;
    }
    return five;
}

/**
 * Аналог функции  https://www.php.net/manual/ru/function.number-format.php
 *
 * @param number
 * @param decimals
 * @param dec_point
 * @param thousands_sep
 */
export function number_format(number: any, decimals: any, dec_point: any, thousands_sep: any) {
    //
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    let i: any, j, kw, kd, km;

    // input sanitation & defaults
    if (isNaN(decimals = Math.abs(decimals))) {
        decimals = 2;
    }
    if (dec_point == undefined) {
        dec_point = ",";
    }
    if (thousands_sep == undefined) {
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, '0').slice(2) : "");

    return km + kw + kd;
}
