import {Site} from "../types/model/Site";
import {SiteSettings} from "../types/api/site.settings";

/**
 *
 * @param site
 */
export function format_site(site: SiteSettings): Site {
    return {
        tariff: site.tariff,
        sid: !!site.sid ? +site.sid : 0,
        id: !!site.id ? +site.id : 0,
        name: site.name,
        domain: site.domain,
        has_certificate: site.has_certificate,
        uses_cdn: !!site.uses_cdn,
        https_redirect: site.https_redirect,
        certificate: (!!site.has_certificate && !!site.certificate) ? site.certificate : null,
        www_redirect: site.www_redirect,
        slash_redirect: site.slash_redirect,
        created_at: site.created_at,
        updated_at: site.updated_at
    };
}
