import axios from 'axios';

export default {
    /**
     *
     * @param site_id
     */
    list(site_id: number) {
        return axios.get(
            `/api/${site_id}/courses`
        );
    },
    /**
     *
     * @param site_id
     * @param id
     */
    get(site_id: number, id: number) {
        return axios.get(
            `/api/${site_id}/courses/${id}`
        );
    },

    update(site_id: number, id: number, data: any) {

    },

    create(site_id: number, id: number, data: any) {

    },
    /**
     *
     * @param site_id
     * @param course_id
     * @param unit_id
     */
    unit(site_id: number, course_id: number, unit_id: number) {
        return axios.get(
            `/api/${site_id}/courses/${course_id}/units/${unit_id}`
        );
    },
    /**
     *
     * @param site_id
     * @param course_id
     * @param per_page
     * @param skip
     * @param silent
     */
    units(site_id: number, course_id: number, per_page: number, skip: number, silent: boolean = false) {
        return axios.get(
            `/api/${site_id}/courses/${course_id}/units?limit=${per_page}&skip=${skip}${silent ? '#silent' : ''}`
        );
    },

    delete(site_id: number, id: number) {

    }
}
