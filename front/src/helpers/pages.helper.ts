import {RouterItem} from "../types/model/RouterItem";
import {Page} from "../types/api/page";

/**
 *
 * @param site_id
 * @param page
 */
export function format_page_router_item(site_id: number, page: Page): RouterItem {
    return {
        id: +page.id,
        hide: false,
        site_id: +page.site_id,
        disable: !!page.disabled,
        draggable: page.parent != 0,
        droppable: true,
        children: [],
        name: page.name,
        url: page.url ? page.url : ''
    };
}

/**
 *
 * @param site_id
 * @param page
 */
export function format_page(site_id: number, page: Page): Page {
    return {
        id: +page.id,
        site_id: +page.site_id,
        parent: +page.parent,
        disabled: !!page.disabled,
        name: page.name,
        url: page.url,
        content: page.content,
        handler: page.handler,
        settings: page.settings,
        sort: page.sort,
        feed: !!page.feed ? page.feed : null,
        created_at: page.created_at,
        updated_at: page.updated_at
    };
}
