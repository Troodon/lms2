/**
 *
 * @param path
 */
export function clear_files_path(path: string): string {
    // path = `static/${path}`;
    // path = path.replace('//', '/');
    // path = path.replace('static/static', 'static/');
    // path = path.replace(/(.+)\/$/, '$1');
    path = path.replace(/^\/$/, '');
    return path;
}
