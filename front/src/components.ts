import axios from 'axios'
import Vue from "vue";
import * as VueCookie from "vue-cookie";
import VueAxios from 'vue-axios'
import Toasted from 'vue-toasted';
import * as VueMoment from 'vue-moment';
import VueScrollmagic from 'vue-scrollmagic'
import SmoothScrollbar from 'vue-smooth-scrollbar'
import * as moment from 'moment';
import 'moment/locale/ru';

Vue.use(Toasted, {
    theme: "toasted-primary",
    position: "top-center",
    duration: 3000,
    fullWidth: true
});
Vue.use(VueCookie);
Vue.use(VueMoment, {moment});
Vue.use(VueAxios, axios);
Vue.use(VueScrollmagic, {
    refreshInterval: 0
});
Vue.use(SmoothScrollbar, {
    alwaysShowTracks: false,
    damping: 0.05,
    plugins: {
        overscroll: {
            enable: true,
            effect: 'bounce',
            damping: 0.2,
            maxOverscroll: 150
        }
    }
});
