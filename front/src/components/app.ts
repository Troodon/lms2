import Vue from 'vue';
import Component from 'vue-class-component';
import * as VueCookie from 'vue-cookie';

@Component<App>({
    name: 'App',
    components: {
        // notifications: VueNotification
    }
})
export default class App extends Vue {
    /**
     * @Data
     */
    is_loading: boolean = true;

    /**
     * @Created
     */
    created() {
        if (VueCookie.get('st')) {
            this.$store.commit('profile/login', VueCookie.get('st'));
        }
        this.axios.interceptors.request.use((config) => {
            if (config.url && !config.url.endsWith('#silent')) {
                this.$store.commit('loading', true);
            }
            return config;
        }, (error) => {
            this.$store.commit('loading', false);
            return Promise.reject(error);
        });

        this.axios.interceptors.response.use((response) => {
            if (response.config.url && !response.config.url.endsWith('#silent')) {
                this.$store.commit('loading', false);
            }
            return response;
        }, (error) => {
            this.$store.commit('loading', false);
            return Promise.reject(error);
        });
        this.is_loading = false;
    }
}
