import Vue from "vue";
import Component from "vue-class-component";
import {Prop} from "vue-property-decorator";

@Component<AcePage>({
    name: "AcePage",
    computed: {
        ace_value: function () {
            return !!this.user_value ? this.user_value : this.value;
        }
    }
})
export default class AcePage extends Vue {
    @Prop({default: ''}) value!: string;
    @Prop({default: ''}) event_name!: string;
    @Prop({default: {}}) options!: { [key: string]: any };

    editor: any;
    id: string = 'ace' + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 2);
    // options: any = {};
    user_value: string = '';

    /**
     *
     */
    mounted() {
        // @ts-ignore
        this.editor = window.ace.edit(document.getElementById(this.id));
        this.editor.$blockScrolling = Infinity;
        //  ignore doctype warnings
        const session = this.editor.getSession();
        session.setMode("ace/mode/handlebars");
        session.setUseWrapMode(true);
        this.editor.setTheme("ace/theme/dawn");

        //  editor options
        //  https://github.com/ajaxorg/ace/wiki/Configuring-Ace
        this.options = this.options || {};

        //  opinionated option defaults
        this.options.maxLines = this.options.maxLines || Infinity;
        this.options.printMargin = this.options.printMargin || false;
        this.options.highlightActiveLine = this.options.highlightActiveLine || false;

        //  hide cursor
        if (this.options.cursor === 'none' || this.options.cursor === false) {
            this.editor.renderer.$cursorLayer.element.style.display = 'none';
            delete this.options.cursor;
        }

        //  add missing mode and theme paths
        if (this.options.mode && this.options.mode.indexOf('ace/mode/') === -1) {
            this.options.mode = `ace/mode/${this.options.mode}`;
        }
        if (this.options.theme && this.options.theme.indexOf('ace/theme/') === -1) {
            this.options.theme = `ace/theme/${this.options.theme}`;
        }
        this.editor.setOptions(this.options);


        //  set model value
        //  if no model value found – use slot content
        if (!this.value || this.value === '') {
            this.$emit('input', this.editor.getValue());
        } else {
            this.editor.setValue(this.value, -1);
        }

        //  editor value changes
        this.editor.on('change', () => {
            this.user_value = this.editor.getValue();
            this.$root.$emit(this.$props.event_name, this.user_value);
        });
    }
}
