import Vue from 'vue';
import Component from 'vue-class-component';
import * as VueCookie from 'vue-cookie';
import {User} from '../types/common';
import {Prop} from 'vue-property-decorator';
import {Site} from '../types/model/Site';
import {mapState} from 'vuex';

@Component<Layout>({
    name: 'dashboard_layout',
    computed: {
        current_site() {
            for (let site of this.$store.getters['profile/sites']) {
                if (site.id == this.$route.params.site) {
                    return site;
                }
            }

            return null;
        },
        site_id() {
            return +this.$store.getters.site_id;
        },
        ...mapState({
            is_loading(state: any) {
                if (state.misc.is_loading) {
                    this.splash_animation_start();
                }

                return state.misc.is_loading;
            },
            loader_count: (state: any) => state.misc.loader_count
        }),
    }
})
export default class Layout extends Vue {
    [x: string]: any;

    @Prop({default: true}) readonly show_menu!: boolean;
    @Prop({default: ''}) readonly active_tab!: string;
    protected current_user: any | User = {};
    protected sites: Site[] = [];
    protected loader_anim_start: boolean = false;
    protected loader_anim_end: boolean = false;

    /**
     *
     */
    created() {
        if (!this.$store.state.profile.authorized || !this.$store.state.profile || !this.$store.state.profile.user) {
            this.$store
                .dispatch('profile/userinfo')
                .then(([result, error]) => {
                    this.current_user = result;
                    if (!result) this.$router.push({name: 'home', replace: true});
                });
        }
        this.current_user = this.$store.getters['profile/user'];
        this.$store.dispatch('set_site_id', this.$route.params.site ? +this.$route.params.site : this.$store.getters['profile/first_site']);

        if (!!this.current_user && 'sites' in this.current_user) {
            for (let ikey in this.current_user.sites) {
                this.sites.push(this.current_user.sites['ikey']);
            }
        }

    }

    /**
     *
     */
    splash_animation_start() {
        if (this.loader_anim_start) return;
        this.loader_anim_end = false;
        this.loader_anim_start = true;

        if (!!this.is_loading) setTimeout(() => {
            this.splash_animation_end();
        }, 4000);
    }

    /**
     *
     */
    splash_animation_end() {
        if (this.loader_anim_end) return;
        this.loader_anim_end = true;
        this.loader_anim_start = false;

        if (!!this.is_loading) setTimeout(() => {
            this.splash_animation_start();
        }, 4000);
    }

    /**
     *
     */
    exit() {
        VueCookie.set('st', '', {expires: '1Y'});
        window.location.reload();
    }

    /**
     *
     * @param site
     */
    set_site(site: any) {
        this.$store.dispatch('set_site_id', site.id);
        this.$router.push(`/${site.id}`)
    }

}

