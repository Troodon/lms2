import {ActionTree} from 'vuex';
import axios from 'axios';
import * as VueCookie from 'vue-cookie';
import {AuthState, RootState, User} from "../../types/common";


export const actions: ActionTree<AuthState, RootState> = {
    fetchData({commit}): any {
        axios({
            url: 'https://....'
        }).then((response) => {
            const payload: User = response && response.data;
            commit('profileLoaded', payload);
        }, (error) => {
            console.log(error);
            commit('profileError');
        });
    },

    /**
     *
     * @param commit
     * @param creds
     */
    login({commit}, creds): any {
        if (VueCookie.get('st')) {
            return new Promise(resolve => {
                commit('login', VueCookie.get('st'));
                resolve([true, null]);
            });
        }

        return new Promise(resolve => {
            let self = this;
            axios.post(
                '/api/auth',
                creds
            )
                .then(response => {
                    if (response.data.success.token) {
                        commit('login', response.data.success.token);
                        resolve([true, null]);
                    } else if (response.data.error) {
                        resolve([false, response.data.error]);
                    }
                    resolve([false, 'Ошибка']);
                })
                .catch(e => {
                    // console.log(this.state.get('profile'))
                    // commit('LOGIN_SUCCESS');
                    resolve([false, e.message]);

                });
        });
    },
    userinfo({commit, state}): any {
        return new Promise(resolve => {
            if (!VueCookie.get('st')) {
                return resolve([false, 'Вы не авторизованы']);
            }

            axios.get(
                '/api/userinfo'
            )
                .then(response => {
                    if (response.data.success) {
                        commit('userinfo', response.data.user);
                        return resolve([response.data.user, null]);
                    } else {
                        commit('logout');
                    }

                    resolve([false, null]);
                })
                .catch(e => {
                    commit('logout');
                    resolve([false, e.message]);
                });
        });
    },
    change_previous_page({commit}, data) {
        commit('mutate_previous_page', data);
    }
};
