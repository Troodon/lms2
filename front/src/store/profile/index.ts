import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import {AuthState, RootState} from "../../types/common";

export const state: AuthState = {
    authorized: false,
    user: undefined,
    token: '',
    error: false,
    previous_page: ''
};

const namespaced: boolean = true;

export const profile: Module<AuthState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};
