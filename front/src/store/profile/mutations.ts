import { MutationTree } from 'vuex';
import * as VueCookie from "vue-cookie";
import axios from 'axios';
import {AuthState, User} from "../../types/common";

export const mutations: MutationTree<AuthState> = {
    profileLoaded(state, payload: User) {
        state.error = false;
        state.user = payload;
    },
    profileError(state) {
        state.error = true;
        state.user = undefined;
    },
    login(state, token) {
        state.authorized = true;
        VueCookie.set('st', token, { expires: '1Y' });
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        state.token = token;
        localStorage.setItem('isLoggedIn', "1");
    },
    logout(state) {
        state.authorized = false;
        VueCookie.set('st', '', { expires: '1Y' });

        if (axios.defaults.headers.common && axios.defaults.headers.common['Authorization']) {
            delete axios.defaults.headers.common['Authorization'];
        }

        localStorage.setItem('isLoggedIn', "");
    },
    userinfo(state, data) {
        state.user = data;
    },
    mutate_previous_page(state, data){
        if(data) state.previous_page = data;
    }
};
