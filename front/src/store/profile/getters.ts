import { GetterTree } from 'vuex';
import {AuthState, RootState} from "../../types/common";

export const getters: GetterTree<AuthState, RootState> = {
    first_site: (state: any) => {
        if(state.user) {
            return state.user.sites[0].id
        } else return 0;
    },
    sites: (state: any) => {
        if(state.user) {
            return state.user.sites
        } else return []
    },
    user_email: (state: any) => {
        if(state.user) {
            return state.user.email
        } else return 'anon'
    },
    user: (state: any) => {
        if(state.user) {
            return state.user
        } else return null
    },
    user_id: (state: any) => {
        if(state.user){
            return state.user.id;
        } else return 0;
    },
    previous_page: state => state.previous_page
};
