import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { profile } from './profile';
import {RootState} from "../types/common";
import {misc} from "./misc";


Vue.use(Vuex);

const store: StoreOptions<RootState> = {
    state: {
        version: '1.0.0' // a simple property,
    },
    modules: {
        profile,
        misc
    }
};

export default new Vuex.Store<RootState>(store);
