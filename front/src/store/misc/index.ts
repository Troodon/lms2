import {ActionTree, GetterTree, Module, MutationTree} from 'vuex';
import {RootState} from "../../types/common";

const state: misc_state = {
    loader_count: 0,
    is_loading: false,
    site_id: 0
};

/**
 *
 */
const getters: GetterTree<any, RootState> = {
    site_id: state => state.site_id,
    is_loading: state => state.is_loading,
    loader_count: state => state.loader_count,
};

/**
 *
 */
const actions: ActionTree<any, RootState> = {
    set_site_id: async ({commit}, siteId) => {
        commit('SET_CURRENT_SITE_ID', siteId);
    },
};

/**
 *
 */
const mutations: MutationTree<any> = {
    SET_CURRENT_SITE_ID: (state, data) => state.site_id = data,
    loading: (state, isLoading) => {
        if (isLoading) {
            state.loader_count++;
            state.is_loading = true;
        } else if (state.loader_count > 0) {
            state.loader_count--;
            state.is_loading = (state.loader_count > 0);
        }
    },
};
/**
 *
 */
export const misc: Module<any, RootState> = {
    state,
    getters,
    actions,
    mutations
};

/**
 *
 */
interface misc_state {
    site_id: number
    is_loading: boolean,
    loader_count: number
}
