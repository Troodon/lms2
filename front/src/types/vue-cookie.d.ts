declare module "vue-cookie" {
    import { PluginFunction } from "vue";

    export const install: PluginFunction<{}>;
    export function get(key: string): any;
    export function set(name: string, value: string, daysOrOptions: any): void;

    /**
     *
     */
    interface VueCookieMethods {
        install(Vue: any): void;
        set(name: string, value: string, daysOrOptions: any): void;
        get(name: string): void;
    }

    /**
     *
     */
    module "vue/types/vue" {
        interface Vue {
            $cookie: VueCookieMethods;
        }
    }
}