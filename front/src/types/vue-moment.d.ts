declare module "vue-moment" {
    import { PluginFunction } from "vue";

    export const install: PluginFunction<{}>;

    /**
     *
     */
    interface VueMomentMethods {
        install(Vue: any): void;
    }
}