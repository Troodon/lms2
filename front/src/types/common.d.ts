import {Site} from "./model/Site";

export {Action, ActionTree, Getter, GetterTree, ModuleTree, Mutation, MutationTree} from "vuex";

/**
 *
 */
export interface RootState {
    version: string;
}
/**
 *
 */
export interface User {
    id: number,
    email: string,
    phone: string,
    firstname: string,
    middle_name: string,
    surname: string,
    sites: Site[]
}

/**
 *
 */
export interface AuthState {
    authorized: boolean,
    user?: User;
    token: string;
    error: boolean;
    previous_page: string;
}


/**
 *
 */
export interface ManagerPathItem {
    name: string;
    path: string;
}
