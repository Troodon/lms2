/**
 *
 */
export interface Feed {
    site_id: number,
    status: string,
    key: string,
    name: string,
    fields: FeedField[],
    created_at: string,
    updated_at: string,
}

/**
 *
 */
export interface FeedField {
    type: string,
    key: string,
    data: string,
    title: string
}
