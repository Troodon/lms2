/**
 *
 */
export interface FileItem {
    id: number,
    path: string,
    name: string,
    url: string,
    is_dir: boolean,
    content: string,
    content_type: string,
    last_modified: string,
    bytes: number,
    editable: boolean,
}
