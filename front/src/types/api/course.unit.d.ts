/**
 *
 */
export interface CourseUnit {
    id: number,
    site_id: number,
    status: string,
    type: string,
    name: string,
    description: string,
    content: string,
    image: string,
    created_at: string,
    updated_at: string,
}
