/**
 *
 */
export interface Profile {
    id: number,
    email: string,
    new_password: string,
    avatar: string,
}
