import {PageFeed} from "../model/Page";

/**
 *
 */
export interface Page {
    id: number,
    parent: number,
    site_id: number,
    feed?: string | null,
    disabled: boolean,
    name: string,
    url?: string,
    content: string,
    settings: string,
    handler: string,
    sort: number,
    created_at: string,
    updated_at: string,
}
