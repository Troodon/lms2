/**
 *
 */
export interface SiteSettings {
    id?: number,
    sid?: number,
    name: string,
    www_redirect: string,
    has_certificate: boolean,
    uses_cdn: boolean,
    slash_redirect: string,
    certificate?: any,
    https_redirect: string,
    domain: string,
    tariff: string,
    created_at: string,
    updated_at: string,
}
