/**
 *
 */
export interface FeedItem {
    id: number,
    site_id: number,
    status: string,
    key: string,
    name: string,
    tags: string,
    fields: {
        [key: string]: any
    },
    created_at: string,
    updated_at: string,
}
