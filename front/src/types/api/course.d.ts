/**
 *
 */
export interface Course {
    id: number,
    site_id: number,
    status: string,
    name: string,
    description: string,
    created_at: string,
    updated_at: string,
}
