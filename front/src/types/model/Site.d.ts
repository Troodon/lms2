/**
 *
 */
export interface Site {
    id: number,
    sid: number,
    name: string,
    domain: string,
    tariff: string,
    active_to?: string,
    www_redirect: string,
    has_certificate: boolean,
    uses_cdn: boolean,
    slash_redirect: string,
    certificate:any,
    https_redirect: string,
    created_at: string,
    updated_at: string,
}
