/**
 *
 */
export interface RouterItem {
    id: number,
    hide: boolean,
    site_id: number,
    name: string,
    url?: string,
    disable?: boolean,
    draggable?: boolean,
    droppable?: boolean,
    children?: RouterItem[]
}
