/**
 *
 */
export interface PageFeed {
    feed: string,
    tag?: string,
    filter?: {
        [key: string]: string | number | boolean
    }
    page?: number,
    per_page?: number,
    sort?: string,
    sort_desc?: boolean,
    pager?: boolean,
    pager_first?: string,
    pager_before?: string,
    pager_after?: string,
    pager_links?: number
}
