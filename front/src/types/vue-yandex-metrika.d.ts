declare module "vue-yandex-metrika" {
    import { PluginFunction } from "vue";

    export const install: PluginFunction<{}>;
    export function hit(path: string): void;

    /**
     *
     */
    interface YandexMetrikaMethods {
        install(Vue: any): void;
        hit(path:string): void;
    }

    /**
     *
     */
    module "vue/types/vue" {
        interface Vue {
            $metrika: YandexMetrikaMethods;
        }
    }
}
