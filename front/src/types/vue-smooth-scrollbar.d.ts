declare module "vue-smooth-scrollbar" {
    import Vue, {PluginFunction} from 'vue';

    export const install: PluginFunction<{}>;

    class SmoothScrollbar extends Vue {
    }

    export {SmoothScrollbar}
}
