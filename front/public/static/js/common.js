function nextAll(elem) {
	var matched = [];
	while (elem = elem.nextSibling) {
		if (elem.nodeType === 1) {
			matched.push(elem);
		}
	}
	return matched;
}



00 && window.addEventListener("DOMContentLoaded", function(event) {

    var logo = document.getElementById("logo_wrapper");

    // setTimeout(function(){
    //     logo.classList.add('active');
    // }, 500);

    document.fonts.ready.then(function() {

        let isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

        let scenes = [];
        let y = 0;

        let scrollbarWrapper = document.querySelector('#scroll_wrapper'),
            options = {
                alwaysShowTracks: true,
                damping: 0.05,
                plugins: {
                    overscroll: {
                        enable: true,
                        effect: 'bounce',
                        damping: 0.2,
                        //glowColor: '#000',
                        maxOverscroll: 150
                    }
                }
            };

        let scrollbar = window.Scrollbar;
            scrollbar.use(window.OverscrollPlugin)
        let scroll = scrollbar.init(scrollbarWrapper, options);

        // initiate ScrollMagic Controller
        let controller =
            new ScrollMagic.Controller(
                {
                    refreshInterval: 0,
                }
            );

        // update scrollY controller
        if (isChrome){
            controller.scrollPos(function () {
                return y;
            });
        }

        function refreshAnimation() {
            if (isChrome){
                controller.update();
            } else {
                scenes.forEach(function(scene){
                    scene.refresh();
                });
            }
        }

        // initiate ScrollMagic Scene each section
        var slides = document.getElementsByClassName("__in_nav_order");
        for (var i = 0; i < slides.length; i++) {

            let t = slides.item(i),
                    tHeight = t.offsetHeight;
            let tl = new TimelineMax();
                //tl.to(head, 1, { css:{position:'fixed'} });

            scenes.push(
                new ScrollMagic
                    .Scene({ offset: -1, triggerHook: 0, triggerElement: t, duration: tHeight, reverse:true })
                        .setTween(tl)
                        .on("leave", function(){
                            //console.log('leave scene');
                            t.classList.remove('__in_viewport');
                        })
                        .on("enter", function(){
                            //console.log('enter scene');
                            t.classList.add('__in_viewport');
                        })
                        .on("progress", function(e){
                            //console.log("progress => ", e.progress);
                            t.classList.add('__in_viewport');
                        })
                        .addTo(controller)
            );

        }

        // listener smooth-scrollbar, update controller
        let currentSlide, nextSlide, prevSlide,
            delay, oldOffset
            ;

            scroll.addListener(function(status) {
                y = status.offset.y;
                currentSlide = document.querySelectorAll('.__in_nav_order.__in_viewport');
                prevSlide = currentSlide[0] ? currentSlide[0].previousElementSibling : undefined;
                nextSlide = currentSlide[0] ? currentSlide[0].nextElementSibling : undefined;
                windowH = window.innerHeight;

                // scrollbar.js hack to determine scroll direction
                if (!delay) delay = window.setTimeout(function() {
                    delay = null;
                    oldOffset = y;
                }, 50);

                // now we know stuff

                if (oldOffset > y) {
                    // scrolling up
                    if (prevSlide !== null) {
                        let viewportOffset = prevSlide.getBoundingClientRect();
                        let outsidePosition = windowH + viewportOffset.top;

                        if (outsidePosition > 0 && outsidePosition <= windowH) {
                            scroll.scrollIntoView(currentSlide[0], { offsetTop: windowH });
                        }
                    }

                } else if (oldOffset < y) {
                    // scrolling down
                    if (nextSlide !== null && scroll.isVisible(nextSlide)) {
                        let viewportOffset = nextSlide.getBoundingClientRect();

                        if (viewportOffset.top >= 0 && viewportOffset.top < windowH-10) {
                            scroll.scrollIntoView(nextSlide);
                        }
                    }

                }

                refreshAnimation();
            });

    });

});
